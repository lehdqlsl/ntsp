package com.telefield.ntsp;

import com.querydsl.core.Tuple;
import com.telefield.ntsp.config.AuthInterceptor;
import com.telefield.ntsp.config.CustomAuthenticationException;
import com.telefield.ntsp.config.InvalidTokenException;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureQueryRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.radar.RadarEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.radar.RadarQueryRepository;
import com.telefield.ntsp.domain.event.entity.EventRepository;
import com.telefield.ntsp.domain.event.entity.EventStat;
import com.telefield.ntsp.domain.recipient.info.QRecipientInfo;
import com.telefield.ntsp.domain.recipient.info.RecipientInfo;
import com.telefield.ntsp.domain.recipient.info.RecipientInfoQueryRepository;
import com.telefield.ntsp.domain.recipient.status.QRecipientStatus;
import com.telefield.ntsp.domain.search.HistorySearchDto;
import com.telefield.ntsp.service.FailureService;
import com.telefield.ntsp.service.LoginService;
import com.telefield.ntsp.service.SensorDataService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest()
@AutoConfigureMockMvc
public class ContollerTest {

    @Autowired
    EnvironmentRepository environmentRepository;

    @Autowired
    LoginService loginService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    AuthInterceptor interceptor;

    @Autowired
    SensorDataService sensorDataService;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    GatewayFailureQueryRepository gatewayFailureQueryRepository;

    @Autowired
    FailureService failureService;

    @Autowired
    RecipientInfoQueryRepository recipientInfoQueryRepository;

    @BeforeEach
    void initTest() throws CustomAuthenticationException, InvalidTokenException {
        when(interceptor.preHandle(any(), any(), any())).thenReturn(true);
        // other stuff
    }

    @Test
    public void 카운트테스트() throws Exception {
        this.mockMvc.perform(get("/sensors/count"))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
    }

    @Test
    public void 카운트테스트2() throws Exception {
        this.mockMvc.perform(get("/sensors/fail"))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
    }

    @Test
    public void 장애목록() throws Exception {
        this.mockMvc.perform(get("/gateways/fail"))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
    }

    @Test
    public void 로그인테스트() {
        loginService.login("admin", "admin@1");
    }

    @Test
    public void 환경데이터테스트() {
        sensorDataService.getEnvData("01222994766", 0);
    }

    @Test
    public void 대상자검색() {
        List<Tuple> result = recipientInfoQueryRepository.getSearchRecipient("", "이동빈", (byte) 8, "성남동");
        for (Tuple row : result) {
            System.out.println("firstName " + row.get(QRecipientInfo.recipientInfo));
            System.out.println("lastName " + row.get(QRecipientStatus.recipientStatus));
        }
    }
}