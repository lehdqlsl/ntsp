package com.telefield.ntsp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefield.ntsp.config.AuthInterceptor;
import com.telefield.ntsp.config.CustomAuthenticationException;
import com.telefield.ntsp.config.InvalidTokenException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest()
@AutoConfigureMockMvc //need this in Spring Boot test
@AutoConfigureRestDocs
public class RecipientTest {
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    AuthInterceptor interceptor;

    @BeforeEach
    void initTest() throws CustomAuthenticationException, InvalidTokenException {
        when(interceptor.preHandle(any(), any(), any())).thenReturn(true);
        // other stuff
    }

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }


    @Test
    public void 대상자정보조회() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/recipients/{phone}", "01222994766")
                .contentType(MediaType.APPLICATION_JSON)
                .header("token","token")
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/recipients/phone/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        responseFields(
                                fieldWithPath("phone").description("G/W 번호"),
                                fieldWithPath("address1").description("주소"),
                                fieldWithPath("address2").description("상세주소"),
                                fieldWithPath("birth").description("생년월일"),
                                fieldWithPath("mobile").description("휴대폰 번호"),
                                fieldWithPath("name").description("이름"),
                                fieldWithPath("imageFile").description("이미지파일명"),
                                fieldWithPath("current_status").description("대상자 상태(이벤트타입 참조)"),
                                fieldWithPath("gateway_status").description("G/W 상태(게이트웨이상태 참조)"),
                                fieldWithPath("regDate").description("등록날짜")
                        )
                ));
    }

    @Test
    public void 대상자프로필사진() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/open/v1/recipients/{phone}/profile", "01222994766")
                .characterEncoding("UTF-8")
                .accept(MediaType.IMAGE_JPEG,MediaType.IMAGE_PNG))
                .andExpect(status().isOk())
                .andDo(document("/recipients/phone/profile",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        )
                ));
    }

//    @Test
//    public void 센서데이터() throws Exception {
//        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/gateways/{phone}/sensor-data", "01012341234")
//                .characterEncoding("UTF-8")
//                .contentType(MediaType.APPLICATION_JSON)
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andDo(document("/gateways/phone/sensor-data",
//                        preprocessRequest(modifyUris()
//                                        .scheme("http")
//                                        .host("14.47.229.122")
//                                        .port(8443)
//                                , prettyPrint()
//                        ),
//                        preprocessResponse(prettyPrint()),
//                        pathParameters(
//                                parameterWithName("phone").description("G/W 번호")
//                        ),
//                        responseFields(
//                                fieldWithPath("illumination").type(JsonFieldType.STRING).description("조도"),
//                                fieldWithPath("temperature").type(JsonFieldType.STRING).description("온도"),
//                                fieldWithPath("humidity").type(JsonFieldType.STRING).description("습도"),
//                                fieldWithPath("activity").type(JsonFieldType.STRING).description("활동량"),
//                                fieldWithPath("regDate").type(JsonFieldType.STRING).description("날짜")
//                        )
//                ));
//    }

    @Test
    @Transactional
    public void FCM키등록() throws Exception {
        JSONObject param = new JSONObject();
        param.put("fcmKey", "FCM-KEY-VALUE");

        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/users/{mobile}/fcm-key", "01041880956")
                .characterEncoding("UTF-8")
                .content(param.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andDo(document("/users/mobile/fcm-key/post",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("mobile").description("휴대폰 번호")
                        ),
                        requestFields(
                                fieldWithPath("fcmKey").description("FCM 키값")
                        )
                ));
    }

    @Test
    @Transactional
    public void FCM키삭제() throws Exception {

        this.mockMvc.perform(RestDocumentationRequestBuilders.delete("/users/{mobile}/fcm-key", "01012341234")
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/users/mobile/fcm-key/delete",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("mobile").description("휴대폰 번호")
                        )
                ));
    }
}