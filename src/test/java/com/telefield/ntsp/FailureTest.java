package com.telefield.ntsp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefield.ntsp.config.AuthInterceptor;
import com.telefield.ntsp.config.CustomAuthenticationException;
import com.telefield.ntsp.config.InvalidTokenException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest()
@AutoConfigureMockMvc //need this in Spring Boot test
@AutoConfigureRestDocs
public class FailureTest {
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Autowired
    private WebApplicationContext context;

    @MockBean
    AuthInterceptor interceptor;

    @BeforeEach
    void initTest() throws CustomAuthenticationException, InvalidTokenException {
        when(interceptor.preHandle(any(), any(), any())).thenReturn(true);
        // other stuff
    }

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    @Test
    public void 장애처리목록() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/gateways/failure/actions/{failureId}",88438)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/failure/actions/actions-list/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("failureId").description("장애번호")
                        ),
                        responseFields(
                                fieldWithPath("[].action_id").description("처리번호"),
                                fieldWithPath("[].title").description("처리상태"),
                                fieldWithPath("[].text").description("처리내용"),
                                fieldWithPath("[].svRegDate").description("처리날짜"),
                                fieldWithPath("[].user").description("처리 계정"),
                                fieldWithPath("[].failureId.failureId").description("장애번호"),
                                fieldWithPath("[].failureId.phone").description("G/W 번호 (미사용)"),
                                fieldWithPath("[].failureId.gatewayFailureStatus").description("장애 상태 (미사용)"),
                                fieldWithPath("[].failureId.regDate").description("장애발생 날짜 (미사용)"),
                                fieldWithPath("[].failureId.closedDate").description("장애복구 날짜 (미사용)"),
                                fieldWithPath("[].failureId.aware").description("장애처리 여부 (미사용)"),
                                fieldWithPath("[].failureId.closed").description("장애닫힘(복구) 여부 (미사용)")
                        )
                ));
    }

    @Test
    @Transactional
    public void 장애처리입력() throws Exception {
        JSONObject param = new JSONObject();
        param.put("failureId", 5153);
        param.put("contents", "Test");
        param.put("isAware", false);
        param.put("isClosed",false);
        param.put("user","admin");

        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/gateways/failure/action")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON)
                .content(param.toString()))
                .andExpect(status().isOk())
                .andDo(document("/gateways/failure/action/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(

                        ),
                        requestFields(
                                fieldWithPath("failureId").description("장애번호").type((JsonFieldType.NUMBER)),
                                fieldWithPath("contents").description("입력내용").type((JsonFieldType.STRING)),
                                fieldWithPath("isAware").description("장애처리 여부").optional().type((JsonFieldType.BOOLEAN)),
                                fieldWithPath("isClosed").description("장애 수동 클로즈").optional().type((JsonFieldType.BOOLEAN)),
                                fieldWithPath("user").description("처리 계정").optional().type((JsonFieldType.STRING))
                        )
                ));
    }

    @Test
    public void 전체장애목록() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/gateways/failures")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/failure/failure-list/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(

                        ),
                        responseFields(
                                fieldWithPath("[].failureId").description("장애번호"),
                                fieldWithPath("[].phone").description("G/W 번호"),
                                fieldWithPath("[].gatewayFailureStatus").description("장비상태"),
                                fieldWithPath("[].regDate").description("장애발생 날짜"),
                                fieldWithPath("[].closedDate").description("장애복구 날짜").optional(),
                                fieldWithPath("[].aware").description("장애처리 여부"),
                                fieldWithPath("[].closed").description("장애닫힘(복구) 여부")
                        )
                ));
    }

    @Test
    public void 특정장애목록() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/gateways/{phone}/failures", "00158D0001EF0795")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/failure/phone/failure-list/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        responseFields(
                                fieldWithPath("[].failureId").description("장애번호"),
                                fieldWithPath("[].phone").description("G/W 번호"),
                                fieldWithPath("[].gatewayFailureStatus").description("장애 상태"),
                                fieldWithPath("[].regDate").description("장애발생 날짜"),
                                fieldWithPath("[].closedDate").description("장애복구 날짜").optional(),
                                fieldWithPath("[].aware").description("장애처리 여부"),
                                fieldWithPath("[].closed").description("장애닫힘(복구) 여부")
                        )
                ));
    }

    @Test
    public void 상세내역() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/gateways/failure/{failure_id}", "24707")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/failure/failure-detail/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("failure_id").description("장애번호")
                        ),
                        responseFields(
                                fieldWithPath("failureId").description("장애번호"),
                                fieldWithPath("phone").description("G/W 번호"),
                                fieldWithPath("gatewayFailureStatus").description("장비상태"),
                                fieldWithPath("regDate").description("장애발생 날짜"),
                                fieldWithPath("closedDate").description("장애처리 날짜"),
                                fieldWithPath("aware").description("장애처리 여부"),
                                fieldWithPath("closed").description("장애닫힘(복구) 여부")
                        )
                ));
    }

}