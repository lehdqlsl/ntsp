package com.telefield.ntsp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefield.ntsp.config.AuthInterceptor;
import com.telefield.ntsp.config.CustomAuthenticationException;
import com.telefield.ntsp.config.InvalidTokenException;
import com.telefield.ntsp.domain.event.emergency.Emergency;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest()
@AutoConfigureMockMvc //need this in Spring Boot test
@AutoConfigureRestDocs
public class EmerTest {
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Autowired
    private WebApplicationContext context;

    @MockBean
    AuthInterceptor interceptor;

    @BeforeEach
    void initTest() throws CustomAuthenticationException, InvalidTokenException {
        when(interceptor.preHandle(any(), any(), any())).thenReturn(true);
        // other stuff
    }

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    @Test
    public void 응급처리목록() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/events/emergency/actions/{emergencyId}",22372)
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/emergency/actions",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("emergencyId").description("응급번호")
                        ),
                        responseFields(
                                fieldWithPath("[].emer_action_id").description("응급 처리번호"),
                                fieldWithPath("[].title").description("사용X"),
                                fieldWithPath("[].text").description("처리내용"),
                                fieldWithPath("[].svRegDate").description("처리 등록 날짜"),
                                fieldWithPath("[].user").description("처리 계정"),
                                fieldWithPath("[].emergencyId.emergencyId").description("응급번호"),
                                fieldWithPath("[].emergencyId.closedDate").description("처리완료 날짜"),
                                fieldWithPath("[].emergencyId.closed").description("처리완료 여부"),
                                fieldWithPath("[].emergencyId.aware").description("사용X"),
                                fieldWithPath("[].emergencyId.eventId.eventId").description("이벤트번호"),
                                fieldWithPath("[].emergencyId.eventId.phone").description("G/W 번호"),
                                fieldWithPath("[].emergencyId.eventId.mac_addr").description("센서 맥주소"),
                                fieldWithPath("[].emergencyId.eventId.devType").description("센서 타입"),
                                fieldWithPath("[].emergencyId.eventId.eventType").description("이벤트 타입"),
                                fieldWithPath("[].emergencyId.eventId.gwRegDate").description("이벤트 발생 날짜"),
                                fieldWithPath("[].emergencyId.eventId.svRegDate").description("서버 저장 날짜"),
                                fieldWithPath("[].emergencyId.eventId.data").description("사용X")
                                )
                        )
                );
    }

    @Test
    @Transactional
    public void 응급처리입력() throws Exception {
        JSONObject param = new JSONObject();
        param.put("emergencyId", 5135);
        param.put("contents", "Test");
        param.put("isAware", false);
        param.put("isClosed",false);
        param.put("user","admin");

        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/events/emergency/action")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON)
                .content(param.toString()))
                .andExpect(status().isCreated())
                .andDo(document("/emergency/action/input/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(

                        ),
                        requestFields(
                                fieldWithPath("emergencyId").description("응급번호").type((JsonFieldType.NUMBER)),
                                fieldWithPath("contents").description("입력내용").type((JsonFieldType.STRING)),
                                fieldWithPath("isClosed").description("응급처리 여부").optional().type((JsonFieldType.BOOLEAN)),
                                fieldWithPath("isAware").description("사용X").optional().type((JsonFieldType.BOOLEAN)),
                                fieldWithPath("user").description("처리 계정").optional().type((JsonFieldType.STRING))
                        )
                ));
    }

//    @Test
//    public void 전체장애목록() throws Exception {
//        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/emergencies")
//                .contentType(MediaType.APPLICATION_JSON)
//                .characterEncoding("UTF-8")
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andDo(document("/emergencies",
//                        preprocessRequest(modifyUris()
//                                        .scheme("http")
//                                        .host("14.47.229.122")
//                                        .port(8443)
//                                , prettyPrint()
//                        ),
//                        preprocessResponse(prettyPrint()),
//                        pathParameters(
//
//                        ),
//                        responseFields(
//                                fieldWithPath("no_activity.[]emergencyId").description("응급번호"),
//                                fieldWithPath("no_activity.[]closedDate").description("응급 처리날짜"),
//                                fieldWithPath("no_activity.[]closed").description("응급 처리여부"),
//                                fieldWithPath("no_activity.[]aware").description("사용X"),
//                                fieldWithPath("no_activity.[]eventId.eventId").description("이벤트 번호"),
//                                fieldWithPath("no_activity.[]eventId.phone").description("G/W 번호"),
//                                fieldWithPath("no_activity.[]eventId.mac_addr").description("센서 맥주소"),
//                                fieldWithPath("no_activity.[]eventId.devType").description("센서타입"),
//                                fieldWithPath("no_activity.[]eventId.eventType").description("이벤트타입"),
//                                fieldWithPath("no_activity.[]eventId.gwRegDate").description("G/W 전송날짜"),
//                                fieldWithPath("no_activity.[]eventId.svRegDate").description("서버 등록날짜"),
//                                fieldWithPath("no_activity.[]eventId.data").description("사용X"),
//                                fieldWithPath("emergency.[]emergencyId").description("응급번호"),
//                                fieldWithPath("emergency.[]closedDate").description("응급 처리날짜"),
//                                fieldWithPath("emergency.[]closed").description("응급 처리여부"),
//                                fieldWithPath("emergency.[]aware").description("사용X"),
//                                fieldWithPath("emergency.[]eventId.eventId").description("이벤트 번호"),
//                                fieldWithPath("emergency.[]eventId.phone").description("G/W 번호"),
//                                fieldWithPath("emergency.[]eventId.mac_addr").description("센서 맥주소"),
//                                fieldWithPath("emergency.[]eventId.devType").description("센서타입"),
//                                fieldWithPath("emergency.[]eventId.eventType").description("이벤트타입"),
//                                fieldWithPath("emergency.[]eventId.gwRegDate").description("G/W 전송날짜"),
//                                fieldWithPath("emergency.[]eventId.svRegDate").description("서버 등록날짜"),
//                                fieldWithPath("emergency.[]eventId.data").description("사용X")
//
//                        )
//                ));
//    }

    @Test
    public void 특정응급목록() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/emergencies/{phone}", "01222994767")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/emergencies/phone",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        responseFields(
                                fieldWithPath("[]emergencyId").description("응급번호"),
                                fieldWithPath("[]closedDate").optional().description("응급 처리날짜"),
                                fieldWithPath("[]closed").description("응급 처리여부"),
                                fieldWithPath("[]aware").description("사용X"),
                                fieldWithPath("[]eventId.eventId").description("이벤트 번호"),
                                fieldWithPath("[]eventId.phone").description("G/W 번호"),
                                fieldWithPath("[]eventId.mac_addr").description("센서 맥주소"),
                                fieldWithPath("[]eventId.devType").description("센서타입"),
                                fieldWithPath("[]eventId.eventType").description("이벤트타입"),
                                fieldWithPath("[]eventId.gwRegDate").description("G/W 전송날짜"),
                                fieldWithPath("[]eventId.svRegDate").description("서버 등록날짜"),
                                fieldWithPath("[]eventId.data").description("사용X")
                        )
                ));
    }

    @Test
    public void 전체응급목록모바일() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/emergencies/mobile")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/emergencies/mobile",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(

                        ),
                        responseFields(
                                fieldWithPath("[]emergencyId").description("응급번호"),
                                fieldWithPath("[]closedDate").optional().description("응급 처리날짜"),
                                fieldWithPath("[]closed").description("응급 처리여부"),
                                fieldWithPath("[]aware").description("사용X"),
                                fieldWithPath("[]eventId.eventId").description("이벤트 번호"),
                                fieldWithPath("[]eventId.phone").description("G/W 번호"),
                                fieldWithPath("[]eventId.mac_addr").description("센서 맥주소"),
                                fieldWithPath("[]eventId.devType").description("센서타입"),
                                fieldWithPath("[]eventId.eventType").description("이벤트타입"),
                                fieldWithPath("[]eventId.gwRegDate").description("G/W 전송날짜"),
                                fieldWithPath("[]eventId.svRegDate").description("서버 등록날짜"),
                                fieldWithPath("[]eventId.data").description("사용X")
                        )
                ));
    }

    @Test
    public void 상세내역() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/events/emergency/{emergencyId}", "25198")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/emergency",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("emergencyId").description("응급번호")
                        ),
                        responseFields(
                                fieldWithPath("emergencyId").description("응급번호"),
                                fieldWithPath("closedDate").description("응급 처리날짜"),
                                fieldWithPath("closed").description("응급 처리여부"),
                                fieldWithPath("aware").description("사용X"),
                                fieldWithPath("eventId.eventId").description("이벤트 번호"),
                                fieldWithPath("eventId.phone").description("G/W 번호"),
                                fieldWithPath("eventId.mac_addr").description("센서 맥주소"),
                                fieldWithPath("eventId.devType").description("센서타입"),
                                fieldWithPath("eventId.eventType").description("이벤트타입"),
                                fieldWithPath("eventId.gwRegDate").description("G/W 전송날짜"),
                                fieldWithPath("eventId.svRegDate").description("서버 등록날짜"),
                                fieldWithPath("eventId.data").description("사용X")
                        )
                ));
    }
}