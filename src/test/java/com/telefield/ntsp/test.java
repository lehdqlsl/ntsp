package com.telefield.ntsp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefield.ntsp.config.AuthInterceptor;
import com.telefield.ntsp.config.CustomAuthenticationException;
import com.telefield.ntsp.config.InvalidTokenException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest()
@AutoConfigureMockMvc //need this in Spring Boot test
@AutoConfigureRestDocs
public class test {
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Autowired
    private WebApplicationContext context;

    // 테스트 시 interceptor 건너뛰기
    @MockBean
    AuthInterceptor interceptor;

    @BeforeEach
    void initTest() throws CustomAuthenticationException, InvalidTokenException {
        when(interceptor.preHandle(any(), any(), any())).thenReturn(true);
        // other stuff
    }
    // 테스트 시 interceptor 건너뛰기

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    @Test
    @Transactional
    public void 개통() throws Exception {
        JSONArray arr = new JSONArray();
        JSONObject sensor1 = new JSONObject();
        sensor1.put("macAddr", "00158d0001c8635c2");
        sensor1.put("devType", 18);
        sensor1.put("battery", 1);
        sensor1.put("network", 1);
        sensor1.put("sensitivity", 1);

        List<Integer> cntList = new ArrayList<>();
        cntList.add(15);
        cntList.add(55);
        cntList.add(115);
        cntList.add(35);
        JSONObject sensor2 = new JSONObject();
        sensor2.put("macAddr", "00158d0001c8635c3");
        sensor2.put("devType", 2);
        sensor2.put("battery", 1);
        sensor2.put("network", 1);
        sensor2.put("sensitivity", 1);
        sensor2.put("location", "room");
        sensor2.put("count", cntList);


        JSONObject sensor3 = new JSONObject();
        sensor3.put("macAddr", "00158d0001c8635c10");
        sensor3.put("devType", 10);
        sensor3.put("battery", 1);
        sensor3.put("network", 1);
        sensor3.put("sensitivity", 1);


        JSONObject sensor4 = new JSONObject();
        List<Integer> illuList = new ArrayList<>();
        illuList.add(15);
        illuList.add(55);
        illuList.add(115);
        illuList.add(35);
        illuList.add(35);
        illuList.add(35);
        illuList.add(15);
        illuList.add(55);
        illuList.add(115);
        illuList.add(35);
        illuList.add(35);
        illuList.add(35);
        illuList.add(15);
        illuList.add(55);
        illuList.add(115);
        illuList.add(35);
        illuList.add(35);
        illuList.add(35);
        illuList.add(15);
        illuList.add(55);
        illuList.add(115);
        illuList.add(35);
        illuList.add(35);
        illuList.add(35);

        List<Integer> humiList = new ArrayList<>();
        humiList.add(1234);
        humiList.add(2234);
        humiList.add(3234);
        humiList.add(4234);

        List<Integer> tempList = new ArrayList<>();
        tempList.add(1234);
        tempList.add(2234);
        tempList.add(3234);
        tempList.add(4234);

        sensor4.put("macAddr", "");
        sensor4.put("devType", 4);
        sensor4.put("temperature", tempList);
        sensor4.put("humidity", humiList);
        sensor4.put("illumination", illuList);

        arr.put(sensor1);
        arr.put(sensor2);
        arr.put(sensor3);
        arr.put(sensor4);

        JSONObject json = new JSONObject();
        json.put("phone", "01012341234");
        json.put("macAddr", "00158d0001c8635c11");
        json.put("devType", 0);
        json.put("power_state", 1);
        json.put("comm_state", 1);
        json.put("sensitivity", 1);
        json.put("regDate", "2021-01-05 12:12:12");
        json.put("fcmId", "fcmId");
        json.put("sensorCycle", arr);
        json.put("app_ver", "1.0.0N");
        json.put("fw_ver", "1.0");
        json.put("codiState", 1);

        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/open/v1/gateways/open")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isCreated())
                .andDo(document("gateway-open/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(

                        ),
                        requestFields(
                                fieldWithPath("phone").type(JsonFieldType.STRING).description("G/W 번호"),
                                fieldWithPath("macAddr").type(JsonFieldType.STRING).description("G/W 맥주소"),
                                fieldWithPath("devType").type(JsonFieldType.NUMBER).description("G/W 장비타입"),
                                fieldWithPath("power_state").type(JsonFieldType.NUMBER).description("전원 연결 상태"),
                                fieldWithPath("comm_state").type(JsonFieldType.NUMBER).description("LTE 네트워크 상태"),
                                fieldWithPath("sensitivity").type(JsonFieldType.NUMBER).description("LTE 통신 감도"),
                                fieldWithPath("regDate").type(JsonFieldType.STRING).description("G/W 주기보고 송신 시간"),
                                fieldWithPath("fcmId").type(JsonFieldType.STRING).description("FCM 키값"),
                                fieldWithPath("codiState").type(JsonFieldType.NUMBER).description("CODI 연결 상태"),
                                fieldWithPath("app_ver").type(JsonFieldType.STRING).description("G/W App 버전"),
                                fieldWithPath("fw_ver").type(JsonFieldType.STRING).description("F/W 버전"),
                                fieldWithPath("sensorCycle.[]devType").type(JsonFieldType.NUMBER).description("센서타입"),
                                fieldWithPath("sensorCycle.[]macAddr").type(JsonFieldType.STRING).description("센서 맥주소"),
                                fieldWithPath("sensorCycle.[]sensitivity").type(JsonFieldType.NUMBER).description("센서 통신 감도").optional(),
                                fieldWithPath("sensorCycle.[]battery").type(JsonFieldType.NUMBER).description("센서 배터리 상태").optional(),
                                fieldWithPath("sensorCycle.[]network").type(JsonFieldType.NUMBER).description("센서 통신 상태").optional(),
                                fieldWithPath("sensorCycle.[]location").description("센서 설치 위치 (선택)").optional(),
                                fieldWithPath("sensorCycle.[]count").description("활동량 (활동감지기)").optional(),
                                fieldWithPath("sensorCycle.[]temperature").type(JsonFieldType.ARRAY).description("온도 (온/습/조도 센서)").optional(),
                                fieldWithPath("sensorCycle.[]humidity").type(JsonFieldType.ARRAY).description("습도 (온/습/조도 센서)").optional(),
                                fieldWithPath("sensorCycle.[]illumination").description("조도 (온/습/조도 센서)").optional()
                        ))
                );
    }

    @Test
    @Transactional
    public void test2() throws Exception {
        JSONArray arr = new JSONArray();
        JSONObject sensor1 = new JSONObject();
        sensor1.put("macAddr", "00158d0001c8635c2");
        sensor1.put("devType", 18);
        sensor1.put("battery", 1);
        sensor1.put("network", 1);
        sensor1.put("sensitivity", 1);

        List<Integer> cntList = new ArrayList<>();
        cntList.add(15);
        cntList.add(55);
        cntList.add(115);
        cntList.add(35);
        JSONObject sensor2 = new JSONObject();
        sensor2.put("macAddr", "00158d0001c8635c3");
        sensor2.put("devType", 2);
        sensor2.put("battery", 1);
        sensor2.put("network", 1);
        sensor2.put("sensitivity", 1);
        sensor2.put("location", "room");
        sensor2.put("count", cntList);


        JSONObject sensor3 = new JSONObject();
        sensor3.put("macAddr", "00158d0001c8635c10");
        sensor3.put("devType", 10);
        sensor3.put("battery", 1);
        sensor3.put("network", 1);
        sensor3.put("sensitivity", 1);


        JSONObject sensor4 = new JSONObject();
        List<Integer> illuList = new ArrayList<>();
        illuList.add(15);
        illuList.add(55);
        illuList.add(115);
        illuList.add(35);
        illuList.add(35);
        illuList.add(35);
        illuList.add(15);
        illuList.add(55);
        illuList.add(115);
        illuList.add(35);
        illuList.add(35);
        illuList.add(35);
        illuList.add(15);
        illuList.add(55);
        illuList.add(115);
        illuList.add(35);
        illuList.add(35);
        illuList.add(35);
        illuList.add(15);
        illuList.add(55);
        illuList.add(115);
        illuList.add(35);
        illuList.add(35);
        illuList.add(35);

        List<Integer> humiList = new ArrayList<>();
        humiList.add(1234);
        humiList.add(2234);
        humiList.add(3234);
        humiList.add(4234);

        List<Integer> tempList = new ArrayList<>();
        tempList.add(1234);
        tempList.add(2234);
        tempList.add(3234);
        tempList.add(4234);

        sensor4.put("macAddr", "");
        sensor4.put("devType", 4);
        sensor4.put("temperature", tempList);
        sensor4.put("humidity", humiList);
        sensor4.put("illumination", illuList);

        arr.put(sensor1);
        arr.put(sensor2);
        arr.put(sensor3);
        arr.put(sensor4);

        JSONObject json = new JSONObject();
        json.put("phone", "01012341234");
        json.put("macAddr", "00158d0001c8635c11");
        json.put("devType", 0);
        json.put("power_state", 1);
        json.put("comm_state", 1);
        json.put("sensitivity", 1);
        json.put("regDate", "2021-01-05 12:12:12");
        json.put("fcmId", "fcmId");
        json.put("sensorCycle", arr);
        json.put("app_ver", "1.0.0N");
        json.put("fw_ver", "1.0");

        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/open/v1/gateways/cycle")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isCreated())
                .andDo(document("gateway-cycle/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(

                        ),
                        requestFields(
                                fieldWithPath("phone").type(JsonFieldType.STRING).description("G/W 번호"),
                                fieldWithPath("macAddr").type(JsonFieldType.STRING).description("G/W 맥주소"),
                                fieldWithPath("devType").type(JsonFieldType.NUMBER).description("G/W 장비타입"),
                                fieldWithPath("power_state").type(JsonFieldType.NUMBER).description("전원 연결 상태"),
                                fieldWithPath("comm_state").type(JsonFieldType.NUMBER).description("LTE 네트워크 상태"),
                                fieldWithPath("sensitivity").type(JsonFieldType.NUMBER).description("LTE 통신 감도"),
                                fieldWithPath("regDate").type(JsonFieldType.STRING).description("G/W 주기보고 송신 시간"),
                                fieldWithPath("fcmId").type(JsonFieldType.STRING).description("FCM 키값"),
                                fieldWithPath("app_ver").type(JsonFieldType.STRING).description("G/W App 버전"),
                                fieldWithPath("fw_ver").type(JsonFieldType.STRING).description("F/W 버전"),
                                fieldWithPath("sensorCycle.[]devType").type(JsonFieldType.NUMBER).description("센서타입"),
                                fieldWithPath("sensorCycle.[]macAddr").type(JsonFieldType.STRING).description("센서 맥주소"),
                                fieldWithPath("sensorCycle.[]sensitivity").type(JsonFieldType.NUMBER).description("센서 통신 감도").optional(),
                                fieldWithPath("sensorCycle.[]battery").type(JsonFieldType.NUMBER).description("센서 배터리 상태").optional(),
                                fieldWithPath("sensorCycle.[]network").type(JsonFieldType.NUMBER).description("센서 통신 상태").optional(),
                                fieldWithPath("sensorCycle.[]location").description("센서 설치 위치 (선택)").optional(),
                                fieldWithPath("sensorCycle.[]count").description("활동량 (활동감지기)").optional(),
                                fieldWithPath("sensorCycle.[]temperature").type(JsonFieldType.ARRAY).description("온도 (온/습/조도 센서)").optional(),
                                fieldWithPath("sensorCycle.[]humidity").type(JsonFieldType.ARRAY).description("습도 (온/습/조도 센서)").optional(),
                                fieldWithPath("sensorCycle.[]illumination").description("조도 (온/습/조도 센서)").optional()
                        ))
                );
    }

    @Test
    @Transactional
    public void 이벤트보고() throws Exception {
        JSONObject json = new JSONObject();
        JSONObject data = new JSONObject();
        data.put("location", "room");
        data.put("start", "2021-01-05 12:12:12");
        data.put("end", "2021-01-05 12:13:12");

        json.put("phone", "01012341234");
        json.put("macAddr", "A349SJJD");
        json.put("devType", 2);
        json.put("eventType", 8);
        json.put("regDate", "2021-01-05 12:12:12");
        json.put("data", data);

        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/open/v1/gateways/event")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON)
                .content(json.toString()))
                .andExpect(status().isCreated())
                .andDo(document("gateway-event/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(

                        ),
                        requestFields(
                                fieldWithPath("phone").type(JsonFieldType.STRING).description("G/W 번호"),
                                fieldWithPath("regDate").type(JsonFieldType.STRING).description("G/W 이벤트 송신 시간"),
                                fieldWithPath("eventType").type(JsonFieldType.NUMBER).description("이벤트 타입"),
                                fieldWithPath("macAddr").type(JsonFieldType.STRING).description("G/W or Sensor 맥주소"),
                                fieldWithPath("devType").type(JsonFieldType.NUMBER).description("센서 타입"),
                                fieldWithPath("data.location").type(JsonFieldType.STRING).description("센서 설치 위치").optional(),
                                fieldWithPath("data.start").type(JsonFieldType.STRING).description("119통화 시작 시간 [eventType=2]").optional(),
                                fieldWithPath("data.end").type(JsonFieldType.STRING).description("119통화 종료 시간 [eventType=2]").optional()
                        ))
                );
    }

    @Test
    public void 센서현황() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/gateways/{phone}/devices", "00158D000361247C ")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/phone/devices/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        responseFields(
                                fieldWithPath("[].phone").description("G/W 번호"),
                                fieldWithPath("[].macAddr").description("Mac 주소"),
                                fieldWithPath("[].devType").description("장비타입"),
                                fieldWithPath("[].power").description("전원"),
                                fieldWithPath("[].battery").description("배터리"),
                                fieldWithPath("[].network").description("통신상태"),
                                fieldWithPath("[].sensitivity").description("감도")
                        )));
    }

    @Test
    public void 로그요청() throws Exception {
        JSONObject data = new JSONObject();
        data.put("startDate", "2021-01-05");
        data.put("endDate", "2021-01-06");
        data.put("userId", "admin");

        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/gateways/{phone}/request-log", "00158D000361247C")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(data.toString())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/phone/request-log/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        requestFields(
                                fieldWithPath("startDate").description("로그 시작 날짜[yyyy-mm-dd]"),
                                fieldWithPath("endDate").description("로그 종료 날짜[yyyy-mm-dd]"),
                                fieldWithPath("userId").description("유저 아이디")
                        )));
    }

    @Test
    public void 원격개통() throws Exception {
        JSONObject data = new JSONObject();
        data.put("userId", "admin");

        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/gateways/{phone}/request-open", "00158D000361247C")
                .content(data.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/phone/request-open/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        requestFields(
                                fieldWithPath("userId").description("유저 아이디")
                        )));
    }

    @Test
    public void 원격주기보고() throws Exception {
        JSONObject data = new JSONObject();
        data.put("userId", "admin");
        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/gateways/{phone}/request-cycle", "00158D000361247C")
                .content(data.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/phone/request-cycle/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        requestFields(
                                fieldWithPath("userId").description("유저 아이디")
                        )));
    }

    @Test
    public void 응급상황해제() throws Exception {
        JSONObject data = new JSONObject();
        data.put("userId", "admin");
        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/gateways/{phone}/request-stop", "00158D000361247C")
                .content(data.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/phone/request-stop/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        requestFields(
                                fieldWithPath("userId").description("유저 아이디")
                        )
                ));
    }

    @Test
    public void 원격재부팅() throws Exception {
        JSONObject data = new JSONObject();
        data.put("userId", "admin");
        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/gateways/{phone}/request-reboot", "00158D000361247C")
                .content(data.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/phone/request-reboot/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        requestFields(
                                fieldWithPath("userId").description("유저 아이디")
                        )));
    }

    @Test
    public void 원격재실() throws Exception {
        JSONObject data = new JSONObject();
        data.put("userId", "admin");
        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/gateways/{phone}/request-indoor", "00158D000361247C")
                .content(data.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/phone/request-indoor/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        requestFields(
                                fieldWithPath("userId").description("유저 아이디")
                        )));
    }

    @Test
    public void 원격활동보고() throws Exception {
        JSONObject data = new JSONObject();
        data.put("userId", "admin");
        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/gateways/{phone}/request-act", "00158D000361247C")
                .content(data.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/phone/request-act/",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        requestFields(
                                fieldWithPath("userId").description("유저 아이디")
                        )));
    }

    @Test
    public void App업데이트() throws Exception {
        JSONObject data = new JSONObject();
        data.put("userId", "admin");

        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/gateways/{phone}/request-update/app/{version}", "00158D000361247C", "latest")
                .content(data.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/phone/request-update/app/version",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호"),
                                parameterWithName("version").description("App 버전(3.8.3 참고)")
                        ),
                        requestFields(
                                fieldWithPath("userId").description("유저 아이디")
                        )));
    }

    @Test
    public void fw업데이트() throws Exception {
        JSONObject data = new JSONObject();
        data.put("userId", "admin");

        this.mockMvc.perform(RestDocumentationRequestBuilders.post("/gateways/{phone}/request-update/fw/{version}", "00158D000361247C", "latest")
                .content(data.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("/gateways/phone/request-update/fw/version",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호"),
                                parameterWithName("version").description("FW 버전(3.9.3 참고)")
                        ),
                        requestFields(
                                fieldWithPath("userId").description("유저 아이디")
                        )));
    }
}