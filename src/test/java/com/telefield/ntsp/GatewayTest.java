package com.telefield.ntsp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefield.ntsp.config.AuthInterceptor;
import com.telefield.ntsp.config.CustomAuthenticationException;
import com.telefield.ntsp.config.InvalidTokenException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest()
@AutoConfigureMockMvc //need this in Spring Boot test
@AutoConfigureRestDocs
public class GatewayTest {
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Autowired
    private WebApplicationContext context;

    // 테스트 시 interceptor 건너뛰기
    @MockBean
    AuthInterceptor interceptor;

    @BeforeEach
    void initTest() throws CustomAuthenticationException, InvalidTokenException {
        when(interceptor.preHandle(any(), any(), any())).thenReturn(true);
        // other stuff
    }
    // 테스트 시 interceptor 건너뛰기

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    @Test
    public void 주기보고목록() throws Exception {
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/gateways/{phone}/cycles", "00158D0001EF0766")
                .param("startDate", "2021-03-07")
                .param("endDate", "2021-03-07"))
                .andExpect(status().isOk())
                .andDo(document("gateway-cycle/get",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        requestParameters(
                                parameterWithName("startDate").description("주기보고 검색 시작 날짜"),
                                parameterWithName("endDate").description("주기보고 검색 끝 날짜")
                        ),
                        responseFields(
                                fieldWithPath("[].id").type(JsonFieldType.NUMBER).description("주기보고 번호"),
                                fieldWithPath("[].phone").type(JsonFieldType.STRING).description("G/W 번호"),
                                fieldWithPath("[].macAddr").type(JsonFieldType.STRING).description("G/W 맥주소"),
                                fieldWithPath("[].devType").type(JsonFieldType.NUMBER).description("G/W 장비타입"),
                                fieldWithPath("[].battery").type(JsonFieldType.NUMBER).description("배터리"),
                                fieldWithPath("[].power").type(JsonFieldType.NUMBER).description("전원 연결 상태"),
                                fieldWithPath("[].network").type(JsonFieldType.NUMBER).description("LTE 네트워크 상태 (사용X)"),
                                fieldWithPath("[].sensitivity").type(JsonFieldType.NUMBER).description("LTE 통신 감도"),
                                fieldWithPath("[].regDate").type(JsonFieldType.STRING).description("서버 등록 시간"),
                                fieldWithPath("[].send_reg_date").type(JsonFieldType.STRING).description("G/W 주기보고 t송신 시간"),
                                fieldWithPath("[].app_ver").type(JsonFieldType.STRING).description("G/W App 버전"),
                                fieldWithPath("[].fw_ver").type(JsonFieldType.STRING).description("F/W 버전"),
                                fieldWithPath("[].codiState").type(JsonFieldType.NUMBER).description("CODI 연결 상태")
                        ))
                );
    }

    @Test
    public void 게이트웨이상태변경() throws Exception {
        JSONObject json = new JSONObject();
        json.put("status", 0);

        this.mockMvc.perform(RestDocumentationRequestBuilders.put("/gateways/{phone}/change-status", "00158D0001EF0766")
                .content(json.toString()))
                .andExpect(status().isOk())
                .andDo(document("gateway/change-status",
                        preprocessRequest(modifyUris()
                                        .scheme("http")
                                        .host("14.47.229.122")
                                        .port(8443)
                                , prettyPrint()
                        ),
                        preprocessResponse(prettyPrint()),
                        pathParameters(
                                parameterWithName("phone").description("G/W 번호")
                        ),
                        requestFields(
                                fieldWithPath("status").description("G/W 상태 (1:점검, 0:해제)").type((JsonFieldType.NUMBER))
                        ))
                );
    }
}