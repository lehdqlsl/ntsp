package com.telefield.ntsp.security;

import java.util.Date;

public interface AuthTokenProvider<T> {
    T createAccessAuthToken(String id, String role);
    T createRefreshAuthToken(String id, String role);
    T convertAccessToken(String token);
    T convertRefreshToken(String token);
}