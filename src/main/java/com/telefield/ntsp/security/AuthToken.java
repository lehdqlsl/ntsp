package com.telefield.ntsp.security;

public interface AuthToken<T> {
    boolean validate();
    T getData();
}