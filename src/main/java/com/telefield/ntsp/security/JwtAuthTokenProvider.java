package com.telefield.ntsp.security;

import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;

import java.security.Key;
import java.util.Date;

@Slf4j
public class JwtAuthTokenProvider implements AuthTokenProvider<AuthToken> {

    private final Key key;

    public JwtAuthTokenProvider(String secret) {
        this.key = Keys.hmacShaKeyFor(secret.getBytes());
    }

    @Override
    public JwtAccessToken createAccessAuthToken(String id, String role) {
        return new JwtAccessToken(id, role, key);
    }

    @Override
    public JwtRefreshToken createRefreshAuthToken(String id, String role) {
        return new JwtRefreshToken(id, role, key);
    }

    @Override
    public JwtAccessToken convertAccessToken(String token) {
        return new JwtAccessToken(token, key);
    }

    @Override
    public JwtRefreshToken convertRefreshToken(String token) {
        return new JwtRefreshToken(token, key);
    }

    public TokenBundle createAuthToken(String id, String role){
        return TokenBundle.builder()
                .jwtAccessToken(new JwtAccessToken(id, role, key))
                .jwtRefreshToken(new JwtRefreshToken(id, role, key))
                .build();
    }
}