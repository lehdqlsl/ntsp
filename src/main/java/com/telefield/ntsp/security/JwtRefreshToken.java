package com.telefield.ntsp.security;

import io.jsonwebtoken.*;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.security.Key;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

@Slf4j
public class JwtRefreshToken implements AuthToken<Claims> {

    @Getter
    private final String refreshToken;

    private final Key key;

    private static final String AUTHORITIES_KEY = "role";

    JwtRefreshToken(String id, String role, Key key) {
        this.key = key;
        this.refreshToken = createJwtAuthToken(id, role, Date.from(ZonedDateTime.now().plusDays(60).toInstant())).get();
        //this.refreshToken = createJwtAuthToken(id, role, Date.from(ZonedDateTime.now().plusMinutes(10).toInstant())).get();
    }

    JwtRefreshToken(String token, Key key) {
        this.key = key;
        this.refreshToken = token;
    }


    @Override
    public boolean validate() {
        return getData() != null;
    }

    @Override
    public Claims getData() {
        try {
            return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(refreshToken).getBody();
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | io.jsonwebtoken.security.SignatureException | IllegalArgumentException e) {
            log.info(e.getMessage());
            return null;
        }
    }

    // 토큰의 유효성 + 만료일자 확인
    public boolean isExpire(String jwtToken) {
        Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jwtToken);
        if(claims.getBody().getExpiration().after(new Date())){
            return true;
        }else{
            return false;
        }
    }

    // 토큰의 유효성 + 만료일자 확인
    public boolean isExpireOneMonth() {
        Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(this.refreshToken);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, -1);

        if(claims.getBody().getExpiration().before(calendar.getTime())){
            return true;
        }else{
            return false;
        }
    }

    private Optional<String> createJwtAuthToken(String id, String role, Date expiredDate) {
        String token = Jwts.builder()
                .setSubject(id)
                .claim(AUTHORITIES_KEY, role)
                .signWith(key, SignatureAlgorithm.HS256)
                .setExpiration(expiredDate)
                .compact();
        return Optional.ofNullable(token);
    }
}