package com.telefield.ntsp.security;

import io.jsonwebtoken.*;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

@Slf4j
public class JwtAccessToken implements AuthToken<Claims> {

    @Getter
    private final String accessToken;

    private final Key key;

    private static final String AUTHORITIES_KEY = "role";

    JwtAccessToken(String id, String role, Key key) {
        this.key = key;
        this.accessToken = createJwtAuthToken(id, role, Date.from(ZonedDateTime.now().plusHours(12).toInstant())).get();
        //this.accessToken = createJwtAuthToken(id, role, Date.from(ZonedDateTime.now().plusMinutes(5).toInstant())).get();
    }

    JwtAccessToken(String token, Key key) {
        this.key = key;
        this.accessToken = token;
    }

    @Override
    public boolean validate() {
        try{
            return getData() != null;
        }catch (Exception e){
            throw e;
        }
    }

    @Override
    public Claims getData() {
        try {
            return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(accessToken).getBody();
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | io.jsonwebtoken.security.SignatureException | IllegalArgumentException e) {
            log.info(e.getMessage());
            throw e;
        }
    }

    // 토큰의 유효성 + 만료일자 확인
    public boolean isExpire(String jwtToken) {
        Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jwtToken);
        if(claims.getBody().getExpiration().after(new Date())){
            return true;
        }else{
            return false;
        }
    }

    private Optional<String> createJwtAuthToken(String id, String role, Date expiredDate) {
        String token = Jwts.builder()
                .setSubject(id)
                .claim(AUTHORITIES_KEY, role)
                .signWith(key, SignatureAlgorithm.HS256)
                .setExpiration(expiredDate)
                .compact();
        return Optional.ofNullable(token);
    }
}