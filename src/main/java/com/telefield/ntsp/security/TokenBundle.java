package com.telefield.ntsp.security;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TokenBundle {
    private JwtAccessToken jwtAccessToken;
    private JwtRefreshToken jwtRefreshToken;

    TokenBundle(JwtAccessToken jwtAccessToken, JwtRefreshToken jwtRefreshToken){
        this.jwtAccessToken = jwtAccessToken;
        this.jwtRefreshToken = jwtRefreshToken;
    }
}