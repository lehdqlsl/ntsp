package com.telefield.ntsp.config;

import com.telefield.ntsp.security.JwtAccessToken;
import com.telefield.ntsp.security.JwtAuthTokenProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class AuthInterceptor implements HandlerInterceptor {
    final static String AUTHORIZATION_HEADER = "token";

    private final JwtAuthTokenProvider jwtAuthTokenProvider;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws InvalidTokenException, CustomAuthenticationException {
        log.debug("URL:{}, IP: {}", request.getRequestURI(), request.getRemoteAddr());
        String token = resolveToken(request).orElse(null);

        if (request.getMethod().equals("OPTIONS")) {
            return true;
        }

        if(token == null){
            throw new CustomAuthenticationException("token is null value");
        }

        if (token != null) {
            Enumeration<String> headerNames = request.getHeaderNames();
            while(headerNames.hasMoreElements()) {
                String headerName = headerNames.nextElement();
                String headerValue = request.getHeader(headerName);
                log.debug(headerName + " : " + headerValue);
            }

            try {
                JwtAccessToken jwtAuthToken = jwtAuthTokenProvider.convertAccessToken(token);
                if (jwtAuthToken.validate()) {
                    return true;
                }
            } catch (Exception e) {
                log.info("token:" + token);
                log.info("msg:" + e.getMessage());
                throw new InvalidTokenException(e.getMessage());
            }
        }else{
            return false;
        }
        return false;
    }

    private Optional<String> resolveToken(HttpServletRequest request) {
        String authToken = request.getHeader(AUTHORIZATION_HEADER);
        if (StringUtils.hasText(authToken)) {
            return Optional.of(authToken);
        } else {
            return Optional.empty();
        }
    }
}

