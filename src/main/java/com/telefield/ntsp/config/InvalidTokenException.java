package com.telefield.ntsp.config;

public class InvalidTokenException extends Exception {

    public InvalidTokenException(){
        super(ErrorCode.AUTHENTICATION_FAILED.getMessage()
        );
    }

    public InvalidTokenException(String msg){
        super(msg);
    }
}