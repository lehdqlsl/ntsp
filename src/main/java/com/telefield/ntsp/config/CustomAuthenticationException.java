package com.telefield.ntsp.config;


public class CustomAuthenticationException extends Exception {
    public CustomAuthenticationException(){
        super(ErrorCode.AUTHENTICATION_FAILED.getMessage());
    }

    public CustomAuthenticationException(String msg){
        super(msg);
    }

    public CustomAuthenticationException(Exception ex){
        super(ex);
    }
}
