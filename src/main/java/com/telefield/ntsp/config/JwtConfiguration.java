package com.telefield.ntsp.config;

import com.telefield.ntsp.security.JwtAuthTokenProvider;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.crypto.SecretKey;

@Configuration
public class JwtConfiguration implements WebMvcConfigurer {
    @Bean
    public JwtAuthTokenProvider jwtProvider() {
        SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS256); //or HS384 or HS512
        return new JwtAuthTokenProvider(key.toString());
    }
}
