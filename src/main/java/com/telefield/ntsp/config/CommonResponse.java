package com.telefield.ntsp.config;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class CommonResponse {
    private String message;
    private int status;
    private String code;
}
