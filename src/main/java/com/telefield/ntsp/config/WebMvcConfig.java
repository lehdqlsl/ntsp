package com.telefield.ntsp.config;

import com.telefield.ntsp.security.JwtAuthTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@RequiredArgsConstructor
public class WebMvcConfig implements WebMvcConfigurer {

    private final AuthInterceptor authInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor)
                .addPathPatterns("/gateways/**")
                .addPathPatterns("/gateway/**")
                .addPathPatterns("/sensors/**")
                .addPathPatterns("/events/**")
                .excludePathPatterns("/login")
                .excludePathPatterns("/auth")
                .excludePathPatterns("/open/v1/**");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
                /*.allowedOrigins("http://localhost:8080")*/
                /*.exposedHeaders("token");*/
    }
}
