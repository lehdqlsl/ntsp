package com.telefield.ntsp.controller;

import com.telefield.ntsp.domain.admin.gateway_failure_action.GatewayFailureAction;
import com.telefield.ntsp.domain.admin.gateway_failure_action.GatewayFailureActionDto;
import com.telefield.ntsp.domain.admin.gateway_failure_action.GatewayFailureActionRepository;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureRepository;
import com.telefield.ntsp.service.FailureService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class FailureController {
    final FailureService failureService;
    final GatewayFailureRepository gatewayFailureRepository;
    final GatewayFailureActionRepository gatewayFailureActionRepository;

    Logger logger = LoggerFactory.getLogger(FailureController.class);

    @GetMapping("/gateways/failure/{failure_id}")
    public GatewayFailure GatewayEnvironment(@PathVariable long failure_id) {
        return failureService.getGatewayFailure(failure_id);
    }

    @PostMapping("/gateways/failure/action")
    public List<GatewayFailureAction> GatewayFailureAction(@RequestBody GatewayFailureActionDto gatewayFailureActionDto) {
        //장애 내역 검색
        GatewayFailure gatewayFailure = gatewayFailureRepository.findById(gatewayFailureActionDto.getFailureId().getFailureId()).orElse(null);
        GatewayFailureAction gatewayFailureAction = gatewayFailureActionDto.toEntity(gatewayFailure);

        // 장애 내역 업데이트
        if (gatewayFailureActionDto.isAware() == true) {
            gatewayFailure.setAware(true);
            gatewayFailureRepository.save(gatewayFailure);
            gatewayFailureAction.setTitle("처리완료");
        }
        gatewayFailureActionRepository.save(gatewayFailureAction);

        // 수동 클로즈
        if (gatewayFailureActionDto.isClosed() == true) {
            failureService.deviceFailureClose(gatewayFailure,false, gatewayFailureActionDto.getUser());
        }

        if (gatewayFailure != null) {
            return gatewayFailureActionRepository.findByFailureIdOrderBySvRegDateDesc(gatewayFailure);
        } else {
            return null;
        }
    }

    @GetMapping("/gateways/failure/actions/{failureId}")
    public List<GatewayFailureAction> getGatewayFailureActionList(@PathVariable long failureId) {
        GatewayFailure gatewayFailure = gatewayFailureRepository.findById(failureId).orElse(null);
        if (gatewayFailure != null) {
            return gatewayFailureActionRepository.findByFailureIdOrderBySvRegDateDesc(gatewayFailure);
        } else {
            return null;
        }
    }

    @GetMapping("/gateways/failures/{failureId}")
    public List<GatewayFailure> getGatewayFailureList(@PathVariable long failureId) {
        GatewayFailure gatewayFailure = gatewayFailureRepository.findById(failureId).orElse(null);
        return gatewayFailureRepository.findByPhoneOrderByRegDateDesc(gatewayFailure.getPhone());
    }

    @GetMapping("/gateways/{phone}/failures")
    public List<GatewayFailure> getGatewayFailureList(@PathVariable String phone) {
        logger.info("GET, /gateways/{}/failures", phone);
        return gatewayFailureRepository.findByPhoneOrderByRegDateDesc(phone);
    }

    @GetMapping("/emergency/{failure_id}")
    public GatewayFailure getEmergency(@PathVariable long failure_id) {
        return failureService.getGatewayFailure(failure_id);
    }


}
