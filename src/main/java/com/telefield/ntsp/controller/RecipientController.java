package com.telefield.ntsp.controller;

import com.telefield.ntsp.domain.admin.gateway_failure_action.GatewayFailureAction;
import com.telefield.ntsp.domain.admin.gateway_failure_action.GatewayFailureActionRepository;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureQueryRepository;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureRepository;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusRepository;
import com.telefield.ntsp.domain.event.emergency.EmergencyQueryRepository;
import com.telefield.ntsp.domain.recipient.info.RecipientInfo;
import com.telefield.ntsp.domain.recipient.info.RecipientInfoDto;
import com.telefield.ntsp.domain.recipient.info.RecipientInfoRepository;
import com.telefield.ntsp.domain.recipient.notice.Notice;
import com.telefield.ntsp.domain.recipient.notice.NoticeRepository;
import com.telefield.ntsp.domain.recipient.numbers.Numbers;
import com.telefield.ntsp.domain.recipient.qrheck.QRCheck;
import com.telefield.ntsp.domain.recipient.qrheck.QRCheckRepository;
import com.telefield.ntsp.domain.recipient.status.RecipientStatus;
import com.telefield.ntsp.domain.recipient.status.RecipientStatusRepository;
import com.telefield.ntsp.service.FCMService;
import com.telefield.ntsp.service.FileService;
import com.telefield.ntsp.service.RecipientService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class RecipientController {
    private static final String IMAGE_DIR = "/images/";

    final RecipientService recipientService;
    final RecipientInfoRepository recipientInfoRepository;
    final NoticeRepository notiRepo;
    final GatewayStatusRepository gatewayStatusRepository;
    final FCMService fcmService;
    final QRCheckRepository qrCheckRepository;
    final GatewayFailureRepository gatewayFailureRepository;
    final GatewayFailureQueryRepository gatewayFailureQueryRepository;
    final EmergencyQueryRepository emergencyQueryRepository;
    final GatewayFailureActionRepository gatewayFailureActionRepository;
    final RecipientStatusRepository recipientStatusRepository;
    final FileService fileService;

    Logger logger = LoggerFactory.getLogger(RecipientController.class);

    @GetMapping("/recipients/{phone}")
    public RecipientInfoDto GatewayEvent(@PathVariable String phone) {
        RecipientInfo recipientInfo = recipientInfoRepository.findById(phone).orElse(null);
        RecipientStatus recipientStatus = recipientStatusRepository.findById(phone).orElse(null);
        GatewayStatusEntity gatewayStatusEntity = gatewayStatusRepository.findById(phone).orElse(null);

        if (recipientInfo == null || recipientStatus == null || gatewayStatusEntity == null) {
            return null;
        }

        return recipientInfo.toEntity(recipientInfo,recipientStatus,gatewayStatusEntity);
    }


    @PostMapping("/recipients")
    public RecipientInfo saveRecipientInfo(@RequestBody RecipientInfoDto recipientInfoDto) {
        RecipientInfo recipientInfo = recipientInfoRepository.findById(recipientInfoDto.getPhone()).orElse(null);
        RecipientInfo setRecipientInfo = recipientInfoDto.toEntity();
        if (recipientInfo != null) {
            setRecipientInfo.setImageFile(recipientInfo.getImageFile());
            setRecipientInfo.setRegDate(recipientInfo.getRegDate());
        }
        return recipientInfoRepository.save(setRecipientInfo);
    }

    @GetMapping(value = "/recipient/notices")
    public ResponseEntity<?> Notice_list1(HttpServletRequest request) {
        logger.info("GET, /recipient/notices");

        List<Notice> list = null;

        try {
            int type = Integer.parseInt(request.getParameter("type"));

            // 0: 일반
            // 1: 긴급
            // 2: 전체
            if (type == 2) {
                list = notiRepo.findAllByOrderBySeqDesc();
            } else {
                list = notiRepo.findByTypeOrderBySeqDesc(type);
            }
        } catch (Exception e) {
            return new ResponseEntity<List<Notice>>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<List<Notice>>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/recipient/notices/latest")
    public ResponseEntity<?> NoticeLatest(HttpServletRequest request) {
        logger.info("GET, /recipient/notices/latest");

        Notice notice = notiRepo.findTopByOrderBySeqDesc().orElse(null);

        return ResponseEntity.ok(notice);
    }

    @DeleteMapping(value = "/recipient/notices/{seq}")
    public ResponseEntity<?> DeleteNotice(@PathVariable int seq) {
        logger.info("DELETE, /recipient/notices/{seq}", seq);

        try {
            notiRepo.deleteById(seq);
        } catch (Exception e) {
            return new ResponseEntity<List<Notice>>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/recipient/notices")
    public ResponseEntity<?> NoticeReg(HttpServletRequest request, MultipartFile uploadFile, Notice notice) {
        logger.info("POST, /recipient/notices");
        logger.info("notice:"+notice);
        List<Notice> list = null;

        try {
            // 저장
            Notice ret = notiRepo.save(notice);
            String type = fileService.SaveNoticeFile(uploadFile,ret.getSeq());

            // Push
            List<String> FcmIdSet = new ArrayList<>();
            List<GatewayStatusEntity> gatewayList = gatewayStatusRepository.findAll();
            for (GatewayStatusEntity gateway : gatewayList) {
                FcmIdSet.add(gateway.getFcmId());
            }

            ret.setCategory(type);
            ret = notiRepo.save(notice);
            fcmService.sendNotice(ret, FcmIdSet, type);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/recipient/notice/{seq}", method = RequestMethod.GET)
    public ResponseEntity<?> Notice_list2(HttpServletRequest request, @PathVariable int seq) {
        String m2mOrigin = request.getHeader("X-M2M-Origin");
        String m2mRI = request.getHeader("X-M2M-RI");

        Notice notice = notiRepo.findById(seq).orElse(null);

        return new ResponseEntity<Notice>(notice, HttpStatus.OK);
    }

    @PostMapping(value = "/recipient/qrcheck")
    public ResponseEntity<?> POST_QRCHECK(@RequestBody QRCheck qrCheck) {
        qrCheckRepository.save(qrCheck);

        GatewayFailure gatewayFailure = gatewayFailureQueryRepository.getFailureByPhone(qrCheck.getPhone());

        if (gatewayFailure != null) {
            GatewayFailureAction close = GatewayFailureAction.builder().failureId(gatewayFailure).text("생활관리사 방문(" + qrCheck.getUserId() + ")").title("해결됨").build();
            gatewayFailureActionRepository.save(close);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/recipient/qrchecks")
    public List<QRCheck> GET_QRCHECK() {
        return qrCheckRepository.findAllByOrderByIdDesc();
    }

    // 이미지 업로드
    @PostMapping(value = "/gateways/{phone}/profile")
    public ResponseEntity<RecipientInfo> uploadImage(MultipartFile file, @PathVariable String phone) {
        // Get a client http header
        logger.info("POST, /gateways/{}/profile", phone);

        List<RecipientInfo> recipientInfo = recipientInfoRepository.findByPhone(phone);

        if (file.isEmpty() || recipientInfo.isEmpty()) {
            return new ResponseEntity<RecipientInfo>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }

        // Make the directory "contents"
        Path currentWorkingDir = Paths.get("").toAbsolutePath();
        String path = currentWorkingDir.normalize().toString() + IMAGE_DIR;

        File dir = new File(path);

        try {
            if (!dir.exists()) {
                dir.mkdir();
            }
        } catch (SecurityException se) {
            return new ResponseEntity<RecipientInfo>(new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // Upload the file
        BufferedOutputStream stream = null;
        String fileName = "";
        try {
            byte[] bytes = file.getBytes();
            fileName = phone + "." + file.getOriginalFilename().split("\\.")[1];
            File uploadFile = new File(dir.getAbsolutePath() + File.separator + fileName);

            stream = new BufferedOutputStream(new FileOutputStream(uploadFile));
            stream.write(bytes);
            stream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<RecipientInfo>(new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        recipientInfo.get(0).setImageFile(fileName);
        RecipientInfo recipientInfo1 = recipientInfoRepository.save(recipientInfo.get(0));

        return new ResponseEntity<RecipientInfo>(recipientInfo1, HttpStatus.OK);
    }

    @GetMapping("/recipients/{phone}/numbers")
    public Numbers saveRecipientInfo(@PathVariable String phone) {
        logger.info("GET, recipients/{}/numbers", phone);
        return recipientService.getNumbers(phone);
    }
}

