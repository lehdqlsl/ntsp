package com.telefield.ntsp.controller;

import com.telefield.ntsp.define.DeviceType;
import com.telefield.ntsp.define.EventType;
import com.telefield.ntsp.security.JwtAuthTokenProvider;
import com.telefield.ntsp.service.LoginService;
import com.telefield.ntsp.service.SensorDataService;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class DataController {

    private final LoginService loginService;
    private final JwtAuthTokenProvider jwtAuthTokenProvider;
    private final SensorDataService sensorDataService;

    Logger logger = LoggerFactory.getLogger(DataController.class);

    @GetMapping("/gateways/{phone}/sensors/{type}/data")
    public ResponseEntity<?> login(@PathVariable String phone, @PathVariable byte type, @RequestParam(defaultValue = "0") int dateType) {
        logger.info("GET, /gateways/{}/sensors/{}/data, dateType={}", phone, type, dateType);
        JSONObject json = new JSONObject();
        JSONObject ret = new JSONObject();
        if (dateType > 4) {
            json.put("message", dateType + " is Wrong date Type");
            return ResponseEntity.badRequest().body(json.toString());
        }

        switch (type) {
            case DeviceType.ENVIRONMENT_SENSOR:
                ret = sensorDataService.getEnvData(phone, dateType);
                break;

            case DeviceType.ACTIVITY_SENSOR:
                ret = sensorDataService.getActData(phone, dateType);
                break;

            case DeviceType.RADAR_SENSOR:
                ret = sensorDataService.getRadarData(phone, LocalDate.now().atStartOfDay());
                break;

            default:
                json.put("message", type + " is Not Supported Sensor Type");
                return ResponseEntity.badRequest().body(json.toString());
        }

        return ResponseEntity.ok().body(ret.toString());
    }

    @GetMapping("/gateways/{phone}/stats/{eventType}")
    public ResponseEntity<?> stats(@PathVariable String phone, @PathVariable byte eventType, @RequestParam int dateType) {
        logger.info("GET, /gateways/{}/stats/{}, dateType={}", phone, eventType, dateType);
        JSONObject obj = new JSONObject();
        switch (eventType) {
            case EventType.NO_ACTIVITY:
            case EventType.GO_OUT:
                obj = sensorDataService.getInActData(phone,eventType,dateType);
                break;
            default:
                obj.put("message", eventType + " is Not Supported Event Type");
                return ResponseEntity.badRequest().body(obj.toString());
        }
        return ResponseEntity.ok().body(obj.toString());
    }

    @GetMapping("/gateways/{phone}/sensor-data")
    public ResponseEntity<?> sensorDate(@PathVariable String phone) {
        logger.info("GET, /gateways/{phone}/sensor-data", phone);
        JSONObject ret = sensorDataService.getSensorDataOne(phone);
        return ResponseEntity.ok().body(ret.toString());
    }
}
