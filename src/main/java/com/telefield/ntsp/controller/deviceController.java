package com.telefield.ntsp.controller;

import com.telefield.ntsp.define.FcmType;
import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.define.RemoteType;
import com.telefield.ntsp.domain.admin.account.User;
import com.telefield.ntsp.domain.admin.account.UserRepository;
import com.telefield.ntsp.domain.admin.remote.RemoteRequest;
import com.telefield.ntsp.domain.admin.remote.RemoteRequestRepository;
import com.telefield.ntsp.domain.device.DeviceDto;
import com.telefield.ntsp.domain.device.DevicesDto;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusQueryRepository;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusRepository;
import com.telefield.ntsp.domain.device.gw_cycle.Cycle;
import com.telefield.ntsp.domain.device.gw_cycle.CycleQueryRepository;
import com.telefield.ntsp.domain.device.gw_cycle.CycleRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.SensorCycle;
import com.telefield.ntsp.domain.device.sensor_cycle.SensorCycleRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentRepository;
import com.telefield.ntsp.domain.device.sensor_failure.SensorFailure;
import com.telefield.ntsp.domain.device.sensor_status.SensorStatusEntity;
import com.telefield.ntsp.domain.device.sensor_status.SensorStatusRepository;
import com.telefield.ntsp.service.DeviceService;
import com.telefield.ntsp.service.EventService;
import com.telefield.ntsp.service.FCMService;
import com.telefield.ntsp.service.RecipientService;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class deviceController {
    final DeviceService deviceService;
    final EventService eventService;
    final RecipientService recipientService;
    final CycleRepository cycleRepository;
    final EnvironmentRepository environmentRepository;
    final GatewayStatusQueryRepository gatewayStatusQueryRepository;
    final CycleQueryRepository cycleQueryRepository;
    final GatewayStatusRepository gatewayStatusRepository;
    final SensorStatusRepository sensorStatusRepository;
    final FCMService fcmService;
    final SensorCycleRepository sensorCycleRepository;
    final UserRepository userRepository;
    final RemoteRequestRepository remoteRequestRepository;

    Logger logger = LoggerFactory.getLogger(deviceController.class);

    @GetMapping("/sensors/count")
    public HashMap<Byte, HashMap<String, Long>> getSensorCount() {
        return deviceService.getSensorCount();
    }

    @GetMapping("/sensors/fail")
    public List<SensorFailure> getSensorFailList() {
        return deviceService.getSensorFailList();
    }

    @GetMapping("/gateways/fail")
    public List<GatewayFailure> getGWFailList() {
        return deviceService.getCurrentFailureGateway();
    }

    @GetMapping("/gateways/failures")
    public List<GatewayFailure> getGWFailList2() {
        return deviceService.getCurrentFailureGateway();
    }

    @GetMapping("/gateways/as")
    public List<GatewayStatusEntity> getGWASList() {
        return deviceService.getASGateway();
    }

    @GetMapping("/gateways/count")
    public HashMap<GatewayStatus, Long> TotalGatewayStatusCount() {
        return deviceService.getTotalGatewayStatusCount();
    }


    @GetMapping("/gateways/{phone}/environment")
    public HashMap<String, List<HashMap<String, Object>>> GatewayEnvironment(@PathVariable String phone) {
        return eventService.getEnvironmentList(phone);
    }

    @GetMapping("/gateways/{phone}/status")
    public List<DeviceDto> GatewayStatus(@PathVariable String phone) {
        return deviceService.getDeviceStatus(phone);
    }

    @GetMapping("/gateways/{phone}/cycles")
    public List<Cycle> GatewayCycle(@PathVariable String phone, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate) {
        logger.info("GET, /gateways/{}/cycles", phone);
        return cycleQueryRepository.getCycleList(phone, startDate, endDate);
    }

    @GetMapping("/sensors/cycles/{cycleId}")
    public List<SensorCycle> SensorCycle(@PathVariable long cycleId) {
        return sensorCycleRepository.findByCycleId(cycleId);
    }


    @GetMapping("/gateways/all")
    public List<GatewayStatusEntity> findGatewayAll() {
        return gatewayStatusRepository.findAll();
    }


    @PostMapping(value = "/gateways/{phone}/request-log")
    public ResponseEntity<Void> RequestGatewayLog(@PathVariable("phone") String phone, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/request-log, POST", phone);

        JSONObject json = new JSONObject(str);
        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            String startDate = json.getString("startDate");
            String endDate = json.getString("endDate");

            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.LOG, startDate + "~" + endDate);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.requestLog(startDate, endDate, findGateway.getFcmId(), remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/request-open")
    public ResponseEntity<Void> RequestGatewayOpen(@PathVariable("phone") String phone, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/request-open, GET", phone);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.OPEN);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_OPEN, "", remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/request-cycle")
    public ResponseEntity<Void> RequestGatewayCycle(@PathVariable("phone") String phone, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/request-cycle, GET", phone);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.CYCLE);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_CYCLE, "", remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/request-stop")
    public ResponseEntity<Void> RequestGatewayStop(@PathVariable("phone") String phone, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/request-stop, GET", phone);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.CLEAR);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_STOP, "", remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/request-reboot")
    public ResponseEntity<Void> RequestGatewayReboot(@PathVariable("phone") String phone, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/request-reboot, GET", phone);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.REBOOT);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_REBOOT, "", remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/request-indoor")
    public ResponseEntity<Void> RequestGatewayInDoor(@PathVariable("phone") String phone, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/request-indoor, GET", phone);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.INDOOR);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_INDOOR, "", remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/request-act")
    public ResponseEntity<Void> RequestGatewayAct(@PathVariable("phone") String phone, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/request-act, GET", phone);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.ACTIVITY);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_ACT, "", remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/request-update/app/{version}")
    public ResponseEntity<Void> RequestGatewayUpdateApp(@PathVariable("phone") String phone, @PathVariable("version") String version, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/request-update/app/{}, GET", phone, version);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.GW_UPDATE, version);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_UPDATE_APP, version, remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/request-update/fw/{version}")
    public ResponseEntity<Void> RequestGatewayUpdateFW(@PathVariable("phone") String phone, @PathVariable("version") String version, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/request-update/fw/{}, GET", phone, version);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.FW_UPDATE, version);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_UPDATE_FW, version, remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/change-inactive/{time}")
    public ResponseEntity<Void> RequestGatewayChangeInactive(@PathVariable("phone") String phone, @PathVariable("time") String time, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/change-inactive/{}, GET", phone, time);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.CHANGE_INACTIVE, time);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_CHANGE_INACTIVE_TIME, time, remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/change-cycle/{time}")
    public ResponseEntity<Void> RequestGatewayChangeCycle(@PathVariable("phone") String phone, @PathVariable("time") String time, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/change-cycle/{}, GET", phone, time);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.CHANGE_CYCLE, time);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_CHANGE_CYCLE_TIME, time, remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/change-mode/{mode}")
    public ResponseEntity<Void> RequestGatewayChangeMode(@PathVariable("phone") String phone, @PathVariable("mode") String mode, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/change-mode/{}, POST", phone, mode);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.CHANGE_MODE, mode);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_MODE_CHANGE, mode, remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/request-home")
    public ResponseEntity<Void> RequestGatewayChangeMode(@PathVariable("phone") String phone, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/request-home, POST", phone);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.HOME_DISPLAY);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_HOME_DISPLAY, "", remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping(value = "/gateways/{phone}/request-sensor-cycle/{sensor}/{time}")
    public ResponseEntity<Void> RequestGatewayChangeMode(@PathVariable("phone") String phone,
                                                         @PathVariable("sensor") String sensor,
                                                         @PathVariable("time") String time,
                                                         @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/request-sensor-cycle/{}/{}, POST", phone);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            RemoteRequest remoteRequest = saveRemote(str, phone, RemoteType.CHANGE_CYCLE_SENSOR);
            remoteRequest = remoteRequestRepository.save(remoteRequest);
            fcmService.fcmRequest(findGateway.getFcmId(), FcmType.PUSH_CHANGE_SENSOR_CYCLE, sensor + ";" + time, remoteRequest.getSeq());
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @GetMapping("/gateways/{phone}/devices")
    public List<DevicesDto> findDevices(@PathVariable String phone) {
        List<DevicesDto> list = new ArrayList<>();

        List<SensorStatusEntity> sensorList = sensorStatusRepository.findByPhone(phone);
        GatewayStatusEntity gw = gatewayStatusRepository.findById(phone).orElse(null);

        if (gw != null) {
            list.add(DevicesDto.builder()
                    .phone(gw.getPhone())
                    .devType((byte) 0)
                    .macAddr(gw.getMac_addr())
                    .network(gw.getNetwork())
                    .power(gw.getPower())
                    .battery(gw.getBattery())
                    .sensitivity(gw.getSensitivity())
                    .build()
            );
        }

        for (SensorStatusEntity entity : sensorList) {
            list.add(DevicesDto.builder()
                    .phone(entity.getPhone())
                    .devType(entity.getDevType())
                    .macAddr(entity.getMacAddr())
                    .network(entity.getNetwork())
                    .battery(entity.getBattery())
                    .sensitivity(entity.getSensitivity())
                    .build()
            );
        }
        return list;
    }

    @PutMapping(value = "/gateways/{phone}/change-status")
    public ResponseEntity<Void> ChangeGatewayStatus(@PathVariable("phone") String phone, @RequestBody String str) {
        // Get a client http header
        logger.info("/gateways/{}/change-status, PUT", phone);

        GatewayStatusEntity findGateway = gatewayStatusRepository.findById(phone).orElse(null);
        JSONObject param = new JSONObject(str);

        int status = param.getInt("status");

        if (findGateway == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        } else {
            findGateway.setGatewayStatus(status == 1 ? GatewayStatus.AS : GatewayStatus.Normal);
            gatewayStatusRepository.save(findGateway);
        }
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/gateways/{phone}")
    public ResponseEntity<Void> deleteGateway(@PathVariable("phone") String phone) {
        logger.info("/gateways/{}, DELETE", phone);

        GatewayStatusEntity gatewayStatusEntity = gatewayStatusRepository.findById(phone).orElse(null);

        if (gatewayStatusEntity != null) {
            gatewayStatusRepository.delete(gatewayStatusEntity);
        }

        recipientService.delete(phone);
        sensorStatusRepository.deleteById(phone);

        return ResponseEntity.ok().build();
    }

    public RemoteRequest saveRemote(String str, String phone, RemoteType remoteType, String content) {
        JSONObject json = new JSONObject(str);
        String userId = json.getString("userId");
        User user = userRepository.findByUserId(userId).orElse(null);
        return RemoteRequest.builder()
                .content(content)
                .isReceived(false)
                .phone(phone)
                .userName(user.getUserName())
                .remoteType(remoteType)
                .build();
    }

    public RemoteRequest saveRemote(String str, String phone, RemoteType remoteType) {
        return saveRemote(str, phone, remoteType, "");
    }
}
