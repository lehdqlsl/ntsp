package com.telefield.ntsp.controller;

import com.querydsl.core.Tuple;
import com.telefield.ntsp.domain.admin.gateway_failure_action.GatewayFailureActionRepository;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureRepository;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusQueryRepository;
import com.telefield.ntsp.domain.device.sensor_status.SensorStatusEntity;
import com.telefield.ntsp.domain.device.sensor_status.SensorStatusQueryRepository;
import com.telefield.ntsp.domain.recipient.info.QRecipientInfo;
import com.telefield.ntsp.domain.recipient.info.RecipientInfoQueryRepository;
import com.telefield.ntsp.domain.recipient.status.QRecipientStatus;
import com.telefield.ntsp.domain.search.HistorySearchDto;
import com.telefield.ntsp.domain.search.RecipientSearchDto;
import com.telefield.ntsp.service.EmergencyService;
import com.telefield.ntsp.service.FailureService;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class SearchController {
    final FailureService failureService;
    final EmergencyService emergencyService;

    final GatewayFailureRepository gatewayFailureRepository;
    final GatewayFailureActionRepository gatewayFailureActionRepository;

    final GatewayStatusQueryRepository gatewayStatusQueryRepository;
    final SensorStatusQueryRepository sensorStatusQueryRepository;

    final RecipientInfoQueryRepository recipientInfoQueryRepository;

    Logger logger = LoggerFactory.getLogger(SearchController.class);

    @GetMapping("/open/v1/history/search")
    public ResponseEntity<?> getHistory(HistorySearchDto historySearchDto) {
        String type = historySearchDto.getSearchType();

        if (type.equals("0")) {
            //장애이력
            return ResponseEntity.ok().body(failureService.searchFailure(historySearchDto));
        } else if (type.equals("1")) {
            //응급이력
            return ResponseEntity.ok().body(emergencyService.searchEmergency(historySearchDto));
        } else {
            JSONObject object = new JSONObject();
            object.put("error", "Not Supported Type. Type Value Must 0 or 1");
            return ResponseEntity.badRequest().body(object.toString());
        }
    }

    @GetMapping("/history/search")
    public ResponseEntity<?> getHistory2(HistorySearchDto historySearchDto) {
        String type = historySearchDto.getSearchType();

        if (type.equals("0")) {
            //장애이력
            return ResponseEntity.ok().body(failureService.searchFailure(historySearchDto));
        } else if (type.equals("1")) {
            //응급이력
            return ResponseEntity.ok().body(emergencyService.searchEmergency(historySearchDto));
        } else {
            JSONObject object = new JSONObject();
            object.put("error", "Not Supported Type. Type Value Must 0 or 1");
            return ResponseEntity.badRequest().body(object.toString());
        }

    }

    @GetMapping("/gateways/search")
    public List<GatewayStatusEntity> findGateway(@RequestParam String gatewayStatus, @RequestParam String phone) {
        return gatewayStatusQueryRepository.findByStatus(gatewayStatus, phone);
    }

    @GetMapping("/sensors/search")
    public List<SensorStatusEntity> SearchSensor(@RequestParam byte devType, @RequestParam String phone, @RequestParam byte network, @RequestParam byte battery) {
        return sensorStatusQueryRepository.getSearchSensor(devType, phone, network, battery);
    }


    @GetMapping("/recipients/search")
    public List<RecipientSearchDto> SearchRecipient(@RequestParam(defaultValue = "") String name, @RequestParam(defaultValue = "") String phone, @RequestParam(defaultValue = "-1") byte status, @RequestParam(defaultValue = "") String address) {
        List<Tuple> result = recipientInfoQueryRepository.getSearchRecipient(phone, name, status, address);
        List<RecipientSearchDto> ret = new ArrayList<>();
        for (Tuple row : result) {
            ret.add(RecipientSearchDto.builder()
                    .recipientInfo(row.get(QRecipientInfo.recipientInfo))
                    .recipientStatus(row.get(QRecipientStatus.recipientStatus)).build());
        }
        return ret;
    }
}
