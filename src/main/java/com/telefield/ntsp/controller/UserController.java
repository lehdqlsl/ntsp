package com.telefield.ntsp.controller;

import com.telefield.ntsp.config.CommonResponse;
import com.telefield.ntsp.config.ErrorCode;
import com.telefield.ntsp.config.InvalidTokenException;
import com.telefield.ntsp.domain.admin.account.User;
import com.telefield.ntsp.domain.admin.account.UserDto;
import com.telefield.ntsp.domain.admin.fcm.FcmKey;
import com.telefield.ntsp.domain.admin.fcm.FcmKeyRepository;
import com.telefield.ntsp.security.JwtAccessToken;
import com.telefield.ntsp.security.JwtAuthTokenProvider;
import com.telefield.ntsp.security.JwtRefreshToken;
import com.telefield.ntsp.security.TokenBundle;
import com.telefield.ntsp.service.LoginService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
@Slf4j
public class UserController {

    private final LoginService loginService;
    private final JwtAuthTokenProvider jwtAuthTokenProvider;
    private final FcmKeyRepository fcmKeyRepository;

    @PostMapping("/auth")
    public ResponseEntity<?> auth(final @Valid @RequestBody UserDto user) throws Exception {
        //AccessToken 재발급 AP
        log.info("/auth, id:{}, refreshToken:{} ", user.getUserId(), user.getRefreshToken());

        User findUser = loginService.getUser(user.getUserId()).orElseThrow(() -> new NotFoundException("'"+user.getUserId()+"' is not exist user"));

        log.info(findUser.toString());
        log.info(findUser.getRefreshToken());

        // 1. 토큰 일치 확인
        // 토큰 불일치 에러 (AUTH_001)
        if(!findUser.getRefreshToken().equals(user.getRefreshToken())){
            throw new InvalidTokenException("Does not Match Refresh Token");
        }

        // 2. Refresh 토큰 유효성 확인
        // 토큰 검증 로직.
        JwtRefreshToken jwtRefreshToken = jwtAuthTokenProvider.convertRefreshToken(user.getRefreshToken());
        if (!jwtRefreshToken.validate()) {
            throw new InvalidTokenException("Request Invalid Token, Try Login and get new token");
        }

        // 3. AccessToken 재발급
        JwtAccessToken jwtAccessToken = jwtAuthTokenProvider.createAccessAuthToken(findUser.getUserId(), findUser.getRole().toString());

        // 4. Refresh 토큰이 만료 1달 전이면 재발급
        if(jwtRefreshToken.isExpireOneMonth()){
            log.info("Renew refreshToken:" + user.getUserId());
            jwtRefreshToken = jwtAuthTokenProvider.createRefreshAuthToken(findUser.getUserId(), findUser.getRole().toString());
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("accessToken", jwtAccessToken.getAccessToken());
        jsonObject.put("refreshToken", jwtRefreshToken.getRefreshToken());

        return ResponseEntity.ok(jsonObject.toString());
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(HttpSession httpSession, @RequestBody User user) {
        log.info("/login, User:" + user.getUserId());
        User login = loginService.login(user.getUserId(), user.getPassword());
        if (login != null) {
            httpSession.setAttribute("KEY_ROLE", login.getRole());
            httpSession.setMaxInactiveInterval(3600);
        } else {
            CommonResponse response = CommonResponse.builder()
                    .code(ErrorCode.Login_FAILED.getCode())
                    .message(ErrorCode.Login_FAILED.getMessage())
                    .status(ErrorCode.Login_FAILED.getStatus())
                    .build();
            return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
        }

        JSONObject json = new JSONObject();
        if(user != null){
            TokenBundle tokenBundle = jwtAuthTokenProvider.createAuthToken(user.getUserId(),"ADMIN");

            login.setRefreshToken(tokenBundle.getJwtRefreshToken().getRefreshToken());
            loginService.save(login);

            log.info("AccessToken:"+tokenBundle.getJwtAccessToken().getAccessToken());
            log.info("RefreshToken:"+tokenBundle.getJwtRefreshToken().getRefreshToken());
            log.info("로그인 유저:"+login.toUserDto());
            json.put("user", login.toUserDto().toJSON());
            json.put("accessToken", tokenBundle.getJwtAccessToken().getAccessToken());
            json.put("refreshToken", tokenBundle.getJwtRefreshToken().getRefreshToken());
            json.put("status", 200);
            json.put("code", "AUTH_04");
        }

        return ResponseEntity.ok().body(json.toString());
    }

    @PostMapping("/users/{mobile}/fcm-key")
    public ResponseEntity<?> PostFCM(@PathVariable String mobile, @RequestBody FcmKey fcmKey) {
        log.info("POST, /users/{}/fcm-key", mobile);

        fcmKey.setMobile(mobile);
        fcmKeyRepository.save(fcmKey);

        return ResponseEntity.created(URI.create("/users/"+mobile+"/fcm-key")).build();
    }

    @DeleteMapping("/users/{mobile}/fcm-key")
    public ResponseEntity<?> DeleteFCM(@PathVariable String mobile) {
        log.info("POST, /users/{}/fcm-key", mobile);

        FcmKey fcmKey = fcmKeyRepository.findById(mobile).orElse(null);
        if(fcmKey!=null){
            fcmKeyRepository.delete(fcmKey);
        }else{
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }
}
