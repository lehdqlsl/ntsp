package com.telefield.ntsp.controller;

import com.telefield.ntsp.domain.admin.emergency_action.EmergencyAction;
import com.telefield.ntsp.domain.admin.emergency_action.EmergencyActionDto;
import com.telefield.ntsp.domain.admin.emergency_action.EmergencyActionRepository;
import com.telefield.ntsp.domain.event.emergency.Emergency;
import com.telefield.ntsp.domain.event.emergency.EmergencyQueryRepository;
import com.telefield.ntsp.domain.event.emergency.EmergencyRepository;
import com.telefield.ntsp.domain.event.entity.EventQueryRepository;
import com.telefield.ntsp.domain.statistics.StatisticsEmergency;
import com.telefield.ntsp.domain.statistics.StatisticsEmergencyRepository;
import com.telefield.ntsp.service.EmergencyService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class emergencyController {
    final EmergencyService emergencyService;
    final EmergencyRepository emergencyRepository;
    final EmergencyActionRepository emergencyActionRepository;
    final EmergencyQueryRepository emergencyQueryRepository;
    final EventQueryRepository eventQueryRepository;
    final StatisticsEmergencyRepository statisticsEmergencyRepository;

    Logger logger = LoggerFactory.getLogger(emergencyController.class);

    @GetMapping("/events/emergency/{emergencyId}")
    public Emergency GatewayEnvironment(@PathVariable long emergencyId) {
        return emergencyRepository.findById(emergencyId).orElse(null);
    }

    //특정응급 처리내역목록
    @GetMapping("/events/emergency/actions/{emergencyId}")
    public List<EmergencyAction> getGatewayFailureActionList(@PathVariable long emergencyId) {
        logger.info("GET, /events/emergency/actions/{}", emergencyId);
        Emergency emergency = emergencyRepository.findById(emergencyId).orElse(null);
        if (emergency != null) {
            return emergencyActionRepository.findByEmergencyIdOrderBySvRegDateDesc(emergency);
        } else {
            return null;
        }
    }

    // 응급내역 처리
    @PostMapping("/events/emergency/action")
    public ResponseEntity<Void> EmergencyAction(@RequestBody EmergencyActionDto emergencyActionDto) {
        Emergency emergency = emergencyRepository.findById(emergencyActionDto.getEmergencyId()).orElse(null);

        // 응급내역 닫기
        if (emergencyActionDto.isClosed()) {
            emergency.setClosed(true);
            emergency.setClosedDate(LocalDateTime.now());
            emergencyRepository.save(emergency);
        }

        // 응급처리 이력 저장
        if (emergency != null) {
            emergencyActionRepository.save(emergencyActionDto.toEntity(emergency));
        }

        return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.CREATED);
    }

    @GetMapping("/events/emergencies/{emergencyId}")
    public List<Emergency> getGatewayFailureList2(@PathVariable long emergencyId) {
        Emergency emergency = emergencyRepository.findById(emergencyId).orElse(null);
        return emergencyQueryRepository.getEmergencyListByPhone(emergency.getEventId().getPhone());
    }

    // 특정 전화번호 응급목록
    @GetMapping("/emergencies/{phone}")
    public List<Emergency> getGatewayFailureList(@PathVariable String phone) {
        logger.info("GET, /emergencies/{}", phone);
        return emergencyQueryRepository.getEmergencyListByPhone(phone);
    }

    @GetMapping("/statistics/emergencies")
    public List<StatisticsEmergency> getStatisticsEmergency() {
        LocalDate start = LocalDate.now().minusDays(1).minusMonths(1);
        LocalDate end = LocalDate.now().minusDays(1);
        return statisticsEmergencyRepository.findByRegDateBetweenOrderByRegDateDesc(start,end);
    }

}
