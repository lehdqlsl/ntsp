package com.telefield.ntsp.controller;

import com.telefield.ntsp.domain.device.GatewayLog;
import com.telefield.ntsp.domain.event.entity.Event;
import com.telefield.ntsp.domain.event.entity.EventQueryRepository;
import com.telefield.ntsp.domain.recipient.info.RecipientInfo;
import com.telefield.ntsp.domain.recipient.info.RecipientInfoRepository;
import com.telefield.ntsp.domain.recipient.notice.Notice;
import com.telefield.ntsp.domain.recipient.notice.NoticeRepository;
import com.telefield.ntsp.service.FileService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/open/v1/*")
@CrossOrigin(origins = "*", maxAge = 3600)
public class FileController {
    private static final String FIRE_DIR = "/fires/";
    private static final String LOG_DIR = "/gwlogs/";
    private static final String IMAGE_DIR = "/images/";

    final EventQueryRepository eventQueryRepository;
    final RecipientInfoRepository recipientInfoRepository;
    final NoticeRepository noticeRepository;
    final FileService fileService;

    Logger logger = LoggerFactory.getLogger(FileController.class);

    // 화재 이미지 가져오기
    @GetMapping(value = "/file/fire/{eventId}")
    public ResponseEntity<Resource> app_download(@PathVariable int eventId) {
        // Get a client http header
        HttpHeaders headers = new HttpHeaders();

        // Get Default Path
        Path currentWorkingDir = Paths.get("").toAbsolutePath();
        String path = currentWorkingDir.normalize().toString() + File.separator + FIRE_DIR + File.separator;

        File file = new File(path + eventId);

        if (!file.exists()) {
            return new ResponseEntity<Resource>(headers, HttpStatus.NOT_FOUND);
        }

        InputStreamResource resource;
        try {
            resource = new InputStreamResource(new FileInputStream(file));
            return ResponseEntity.ok().headers(headers).contentLength(file.length())
                    .contentType(MediaType.IMAGE_PNG).body(resource);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ResponseEntity<Resource>(headers, HttpStatus.NOT_FOUND);
        }
    }


    // 게이트웨이 로그 다운로드
    @GetMapping(value = "/gateway/log/{file_name}")
    public ResponseEntity<Resource> app_download(@PathVariable String file_name) {
        // Get a client http header
        HttpHeaders headers = new HttpHeaders();

        // Get Default Path
        Path currentWorkingDir = Paths.get("").toAbsolutePath();
        String path = currentWorkingDir.normalize().toString() + File.separator + LOG_DIR + File.separator;

        File file = new File(path + file_name);

        if (!file.exists()) {
            return new ResponseEntity<Resource>(headers, HttpStatus.NOT_FOUND);
        }

        InputStreamResource resource;
        try {
            resource = new InputStreamResource(new FileInputStream(file));
            return ResponseEntity.ok().headers(headers).contentLength(file.length())
                    .contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ResponseEntity<Resource>(headers, HttpStatus.NOT_FOUND);
        }
    }

    //게이트웨이 로그 목록
    @GetMapping(value = "/gateways/{phone}/logs")
    public ResponseEntity<List<GatewayLog>> GetAllGatewayLogs(@PathVariable String phone) {
        logger.info("GET, /gateways/{}/log", phone);

        Path currentWorkingDir = Paths.get("").toAbsolutePath();
        String path = currentWorkingDir.normalize().toString() + LOG_DIR;

        File[] files = new File(path).listFiles();

        List<GatewayLog> findAllGatewayLogs = new ArrayList<GatewayLog>();

        try {
            if (files.length > 0) {
                for (File file : files) {
                    String sFileName = file.getName();
                    if (phone.equals(sFileName.split("\\.")[0])) {
                        GatewayLog gatewayLog = new GatewayLog();
                        gatewayLog.setPhone(phone);
                        gatewayLog.setFile_name(sFileName);
                        gatewayLog.setSize(file.length() / 1024 + " KB");
                        gatewayLog.setRegDate(sFileName.split("\\.")[1]);
                        findAllGatewayLogs.add(gatewayLog);
                    }
                }
            } else {
                return new ResponseEntity<List<GatewayLog>>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<List<GatewayLog>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<List<GatewayLog>>(findAllGatewayLogs, HttpStatus.OK);
    }

    // 프로필 사진 가져오기
    @GetMapping(value = "/recipients/{phone}/profile")
    public ResponseEntity<Resource> getProfile(@PathVariable String phone) {
        logger.info("GET, /recipients/{}/profile", phone);

        HttpHeaders headers = new HttpHeaders();

        List<RecipientInfo> recipientInfo = recipientInfoRepository.findByPhone(phone);

        if (recipientInfo.isEmpty()) {
            return new ResponseEntity<Resource>(headers, HttpStatus.NOT_FOUND);
        }

        // Get Default Path
        Path currentWorkingDir = Paths.get("").toAbsolutePath();
        String path = currentWorkingDir.normalize().toString() + IMAGE_DIR;

        File file = new File(path + recipientInfo.get(0).getImageFile());

        if (!file.exists()) {
            return new ResponseEntity<Resource>(headers, HttpStatus.NOT_FOUND);
        }

        InputStreamResource resource;
        try {
            resource = new InputStreamResource(new FileInputStream(file));

            return ResponseEntity.ok().headers(headers).contentType(MediaType.IMAGE_JPEG).contentType(MediaType.IMAGE_PNG).contentLength(file.length()).body(resource);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ResponseEntity<Resource>(headers, HttpStatus.NOT_FOUND);
        }
    }

    // 오디오 파일 가져오기
    @GetMapping(value = "/recipient/notices/audio/{seq}")
    public ResponseEntity<?> getProfile(@PathVariable int seq) {
        logger.info("GET, /recipient/notices/audio/{}", seq);

        HttpHeaders headers = new HttpHeaders();
        Notice notice = noticeRepository.findById(seq).orElseGet(null);

        if (notice == null) {
            return new ResponseEntity<Resource>(headers, HttpStatus.NOT_FOUND);
        }

        String path = fileService.findAudioPath(seq);

        File file = new File(path + seq);

        if (!file.exists()) {
            return new ResponseEntity<Resource>(headers, HttpStatus.NOT_FOUND);
        }

        InputStreamResource resource;
        try {
            resource = new InputStreamResource(new FileInputStream(file));
            return ResponseEntity.ok().contentType(MediaType.valueOf("audio/mp4")).contentLength(file.length()).body(resource);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ResponseEntity<Resource>(headers, HttpStatus.NOT_FOUND);
        }
    }
}
