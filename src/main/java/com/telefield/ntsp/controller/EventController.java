package com.telefield.ntsp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefield.ntsp.define.EventType;
import com.telefield.ntsp.domain.event.emergency.Emergency;
import com.telefield.ntsp.domain.event.entity.*;
import com.telefield.ntsp.service.EventService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class EventController {
    final EventService eventService;
    final EventRepository eventRepository;
    final EventQueryRepository eventQueryRepository;
    final EventTestQueryRepository eventTestQueryRepository;

    Logger logger = LoggerFactory.getLogger(EventController.class);


    @GetMapping("/gateways/event/open")
    public List<Event> GatewayOpenEvent() {
        logger.info("/gateways/event/open");
        return eventQueryRepository.GetOpenEvent();
    }

    @GetMapping("/gateways/{phone}/events")
    public List<EventDto> GatewayCycle(@PathVariable String phone) {
        List<Event> list = eventRepository.findByPhoneOrderByGwRegDateDesc(phone);
        ObjectMapper mapper = new ObjectMapper();
        List<EventDto> ret = new ArrayList<>();

        for(Event event : list){
            try {
                HashMap<String, Object> map = mapper.readValue(event.getData(), HashMap.class);
                ret.add(event.toEventDto(map));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        }
        return ret;
    }

    @GetMapping("/gateways/{phone}/events/{offset}")
    public List<EventDto> GatewayEvents(@PathVariable String phone, @PathVariable int offset) {
        List<Event> list = eventQueryRepository.findByPhoneLimit(phone, offset*20);
        ObjectMapper mapper = new ObjectMapper();
        List<EventDto> ret = new ArrayList<>();

        for(Event event : list){
            try {
                HashMap<String, Object> map = mapper.readValue(event.getData(), HashMap.class);
                ret.add(event.toEventDto(map));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        }
        return ret;
    }

    @GetMapping("/gateways/{phone}/events/{offset}/testmode")
    public List<EventDto> GatewayTestModeEvents(@PathVariable String phone, @PathVariable int offset) {
        List<EventTest> list = eventTestQueryRepository.findByPhoneLimit(phone, offset*20);
        ObjectMapper mapper = new ObjectMapper();
        List<EventDto> ret = new ArrayList<>();

        for(EventTest event : list){
            try {
                HashMap<String, Object> map = mapper.readValue(event.getData(), HashMap.class);
                ret.add(event.toEventDto(map));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        }
        return ret;
    }

    @GetMapping("/emergency/count")
    public Map<String,Long> emergencyCountThisMonth(){
        return eventService.getEmergencyCountThisMonth();
    }

    @GetMapping("/emergency/count/last")
    public Map<String,Long> emergencyCountLastMonth(){
        return eventService.getEmergencyCountLastMonth();
    }

    @GetMapping("/emergencies")
    public HashMap<String, List<Emergency>> getEmergencyList() {
        List<Emergency> list = eventService.getEmergencyList();

        HashMap<String,List<Emergency>> ret = new HashMap<String, List<Emergency>>();
        List<Emergency> emergencyList = new ArrayList<>();
        List<Emergency> noactList = new ArrayList<>();

        for(Emergency entity : list){
            if(EventType.isEmergencyEvent(entity.getEventId().getEventType())){
                emergencyList.add(entity);
            }else{
                noactList.add(entity);
            }
        }

        ret.put("emergency", emergencyList);
        ret.put("no_activity", noactList);
        return ret;
    }

    @GetMapping("/emergencies/mobile")
    public List<Emergency> getEmergencyListMobile() {
        return eventService.getEmergencyList();
    }
}

