package com.telefield.ntsp.controller;

import com.telefield.ntsp.define.EventType;
import com.telefield.ntsp.define.FcmType;
import com.telefield.ntsp.domain.admin.remote.RemoteRequest;
import com.telefield.ntsp.domain.admin.remote.RemoteRequestRepository;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusQueryRepository;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusRepository;
import com.telefield.ntsp.domain.device.gw_cycle.Cycle;
import com.telefield.ntsp.domain.device.gw_cycle.CycleDto;
import com.telefield.ntsp.domain.device.gw_cycle.CycleQueryRepository;
import com.telefield.ntsp.domain.device.gw_cycle.CycleRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.SensorCycleRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.SensorDto;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentRepository;
import com.telefield.ntsp.domain.device.sensor_status.SensorStatusEntity;
import com.telefield.ntsp.domain.device.sensor_status.SensorStatusRepository;
import com.telefield.ntsp.domain.event.entity.ActivityCycleDto;
import com.telefield.ntsp.domain.event.entity.Event;
import com.telefield.ntsp.domain.event.entity.EventDto;
import com.telefield.ntsp.domain.event.entity.EventQueryRepository;
import com.telefield.ntsp.domain.file.contents.ContentInfo;
import com.telefield.ntsp.domain.file.contents.ContentInfoRepository;
import com.telefield.ntsp.service.DeviceService;
import com.telefield.ntsp.service.EventService;
import com.telefield.ntsp.service.FCMService;
import com.telefield.ntsp.service.RecipientService;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Response;
import java.io.*;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/open/v1/*")
@CrossOrigin(origins = "*", maxAge = 3600)
public class GatewayController {
    private static final String FIRE_DIR = "/fires/";
    private static final String LOG_DIR = "/gwlogs/";
    private static final String CONTENTS_DIR = "/contents/";

    final DeviceService deviceService;
    final EventService eventService;
    final RecipientService recipientService;
    final CycleRepository cycleRepository;
    final EnvironmentRepository environmentRepository;
    final GatewayStatusQueryRepository gatewayStatusQueryRepository;
    final CycleQueryRepository cycleQueryRepository;
    final GatewayStatusRepository gatewayStatusRepository;
    final SensorStatusRepository sensorStatusRepository;
    final FCMService fcmService;
    final SensorCycleRepository sensorCycleRepository;
    final EventQueryRepository eventQueryRepository;
    final ContentInfoRepository contentInfoRepository;
    final RemoteRequestRepository remoteRequestRepository;

    Logger logger = LoggerFactory.getLogger(GatewayController.class);

    // 게이트웨이 이미지 업로드
    @PostMapping(value = "/file/fire")
    public ResponseEntity<Void> uploadFires(HttpServletRequest request,
                                            @PathVariable("upload_file") MultipartFile upload_file, @RequestParam String phone) {
        // Get a client http header
        Event fire = eventQueryRepository.getLastEmergencyEvent(phone);
        logger.info("POST, /file/fire, " + phone);
        long seq = fire.getEventId();

        if (upload_file.isEmpty()) {
            return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }

        // Make the directory "contents"
        Path currentWorkingDir = Paths.get("").toAbsolutePath();
        String path = currentWorkingDir.normalize().toString() + FIRE_DIR;

        File dir = new File(path);

        try {
            if (!dir.exists()) {
                dir.mkdir();
            }
        } catch (SecurityException se) {
            return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // Upload the file
        BufferedOutputStream stream = null;
        try {
            byte[] bytes = upload_file.getBytes();

            File uploadFile = new File(dir.getAbsolutePath() + File.separator + seq);
            stream = new BufferedOutputStream(new FileOutputStream(uploadFile));
            stream.write(bytes);
            stream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.OK);
    }

    // 게이트웨이 로그 업로드
    @PostMapping(value = "/gateways/{phone}/log")
    public ResponseEntity<Void> uploadLogFile(HttpServletRequest request, MultipartFile upload_file, @PathVariable String phone, String logDate) {
        // Get a client http header
        logger.info("POST, /gateways/{}/log", phone);

        if (upload_file.isEmpty()) {
            return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }

        // Make the directory "contents"
        Path currentWorkingDir = Paths.get("").toAbsolutePath();
        String path = currentWorkingDir.normalize().toString() + LOG_DIR;

        File dir = new File(path);

        try {
            if (!dir.exists()) {
                dir.mkdir();
            }
        } catch (SecurityException se) {
            return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // Upload the file
        BufferedOutputStream stream = null;
        try {
            byte[] bytes = upload_file.getBytes();

            File uploadFile = new File(dir.getAbsolutePath() + File.separator + phone + "." + logDate + ".txt");

            stream = new BufferedOutputStream(new FileOutputStream(uploadFile));
            stream.write(bytes);
            stream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/gateways/event/testmode")
    public ResponseEntity<Void> GatewayEventTest(@RequestBody EventDto eventDto) {
        logger.info("POST, /gateways/event/testmode" + " , " + eventDto);
        //게이트웨이 테스트 모드
        eventService.saveEvent(eventDto.toTestEntity());
        return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.CREATED);
    }

    @PutMapping("/gateways/{phone}/fcm")
    public ResponseEntity<Void> GatewayFcmUpdate(@PathVariable String phone, @RequestBody String str) {
        logger.info("PUT, /gateways/{}/fcm", phone);

        GatewayStatusEntity gatewayStatusEntity = gatewayStatusRepository.findById(phone).orElse(null);

        if (gatewayStatusEntity != null) {
            JSONObject json = new JSONObject(str);
            String fcmId = json.getString("fcmId");
            logger.info("FCM:" + fcmId);
            gatewayStatusEntity.setFcmId(fcmId);
            gatewayStatusRepository.save(gatewayStatusEntity);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/gateways/{phone}/number")
    public ResponseEntity<Void> modifyNumber(@PathVariable String phone, @RequestBody String str) {
        logger.info("POST, /gateways/{}/number", phone);
        JSONObject json = new JSONObject(str);
        logger.info("Param, {}", json);
        String type = json.getString("type");
        String number = json.getString("number");
        recipientService.modifyNumber(phone,type,number);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/gateways/{phone}/number")
    public ResponseEntity<Void> modifyNumberAndPush(@PathVariable String phone, @RequestBody String str) {
        logger.info("PUT, /gateways/{}/number", phone);
        JSONObject json = new JSONObject(str);
        logger.info("Param, {}", json);
        String type = json.getString("type");
        String number = json.getString("number");
        recipientService.modifyNumber(phone,type,number);

        fcmService.fcmRequest("","",type+";"+"이거는 생략합시다"+";"+number);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/gateways/open")
    public ResponseEntity<Void> GatewayOpening(@RequestBody CycleDto cycle) {
        logger.info("POST, /gateways/open" + " , " + cycle.getPhone());
        logger.info("parameter:" + cycle);
        Cycle cycleEntity = cycle.toCycleEntity();
        if (cycle.getSensorCycle() == null) {
            SensorDto[] sensor = new SensorDto[0];
            cycle.setSensorCycle(sensor);
        }

        // 1. 대상자 정보 조회
        // 2. 없으면 새로운 대상자 생성
        recipientService.recipientSave(cycle);

        // 2-1 전화번호 목록 저장
        recipientService.saveNumbers(cycle);

        // 3. 센서 데이터 삭제
        deviceService.deleteSensor(cycle.getPhone());

        // 4. 개통이벤트 삽입
        Event event = Event.builder()
                .eventType(EventType.OPEN)
                .devType((byte) 0)
                .mac_addr(cycleEntity.getMacAddr())
                .phone(cycleEntity.getPhone())
                .send_reg_date(cycleEntity.getSend_reg_date())
                .build();
        eventService.saveEvent(event);

        // 5. 주기보고 저장
        deviceService.saveCycle(cycle);

        return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.CREATED);
    }

    @PostMapping("/gateways/cycle")
    public ResponseEntity<Void> GatewayCycle(@RequestBody CycleDto cycle) {
        logger.info("POST, /gateways/cycle" + " , " + cycle.getPhone());
        deviceService.saveCycle(cycle);
        return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.CREATED);
    }

    @PostMapping("/gateways/cycle/testmode")
    public ResponseEntity<Void> GatewayCycle2(@RequestBody CycleDto cycle) {
        logger.info("POST, /gateways/cycle" + " , " + cycle.getPhone());
        SensorDto[] sensorDtos = cycle.getSensorCycle();
        return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.CREATED);
    }

    @PostMapping("/gateways/event")
    public ResponseEntity<?> GatewayEvent(@RequestBody EventDto eventDto) {
        logger.info("POST, /gateways/event" + " , " + eventDto);
        Event event = eventService.saveEvent(eventDto.toEntity());

        byte eventType = event.getEventType();

        if(EventType.isStatusEvent(eventType)){
            //1. 상태변경
            eventService.changeUserStatus(event);
        }

        //2. 응급이벤트 처리
        if(EventType.isEmergencyEvent(eventType) || EventType.NO_ACTIVITY == eventType){
            eventService.processEmergencyEvent(event);
        }else if(eventType == EventType.GATEWAY_PLUG){
            eventService.processPowerPlug(event);
        }else if(eventType == EventType.GATEWAY_UNPLUG){
            eventService.processPowerUnplug(event);
        }else if(EventType.isEmergencyClear(eventType)){
            eventService.processEmergencyClear(event);
        }

        return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.CREATED);
    }

    @PostMapping("/gateways/{phone}/cycle/activity")
    public ResponseEntity<JSONObject> GatewayCycle(@RequestBody ActivityCycleDto activityCycleDto, @PathVariable String phone) {
        logger.info("POST, /gateways/{}/cycle/activity", phone);
        logger.info("radar:"+activityCycleDto.getRadar());

        if(activityCycleDto.getRadar() != null){
            deviceService.saveAct(activityCycleDto.getRadar(), phone, activityCycleDto.getRegDate());
        }

        if(activityCycleDto.getActivity()[0] != null){
            deviceService.saveAct(activityCycleDto.getActivity(), phone, activityCycleDto.getRegDate());
        }

        return new ResponseEntity<JSONObject>(new HttpHeaders(), HttpStatus.CREATED);
    }

    @GetMapping("/contents")
    public ResponseEntity<?> getContents() {
        logger.info("GET, /contents");
        return ResponseEntity.ok(contentInfoRepository.findAll());
    }

    @GetMapping(value = "/contents/{seq}")
    public ResponseEntity<?> app_download(@PathVariable long seq) throws UnsupportedEncodingException {
        logger.info("GET, /contents/{}", seq);
        ContentInfo contentInfo = contentInfoRepository.findById(seq).orElse(null);

        if(contentInfo == null)
            return ResponseEntity.ok("Invalid Seq");

        String fileNameByUTF8 = URLEncoder.encode(contentInfo.getFileName(), "UTF-8").replaceAll("\\+", "%20");
        // header
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileNameByUTF8);
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        // Get Default Path
        Path currentWorkingDir = Paths.get("").toAbsolutePath();
        String path = currentWorkingDir.normalize().toString() + File.separator + CONTENTS_DIR + File.separator;
        File file = new File(path + contentInfo.getFileName());

        if (!file.exists()) {
            return ResponseEntity.ok("File NotFound");
        }

        InputStreamResource resource;
        try {
            resource = new InputStreamResource(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            return ResponseEntity.ok("File NotFound");
        }
        return ResponseEntity.ok()
                .headers(header)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream;charset=UTF-8")).body(resource);
    }

    @PutMapping(value = "/remote/{seq}")
    public ResponseEntity<?> updateRemoteRequest(@PathVariable long seq){
        logger.info("PUT, /remote/{}", seq);
        RemoteRequest remoteRequest = remoteRequestRepository.findById(seq).orElse(null);
        if(remoteRequest == null){
            return ResponseEntity.ok("not found seq");
        }
        remoteRequest.setReceived(true);
        remoteRequestRepository.save(remoteRequest);
        return ResponseEntity.ok("");
    }

    @GetMapping(value = "/gateways/{phone}/remotes")
    public ResponseEntity<?> updateRemoteRequest(@PathVariable String phone){
        logger.info("GET, /gateways/{}/remotes", phone);
        List<RemoteRequest> remoteRequest = remoteRequestRepository.findByPhone(phone);
        return ResponseEntity.ok(remoteRequest);
    }

    @GetMapping(value = "/gateways/version/latest")
    public ResponseEntity<?> getLastGatewayVersion(){
        logger.info("GET, /gateways/version/latest");
        JSONObject ret = new JSONObject(new RestTemplate().getForEntity("http://14.47.229.119:8888/version/latest", String.class).getBody());
        return ResponseEntity.ok(ret.getString("version"));
    }
}

