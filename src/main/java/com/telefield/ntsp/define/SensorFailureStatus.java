package com.telefield.ntsp.define;

public enum SensorFailureStatus {
    NETWORK_BAD, BATTERY_LOWEST
}
