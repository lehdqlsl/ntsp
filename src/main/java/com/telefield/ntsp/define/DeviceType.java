package com.telefield.ntsp.define;

public class DeviceType {
    public static final byte GATEWAY = 0;
    public static final byte ACTIVITY_SENSOR = 2;
    public static final byte DOOR_SENSOR = 18;
    public static final byte FIRE_SENSOR = 11;
    public static final byte EMERGENCY_CALLER = 10;
    public static final byte RADAR_SENSOR = 3;
    public static final byte ENVIRONMENT_SENSOR = 4;
}
