package com.telefield.ntsp.define;

public class PowerState {
    public static final byte UNPLUG = 0;
    public static final byte PLUG = 1;
}
