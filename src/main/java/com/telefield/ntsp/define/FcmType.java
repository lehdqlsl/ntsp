package com.telefield.ntsp.define;

public class FcmType {
    //로그 요청
    public static final String PUSH_LOG = "log";

    //원격 개통
    public static final String PUSH_OPEN = "reggw";

    //원격 주기보고 요청
    public static final String PUSH_CYCLE = "reqcycle";

    //응급상황 해제
    public static final String PUSH_STOP = "emgstop";

    //태블릿 재부팅
    public static final String PUSH_REBOOT = "reboot";

    //원격 재실 보고
    public static final String PUSH_INDOOR = "chgin";

    //원격 활동 보고
    public static final String PUSH_ACT = "reqact";

    //App 업데이트
    public static final String PUSH_UPDATE_APP = "udap";

    //F/W 업데이트
    public static final String PUSH_UPDATE_FW = "udfw";

    // 활동미감지 시간 변경
    public static final String PUSH_CHANGE_INACTIVE_TIME = "inactivechange";

    // 주기보고 시간 변경
    public static final String PUSH_CHANGE_CYCLE_TIME = "cyclechange";

    // 역점검
    public static final String PUSH_REVERSE_CHECK = "reversecheck";

    // 원격 시스템 모드 설정
    public static final String PUSH_MODE_CHANGE = "modechange";

    // 원격 URL 설정
    public static final String PUSH_URL_CHANGE = "urlchange";

    // 원격 홈 실행
    public static final String PUSH_HOME_DISPLAY = "homedisplay";

    // 원격 센서 보고 설정
    public static final String PUSH_CHANGE_SENSOR_CYCLE = "sensorcyclechange";
}

