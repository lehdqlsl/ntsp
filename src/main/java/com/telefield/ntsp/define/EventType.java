package com.telefield.ntsp.define;

import java.lang.reflect.Field;

public class EventType {
    public static final byte _119_BUTTON = 0;
    public static final byte _119_CANCEL = 1;
    public static final byte _119_CALL = 2;
    public static final byte CENTER_CALL = 3;

    public static final byte GATEWAY_PLUG = 4;
    public static final byte GATEWAY_UNPLUG = 5;

    public static final byte NO_ACTIVITY = 7;
    public static final byte ACTIVITY = 8;

    public static final byte DOOR_OPEN = 9;
    public static final byte DOOR_CLOSE = 10;

    public static final byte FIRE_START= 11;
    public static final byte FIRE_END = 12;
    public static final byte FIRE_CANCEL = 24;

    public static final byte EMERGENCY_CALL = 15;
    public static final byte EMERGENCY_CALL_CANCEL = 16;

    public static final byte GO_OUT = 17;
    public static final byte GO_IN = 18;

    public static final byte CODI_DISABLED = 19;
    public static final byte CODI_ENABLE = 25;

    public static final byte LOW_BATTERY = 20;

    //음성인식 성공
    public static final byte VOICE = 21;

    public static final byte TEST_MODE_ON= 22;
    public static final byte TEST_MODE_OFF= 23;

    public static final byte RADAR_DISABLED = 26;
    public static final byte RADAR_ENABLED = 27;


    //개통
    public static final byte OPEN = 100;

    public static boolean isEmergencyEvent(byte eventType){
        return (eventType==_119_CALL||eventType==EMERGENCY_CALL||eventType==FIRE_START ? true : false);
    }

    public static boolean isStatusEvent(byte eventType){
        return (eventType==NO_ACTIVITY||eventType==ACTIVITY||eventType==GO_OUT||eventType==GO_IN ? true : false);
    }

    public static boolean isPowerEvent(byte eventType) {
        return (eventType==GATEWAY_PLUG||eventType==GATEWAY_UNPLUG ? true : false);
    }

    public static boolean isEmergencyClear(byte eventType) {
        return (eventType==FIRE_END||eventType==EMERGENCY_CALL_CANCEL || eventType==FIRE_CANCEL ? true : false);
    }
}

