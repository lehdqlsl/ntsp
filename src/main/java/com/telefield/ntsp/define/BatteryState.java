package com.telefield.ntsp.define;

public class BatteryState {
    public static final byte LOWEST = 0;
    public static final byte LOW = 1;
    public static final byte NORMAL = 2;
}
