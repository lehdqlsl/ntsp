package com.telefield.ntsp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@RequiredArgsConstructor
public class FileService {
    private static final String FIRE_DIR = "/fires/";
    private static final String LOG_DIR = "/gwlogs/";
    private static final String NOTICE_AUDIO_DIR = File.separator + "notice" + File.separator;

    public String SaveNoticeFile(MultipartFile uploadFile, int seq){
        if (uploadFile == null ||uploadFile.isEmpty()) {
            return "text";
        }

        Path currentWorkingDir = Paths.get("").toAbsolutePath();
        String path = currentWorkingDir.normalize().toString() + NOTICE_AUDIO_DIR;

        File dir = new File(path);

        if (!dir.exists()) {
            dir.mkdir();
        }

        BufferedOutputStream stream = null;
        try {
            byte[] bytes = uploadFile.getBytes();
            File file = new File(dir.getAbsolutePath() + File.separator + seq);
            stream = new BufferedOutputStream(new FileOutputStream(file));
            stream.write(bytes);
            stream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return "text";
        }
        return "audio";
    }

    public String findAudioPath(int seq) {
        Path currentWorkingDir = Paths.get("").toAbsolutePath();
        return currentWorkingDir.normalize().toString() + NOTICE_AUDIO_DIR;
    }
}
