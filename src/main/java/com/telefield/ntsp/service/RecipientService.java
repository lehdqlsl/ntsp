package com.telefield.ntsp.service;

import com.telefield.ntsp.domain.device.gw_cycle.CycleDto;
import com.telefield.ntsp.domain.recipient.info.RecipientInfo;
import com.telefield.ntsp.domain.recipient.info.RecipientInfoRepository;
import com.telefield.ntsp.domain.recipient.numbers.Numbers;
import com.telefield.ntsp.domain.recipient.numbers.NumbersRepository;
import com.telefield.ntsp.domain.recipient.status.RecipientStatus;
import com.telefield.ntsp.domain.recipient.status.RecipientStatusRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RecipientService {
    private final RecipientStatusRepository recipientStatusRepository;
    private final RecipientInfoRepository recipientInfoRepository;
    private final NumbersRepository numbersRepository;

    public void recipientSave(CycleDto cycleDto){
        RecipientStatus recipientStatus = recipientStatusRepository.findById(cycleDto.getPhone()).orElse(null);
        List<RecipientInfo> recipientInfo = recipientInfoRepository.findByPhone(cycleDto.getPhone());

        if(recipientStatus == null){
            recipientStatusRepository.save(createRecipient(cycleDto));
        }else{
            recipientStatus.setSend_reg_date(cycleDto.getRegDate());
            recipientStatusRepository.save(recipientStatus);
        }

        if(recipientInfo.isEmpty()){
            recipientInfoRepository.save(createRecipientInfo(cycleDto));
        }else{
            recipientInfo.get(0).setRegDate(cycleDto.getRegDate());
            recipientInfoRepository.save(recipientInfo.get(0));
        }
    }

    public RecipientStatus createRecipient(CycleDto cycleDto){
        return RecipientStatus.builder()
                .current_status((byte) 0)
                .mac_addr(cycleDto.getMacAddr())
                .phone(cycleDto.getPhone())
                .send_reg_date(cycleDto.getRegDate())
                .build();
    }

    public RecipientInfo createRecipientInfo(CycleDto cycleDto){
        return RecipientInfo.builder()
                .imageFile("default.jpg")
                .phone(cycleDto.getPhone())
                .build();
    }

    public List<RecipientInfo> findRecipientByType(String data, int type){
        if(type == 0)
            return recipientInfoRepository.findByPhoneEndingWith(data);
        if(type == 1)
            return recipientInfoRepository.findByPhone(data);
        if(type == 2)
            return recipientInfoRepository.findByName(data);
        return null;
    }

    public void delete(String phone) {
        RecipientInfo recipientInfo = recipientInfoRepository.findById(phone).orElse(null);
        if(recipientInfo != null){
            recipientInfoRepository.delete(recipientInfo);
        }

        RecipientStatus recipientStatus = recipientStatusRepository.findById(phone).orElse(null);
        if(recipientStatus != null){
            recipientStatusRepository.delete(recipientStatus);
        }
    }

    public void saveNumbers(CycleDto cycle) {
        if(cycle.getNumbers() != null){
            cycle.getNumbers().setPhone(cycle.getPhone());
            numbersRepository.save(cycle.getNumbers());
        }
    }

    public Numbers getNumbers(String phone){
        return numbersRepository.findById(phone).orElse(null);
    }
    public void modifyNumber(String phone, String type, String number) {
        Numbers numbers = numbersRepository.findById(phone).orElse(null);

        if(numbers != null){
            if(type.equals("_119")){
                numbers.set_119(number);
            }else if(type.equals("helper")){
                numbers.setHelper(number);
            }else if(type.equals("center")){
                numbers.setCenter(number);
            }else if(type.equals("care1")){
                numbers.setCare1(number);
            }else if(type.equals("care2")){
                numbers.setCare2(number);
            }else if(type.equals("etc1")){
                numbers.setEtc1(number);
            }else if(type.equals("etc2")){
                numbers.setEtc2(number);
            }else if(type.equals("etc3")){
                numbers.setEtc3(number);
            }else if(type.equals("etc4")){
                numbers.setEtc4(number);
            }else if(type.equals("etc5")){
                numbers.setEtc5(number);
            }else if(type.equals("etc6")){
                numbers.setEtc6(number);
            }else if(type.equals("etc7")){
                numbers.setEtc7(number);
            }else if(type.equals("etc8")){
                numbers.setEtc8(number);
            }else if(type.equals("etc9")){
                numbers.setEtc9(number);
            }else if(type.equals("etc10")){
                numbers.setEtc10(number);
            }
        }

        numbersRepository.save(numbers);
    }
}
