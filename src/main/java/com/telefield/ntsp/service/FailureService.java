package com.telefield.ntsp.service;

import com.telefield.ntsp.define.GatewayFailureStatus;
import com.telefield.ntsp.domain.admin.gateway_failure_action.GatewayFailureAction;
import com.telefield.ntsp.domain.admin.gateway_failure_action.GatewayFailureActionRepository;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureQueryRepository;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureRepository;
import com.telefield.ntsp.domain.device.sensor_failure.SensorFailure;
import com.telefield.ntsp.domain.device.sensor_failure.SensorFailureRepository;
import com.telefield.ntsp.domain.search.HistorySearchDto;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class                                                                                                                                                                                                                                                                                                                                    FailureService {
    private final GatewayFailureRepository gatewayFailureRepository;
    private final SensorFailureRepository sensorFailureRepository;
    private final GatewayFailureActionRepository gatewayFailureActionRepository;
    private final GatewayFailureQueryRepository gatewayFailureQueryRepository;

    Logger logger = LoggerFactory.getLogger(FailureService.class);

    @Transactional(readOnly = true)
    public GatewayFailure getGatewayFailure(long failure_id) {
        return gatewayFailureRepository.findById(failure_id).orElse(null);
    }

    @Transactional
    public void closedSensorFailure(SensorFailure sensorFailure) {
        sensorFailure.setClosed(true);
        sensorFailure.setClosedDate(LocalDateTime.now());
        sensorFailureRepository.save(sensorFailure);
    }

    @Transactional
    public void deviceFailureClose(GatewayFailure gatewayFailure, boolean isAuto, String user) {
        GatewayFailureAction close;
        // 처리이력 추가
        if(isAuto){
            close = GatewayFailureAction.builder()
                    .failureId(gatewayFailure)
                    .text("시스템에 의한 자동 클로즈")
                    .title("해결됨")
                    .user("System")
                    .build();
        }else{
            close = GatewayFailureAction.builder()
                    .failureId(gatewayFailure)
                    .user(user)
                    .text("관리자에 의한 수동 클로즈")
                    .title("해결됨")
                    .build();
        }
        gatewayFailureActionRepository.save(close);

        // 장애내역 닫기
        gatewayFailure.setClosed(true);
        gatewayFailure.setClosedDate(LocalDateTime.now());
        gatewayFailureRepository.save(gatewayFailure);
    }

    public List<GatewayFailure> searchFailure(HistorySearchDto historySearchDto) {

        if(historySearchDto.getFailureType().equals("1")){
            historySearchDto.setFailureValue(GatewayFailureStatus.Unplug);
        }else if(historySearchDto.getFailureType().equals("2")){
            historySearchDto.setFailureValue(GatewayFailureStatus.Non_Receive);
        }

        if(historySearchDto.getCloseType().equals("1")){
            historySearchDto.setClosed(true);
        }else if(historySearchDto.getCloseType().equals("2")){
            historySearchDto.setClosed(false);
        }

        return gatewayFailureQueryRepository.failureSearch(historySearchDto);
    }
}
