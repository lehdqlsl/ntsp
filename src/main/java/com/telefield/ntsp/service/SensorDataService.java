package com.telefield.ntsp.service;

import com.telefield.ntsp.domain.device.sensor_cycle.activity.ActivityEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.activity.ActivityQueryRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.activity.ActivityRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentQueryRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.radar.RadarEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.radar.RadarQueryRepository;
import com.telefield.ntsp.domain.event.entity.EventQueryRepository;
import com.telefield.ntsp.domain.event.entity.EventRepository;
import com.telefield.ntsp.domain.event.entity.EventStat;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SensorDataService {
    private final EnvironmentRepository environmentRepository;
    private final EnvironmentQueryRepository environmentQueryRepository;
    private final ActivityQueryRepository activityQueryRepository;
    private final ActivityRepository activityRepository;
    private final EventQueryRepository eventQueryRepository;
    private final EventRepository eventRepository;
    private final RadarQueryRepository radarQueryRepository;

    @Transactional(readOnly = true)
    public JSONObject getInActData(String phone, int eventType, int dateType) {
        LocalDateTime searchTime;

        switch (dateType) {
            case 1: // last 1 week
                searchTime = LocalDateTime.now().minusWeeks(1);
                break;
            case 2: // last 1 month
                searchTime = LocalDateTime.now().minusMonths(1);
                break;
            case 3: // last 1 year
                searchTime = LocalDateTime.now().minusYears(1);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + dateType);
        }

        List<EventStat> ret = eventRepository.findEventStatByPhoneAndDate(phone, eventType, searchTime);


        JSONArray arr = new JSONArray();
        while (searchTime.isBefore(LocalDateTime.now())) {
            JSONObject obj = new JSONObject();
            obj.put("x", searchTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            obj.put("y", 0);
            arr.put(obj);
            searchTime = searchTime.plusDays(1);
        }

        int index = 0;
        if (ret.size() != 0) {
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);

                String init = obj.getString("x");
                EventStat stat = ret.get(index);

                if (init.equals(stat.getDate())) {
                    obj.put("y", stat.getCount());
                    arr.put(i, obj);
                    index++;
                }

                if (index == ret.size()) {
                    break;
                }
            }
        }

        JSONObject obj = new JSONObject();
        obj.put("inactivity", arr);
        return obj;
    }

    @Transactional(readOnly = true)
    public JSONObject getEnvData(String phone, int dateType) {
        LocalDateTime searchTime;

        switch (dateType) {
            case 0: // last 1 day
                searchTime = LocalDateTime.now().minusDays(1);
                break;
            case 1: // last 1 week
                searchTime = LocalDateTime.now().minusWeeks(1);
                break;
            case 2: // last 1 month
                searchTime = LocalDateTime.now().minusMonths(1);
                break;
            case 3: // last 1 year
                searchTime = LocalDateTime.now().minusYears(1);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + dateType);
        }

        List<EnvironmentEntity> list = environmentQueryRepository.getListByDate(phone, searchTime);

        JSONArray tempJSONArr = new JSONArray();
        JSONArray humiJSONArr = new JSONArray();
        JSONArray illuJSONArr = new JSONArray();

        for (EnvironmentEntity entity : list) {
            String temp = entity.getTemperature();
            String[] tempArr = temp.split(";");
            LocalDateTime date = entity.getSvRegDate();
            for (int i = 0; i < tempArr.length; i++) {
                JSONObject obj = new JSONObject();
                obj.put("x", date.minusHours(i));
                obj.put("y", tempArr[i]);
                tempJSONArr.put(obj);
            }

            String humi = entity.getHumidity();
            String[] humiArr = humi.split(";");
            for (int i = 0; i < humiArr.length; i++) {
                JSONObject obj = new JSONObject();
                obj.put("x", date.minusHours(i));
                obj.put("y", humiArr[i]);
                humiJSONArr.put(obj);
            }

            String illu = entity.getIllumination();
            String[] illuArr = illu.split(";");
            for (int i = 0; i < illuArr.length; i++) {
                JSONObject obj = new JSONObject();
                obj.put("x", date.minusMinutes(i * 10));
                obj.put("y", illuArr[i]);
                illuJSONArr.put(obj);
            }
        }

        JSONObject ret = new JSONObject();
        ret.put("temperature", tempJSONArr);
        ret.put("humidity", humiJSONArr);
        ret.put("illuminance", illuJSONArr);

        return ret;
    }

    @Transactional(readOnly = true)
    public JSONObject getActData(String phone, int dateType) {
        LocalDateTime searchTime;

        switch (dateType) {
            case 0: // last 1 hour
                searchTime = LocalDateTime.now().minusHours(3);
                break;
            case 1: // last 4 hour
                searchTime = LocalDateTime.now().minusHours(4);
                break;
            case 2: // last 12 hour
                searchTime = LocalDateTime.now().minusHours(12);
                break;
            case 3: // last 1 day
                searchTime = LocalDateTime.now().minusDays(1);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + dateType);
        }

        List<ActivityEntity> list = activityQueryRepository.getListByDate(phone, searchTime);

        JSONArray actJSONArr = new JSONArray();

        for (ActivityEntity activityEntity : list) {
            JSONObject obj = new JSONObject();
            obj.put("x", activityEntity.getSvRegDate());
            obj.put("y", activityEntity.getCount());
            actJSONArr.put(obj);
        }

        JSONObject ret = new JSONObject();
        ret.put("activity", actJSONArr);
        return ret;
    }

    @Transactional(readOnly = true)
    public JSONObject getRadarData(String phone, LocalDateTime date) {
        List<RadarEntity> list = radarQueryRepository.getToday(phone, date);

        JSONArray actJSONArr = new JSONArray();
        JSONArray heartJSONArr = new JSONArray();
        JSONArray breathJSONArr = new JSONArray();
        JSONObject locJSONArr = new JSONObject();
        JSONArray am = new JSONArray();
        JSONArray pm = new JSONArray();
        for(RadarEntity radarEntity : list){
            JSONObject obj = new JSONObject();
            obj.put("x", radarEntity.getX());
            obj.put("y", radarEntity.getY());
            obj.put("date", radarEntity.getSvRegDate().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
            if(radarEntity.getSvRegDate().getHour()<12){
                am.put(obj);
            }else{
                pm.put(obj);
            }

            JSONObject actObj = new JSONObject();
            JSONObject heartObj = new JSONObject();
            JSONObject breathObj = new JSONObject();

            actObj.put("x", radarEntity.getSvRegDate());
            actObj.put("y", radarEntity.getCount());
            actJSONArr.put(actObj);

            breathObj.put("x", radarEntity.getSvRegDate());
            breathObj.put("y", radarEntity.getBreath());
            breathJSONArr.put(breathObj);

            heartObj.put("x", radarEntity.getSvRegDate());
            heartObj.put("y", radarEntity.getHeartRate());
            heartJSONArr.put(heartObj);

        }

        JSONObject ret = new JSONObject();
        locJSONArr.put("am", am);
        locJSONArr.put("pm", pm);
        ret.put("heart", heartJSONArr);
        ret.put("breath", breathJSONArr);
        ret.put("activity", actJSONArr);
        ret.put("radar", locJSONArr);
        return ret;
    }

    @Transactional(readOnly = true)
    public JSONObject getSensorDataOne(String phone) {
        JSONObject ret = new JSONObject();
        EnvironmentEntity environmentEntity = environmentRepository.findTopByPhoneOrderBySvRegDateDesc(phone).orElse(null);
        ActivityEntity activityEntity = activityRepository.findTopByPhoneOrderBySvRegDateDesc(phone).orElse(null);
        String temp = "0";
        String humi = "0";
        String illu = "0";
        int act = 0;
        LocalDateTime regDate = null;
        if (environmentEntity != null) {
            temp = environmentEntity.getTemperature().split(";")[0];
            humi = environmentEntity.getHumidity().split(";")[0];
            illu = environmentEntity.getIllumination().split(";")[0];
            regDate = environmentEntity.getGwRegDate();
            ret.put("temperature", temp);
            ret.put("humidity", humi);
            ret.put("illumination", illu);
            ret.put("regDate", regDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        }
        if (activityEntity != null) {
            act = activityEntity.getCount();
            ret.put("activity", act);
        }
        return ret;
    }
}
