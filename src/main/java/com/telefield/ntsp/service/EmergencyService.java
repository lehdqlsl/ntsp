package com.telefield.ntsp.service;

import com.telefield.ntsp.define.GatewayFailureStatus;
import com.telefield.ntsp.domain.admin.emergency_action.EmergencyAction;
import com.telefield.ntsp.domain.admin.emergency_action.EmergencyActionRepository;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureRepository;
import com.telefield.ntsp.domain.device.sensor_failure.SensorFailure;
import com.telefield.ntsp.domain.event.emergency.Emergency;
import com.telefield.ntsp.domain.event.emergency.EmergencyQueryRepository;
import com.telefield.ntsp.domain.event.emergency.EmergencyRepository;
import com.telefield.ntsp.domain.event.entity.Event;
import com.telefield.ntsp.domain.search.HistorySearchDto;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmergencyService {
    private final EmergencyRepository emergencyRepository;
    private final EmergencyQueryRepository emergencyQueryRepository;
    private final EmergencyActionRepository emergencyActionRepository;

    Logger logger = LoggerFactory.getLogger(EmergencyService.class);

    public List<Emergency> searchEmergency(HistorySearchDto historySearchDto) {

        if (historySearchDto.getCloseType().equals("1")) {
            historySearchDto.setClosed(true);
        } else if (historySearchDto.getCloseType().equals("2")) {
            historySearchDto.setClosed(false);
        }

        return emergencyQueryRepository.emergencySearch(historySearchDto);
    }

    @Transactional
    public void closedNoActFailure(String phone, Event event) {
        List<Emergency> list = emergencyQueryRepository.findNoActivityByClose(phone);
        for (Emergency emergency : list) {
            EmergencyAction emergencyAction = EmergencyAction.builder()
                    .emergencyId(emergency)
                    .text(event.getGwRegDate()+" 활동이벤트가 발생하여 자동 클로즈 되었음")
                    .title("해결됨")
                    .user("System")
                    .build();

            emergencyActionRepository.save(emergencyAction);

            emergency.setClosed(true);
            emergency.setClosedDate(LocalDateTime.now());
            emergencyRepository.save(emergency);
        }
    }

    public void closedEmerCallFailure(String phone, Event event) {
       // 마지막 응급호출 이벤트 조회
        Emergency emergency = emergencyQueryRepository.findLastEmerCall(phone);

        if(emergency != null){
            EmergencyAction emergencyAction = EmergencyAction.builder()
                    .emergencyId(emergency)
                    .text(event.getGwRegDate()+" 응급호출 취소 이벤트가 발생하여 자동 클로즈 되었음")
                    .title("해결됨")
                    .user("System")
                    .build();

            emergencyActionRepository.save(emergencyAction);

            emergency.setClosed(true);
            emergency.setClosedDate(LocalDateTime.now());
            emergencyRepository.save(emergency);
        }
    }

    public void closedFireFailure(String phone, Event event) {
        // 마지막 화재 이벤트 조회
        Emergency emergency = emergencyQueryRepository.findLastFire(phone);

        if(emergency != null){
            EmergencyAction emergencyAction = EmergencyAction.builder()
                    .emergencyId(emergency)
                    .text(event.getGwRegDate()+" 화재 해제 이벤트가 발생하여 자동 클로즈 되었음")
                    .title("해결됨")
                    .user("System")
                    .build();

            emergencyActionRepository.save(emergencyAction);

            emergency.setClosed(true);
            emergency.setClosedDate(LocalDateTime.now());
            emergencyRepository.save(emergency);
        }
    }
}
