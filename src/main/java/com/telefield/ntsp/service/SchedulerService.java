package com.telefield.ntsp.service;

import com.telefield.ntsp.define.GatewayFailureStatus;
import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureRepository;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusQueryRepository;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusRepository;
import com.telefield.ntsp.domain.event.entity.EventRepository;
import com.telefield.ntsp.domain.recipient.info.RecipientInfo;
import com.telefield.ntsp.domain.recipient.info.RecipientInfoRepository;
import com.telefield.ntsp.domain.statistics.StatisticsEmergency;
import com.telefield.ntsp.domain.statistics.StatisticsEmergencyRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Component
@RequiredArgsConstructor
public class SchedulerService {
    private final GatewayStatusRepository gatewayStatusRepository;
    private final GatewayStatusQueryRepository gatewayStatusQueryRepository;
    private final GatewayFailureRepository gatewayFailureRepository;
    private final EventService eventService;
    private final EventRepository eventRepository;
    private final RecipientInfoRepository recipientInfoRepository;
    private final FCMService fcmService;
    private final StatisticsEmergencyRepository statisticsEmergencyRepository;

    Logger logger = LoggerFactory.getLogger(SchedulerService.class);

    @Scheduled(cron = "0 */10 * * * *")
    public void dataNotReceive() throws ExecutionException, InterruptedException {
        List<GatewayStatusEntity> list = gatewayStatusQueryRepository.findNotReceive();

        for (GatewayStatusEntity entity : list) {
            GatewayFailure gatewayFailure = gatewayFailureRepository.save(
                    GatewayFailure.builder()
                            .phone(entity.getPhone())
                            .regDate(entity.getSend_reg_date())
                            .gatewayFailureStatus(GatewayFailureStatus.Non_Receive)
                            .build());

            entity.setGatewayStatus(GatewayStatus.Non_Receive);
            gatewayStatusRepository.save(entity);


            RecipientInfo recipientInfo = recipientInfoRepository.findByPhone(entity.getPhone()).get(0);
            fcmService.sendFailureFCM(gatewayFailure.getFailureId(), GatewayFailureStatus.Non_Receive, recipientInfo.getPhone(), recipientInfo.getName());
        }
    }

//    @Scheduled(cron = "0 */1 * * * *")
    @Scheduled(cron = "0 0 1 * * *")
    public void Stat() {
        //어제 발생한 이벤트를 GW번호별로 조회하여 테이블에 삽입
        // 조회 이벤트: 활동미감지, 외출
        Map<Byte, Integer> map = eventService.getTodayEmergencyCount();

        StatisticsEmergency statisticsEmergency = StatisticsEmergency.builder()
                .emergency(map.getOrDefault((byte)15,0))
                ._119(map.getOrDefault((byte)2,0))
                .fire(map.getOrDefault((byte)11,0))
                .noActivity(map.getOrDefault((byte)7,0))
                .regDate(LocalDate.now().minusDays(1))
                .build();

        statisticsEmergencyRepository.save(statisticsEmergency);


        //get This Month
//        LocalDateTime start = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay();
//        LocalDateTime end = LocalDate.now().plusMonths(1).with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay();
//
//        while(start.isBefore(end)){
//            Map<Byte, Integer> map = eventService.getTodayEmergencyCount(start.toLocalDate(), start.plusDays(1).toLocalDate());
//
//            StatisticsEmergency statisticsEmergency = StatisticsEmergency.builder()
//                    .emergency(map.getOrDefault((byte)15,0))
//                    ._119(map.getOrDefault((byte)2,0))
//                    .fire(map.getOrDefault((byte)11,0))
//                    .noActivity(map.getOrDefault((byte)7,0))
//                    .regDate(start.toLocalDate())
//                    .build();
//            statisticsEmergencyRepository.save(statisticsEmergency);
//
//            start = start.plusDays(1);
//        }
    }
}
