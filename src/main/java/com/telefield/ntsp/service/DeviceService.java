package com.telefield.ntsp.service;

import com.telefield.ntsp.define.*;
import com.telefield.ntsp.domain.device.DeviceDto;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureQueryRepository;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureRepository;
import com.telefield.ntsp.domain.device.gateway_status.DeviceId;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusQueryRepository;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusRepository;
import com.telefield.ntsp.domain.device.gw_cycle.Cycle;
import com.telefield.ntsp.domain.device.gw_cycle.CycleDto;
import com.telefield.ntsp.domain.device.gw_cycle.CycleRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.DateBaseEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.SensorCycle;
import com.telefield.ntsp.domain.device.sensor_cycle.SensorCycleRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.activity.ActivityEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.activity.ActivityRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.radar.RadarEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.radar.RadarRepository;
import com.telefield.ntsp.domain.device.sensor_failure.SensorFailure;
import com.telefield.ntsp.domain.device.sensor_failure.SensorFailureQueryRepository;
import com.telefield.ntsp.domain.device.sensor_failure.SensorFailureRepository;
import com.telefield.ntsp.domain.device.sensor_status.SensorStatusEntity;
import com.telefield.ntsp.domain.device.sensor_status.SensorStatusQueryRepository;
import com.telefield.ntsp.domain.device.sensor_status.SensorStatusRepository;
import com.telefield.ntsp.domain.event.entity.EventDto;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DeviceService {
    // Service
    private final FailureService failureService;
    // Service

    private final GatewayFailureRepository gatewayFailureRepository;
    private final GatewayFailureQueryRepository gatewayFailureQueryRepository;
    private final GatewayStatusRepository gatewayStatusRepository;
    private final GatewayStatusQueryRepository deviceStatusQueryRepository;
    private final SensorStatusQueryRepository sensorStatusQueryRepository;
    private final SensorFailureRepository sensorFailureRepository;
    private final SensorFailureQueryRepository sensorFailureQueryRepository;
    private final SensorStatusRepository sensorStatusRepository;
    private final CycleRepository cycleRepository;
    private final SensorCycleRepository sensorCycleRepository;
    private final EnvironmentRepository environmentRepository;
    private final ActivityRepository activityRepository;
    private final RadarRepository radarRepository;

    Logger logger = LoggerFactory.getLogger(DeviceService.class);

    @Transactional(readOnly = true)
    public HashMap<GatewayStatus, Long> getTotalGatewayStatusCount() {
        HashMap<GatewayStatus, Long> map = new HashMap<GatewayStatus, Long>();

        long normal = gatewayStatusRepository.countByGatewayStatus(GatewayStatus.Normal);
        long as = gatewayStatusRepository.countByGatewayStatus(GatewayStatus.AS);
        long non_receive = gatewayStatusRepository.countByGatewayStatus(GatewayStatus.Non_Receive);
        long unplug = gatewayStatusRepository.countByGatewayStatus(GatewayStatus.Unplug);

        map.put(GatewayStatus.Normal, normal);
        map.put(GatewayStatus.AS, as);
        map.put(GatewayStatus.Non_Receive, non_receive);
        map.put(GatewayStatus.Unplug, unplug);
        return map;
    }

    @Transactional(readOnly = true)
    public List<GatewayFailure> getCurrentFailureGateway() {
        return gatewayFailureQueryRepository.getFailureList();
    }

    @Transactional(readOnly = true)
    public List<GatewayStatusEntity> getASGateway() {
        return gatewayStatusRepository.findByGatewayStatus(GatewayStatus.AS);
    }

    private HashMap<String, Long> getSensorCountByType(byte devType) {
        HashMap<String, Long> map = new HashMap<String, Long>();
        map.put("normal", sensorStatusQueryRepository.countSensorNormal(devType));
        map.put("low_battery", sensorStatusQueryRepository.countSensorLowBattery(devType));
        map.put("lowest_battery", sensorStatusQueryRepository.countSensorLowestBattery(devType));
        map.put("bad_network", sensorStatusQueryRepository.countSensorBadNetwork(devType));
        map.put("not_receive", sensorStatusQueryRepository.countSensorNotReceive(devType));
        return map;
    }

    @Transactional(readOnly = true)
    public HashMap<Byte, HashMap<String, Long>> getSensorCount() {
        HashMap<Byte, HashMap<String, Long>> map = new HashMap<Byte, HashMap<String, Long>>();
        map.put(DeviceType.ACTIVITY_SENSOR, getSensorCountByType(DeviceType.ACTIVITY_SENSOR));
        map.put(DeviceType.DOOR_SENSOR, getSensorCountByType(DeviceType.DOOR_SENSOR));
        map.put(DeviceType.EMERGENCY_CALLER, getSensorCountByType(DeviceType.EMERGENCY_CALLER));
        map.put(DeviceType.FIRE_SENSOR, getSensorCountByType(DeviceType.FIRE_SENSOR));
        return map;
    }

    @Transactional(readOnly = true)
    public List<SensorFailure> getSensorFailList() {
        return sensorFailureQueryRepository.getFailureList();
    }

    @Transactional(readOnly = true)
    public GatewayStatusEntity getGatewayStatus(String phone) {
        return gatewayStatusRepository.findById(phone).orElse(null);
    }

    public GatewayStatusEntity saveGatewayStatus(GatewayStatusEntity entity) {
        return gatewayStatusRepository.save(entity);
    }

    @Transactional
    public void saveCycle(CycleDto cycle) {
        GatewayStatusEntity beforeGatwayStatus = gatewayStatusRepository.findById(cycle.getPhone()).orElse(null);
        GatewayStatusEntity currentGatewayStatus = cycle.toGatewayStatusEntity();

        logger.info("beforeGatwayStatus:" + beforeGatwayStatus);
        logger.info("currentGatewayStatus:" + currentGatewayStatus);

        // 게이트웨이 주기보고 처리
        Cycle saveCycle = cycleRepository.save(cycle.toCycleEntity());

        // 주기보고가 들어오면 마지막 미수신 장애 해결로직.

        if (cycle.getPower_state() == PowerState.PLUG) {
            currentGatewayStatus.setGatewayStatus(GatewayStatus.Normal);
            if (beforeGatwayStatus != null) {
                GatewayStatus beforeState = beforeGatwayStatus.getGatewayStatus();
                if (beforeState == GatewayStatus.Unplug || beforeState == GatewayStatus.Non_Receive) {
                    // 자동 장애 해제
                    failureClose(cycle.getPhone(), transGatewayFailureStatus(beforeState), "System");
                } else if (beforeState == GatewayStatus.AS) {

                }
            }
        } else if (cycle.getPower_state() == PowerState.UNPLUG) {
            currentGatewayStatus.setGatewayStatus(GatewayStatus.Unplug);
            GatewayStatus beforeState = beforeGatwayStatus.getGatewayStatus();
            if (beforeState == GatewayStatus.Normal) {
                // 신규 장애
                createNewFailure(cycle.getPhone(), GatewayFailureStatus.Unplug);
            }
        }
        gatewayStatusRepository.save(currentGatewayStatus);
        // 게이트웨이 주기보고 처리

        // 온/습/조도 처리 --- START ---
        EnvironmentEntity environmentEntity = cycle.toEnvironment();
        if (environmentEntity != null) {
            EnvironmentEntity last = environmentRepository.findTopByPhoneOrderBySvRegDateDesc(cycle.getPhone()).orElse(null);
            if (Save4HourSensorDate(setDate(environmentEntity, cycle.getRegDate()), last)) {
                environmentRepository.save(environmentEntity);
            }
        }
        // 온/습/조도 처리 --- END ---

//        // 활동량 처리 처리 --- START ---
//        ActivityEntity activityEntity = cycle.toActivity();
//        if (activityEntity != null) {
//            ActivityEntity last = activityRepository.findTopByPhoneOrderBySvRegDateDesc(cycle.getPhone()).orElse(null);
//            if(Save4HourSensorDate(setDate(activityEntity, cycle.getRegDate()),last)){
//                activityRepository.save(activityEntity);
//            }
//        }
//        // 활동량 처리 --- END ---
//
//        // 레이더 처리 처리 --- START ---
//        RadarEntity radarEntity = cycle.toRadar();
//        if (radarEntity != null) {
//            RadarEntity last = radarRepository.findTopByPhoneOrderBySvRegDateDesc(cycle.getPhone()).orElse(null);
//            if(Save4HourSensorDate(setDate(radarEntity, cycle.getRegDate()),last)){
//                radarRepository.save(radarEntity);
//            }
//        }
//        // 레이더 처리 --- END ---

        // 센서 주기보고 처리
        List<SensorStatusEntity> list = cycle.toSensorListEntity();
        long cycleId = saveCycle.getId();

        for (SensorStatusEntity currentSensorStatus : list) {
            logger.info("currentSensorStatus:" + currentSensorStatus);
            // 센서 주기보고 저장
            SensorCycle sensorCycle = SensorCycle.builder()
                    .battery(currentSensorStatus.getBattery())
                    .cycleId(cycleId)
                    .devType(currentSensorStatus.getDevType())
                    .gwRegDate(currentSensorStatus.getSend_reg_date())
                    .macAddr(currentSensorStatus.getMacAddr())
                    .network(currentSensorStatus.getNetwork())
                    .sensitivity(currentSensorStatus.getSensitivity())
                    .beforeNetwork(currentSensorStatus.getBeforeNetwork())
                    .build();
            sensorCycleRepository.save(sensorCycle);

            // 센서 상태 저장
            sensorStatusRepository.save(currentSensorStatus);

            // 1. 센서 상태 확인
            byte battery = currentSensorStatus.getBattery();
            byte network = currentSensorStatus.getNetwork();

            SensorFailure sensorBatteryFailure = sensorFailureQueryRepository.getSensorFailureOne(currentSensorStatus.getMacAddr(), SensorFailureStatus.BATTERY_LOWEST).orElse(null);
            SensorFailure sensorNetworkFailure = sensorFailureQueryRepository.getSensorFailureOne(currentSensorStatus.getMacAddr(), SensorFailureStatus.NETWORK_BAD).orElse(null);

            if (battery == 2) {
                if (sensorBatteryFailure != null) {
                    failureService.closedSensorFailure(sensorBatteryFailure);
                }
            }

            if (network == 1) {
                if (sensorNetworkFailure != null) {
                    failureService.closedSensorFailure(sensorNetworkFailure);
                }
            }

            if (battery == 0) {
                if (sensorBatteryFailure == null) {
                    sensorFailureRepository.save(currentSensorStatus.toSensorFailureEntity(SensorFailureStatus.BATTERY_LOWEST));
                }
            }

            if (network == 0) {
                if (sensorNetworkFailure == null) {
                    sensorFailureRepository.save(currentSensorStatus.toSensorFailureEntity(SensorFailureStatus.NETWORK_BAD));
                }
            }
        }
        // 센서 주기보고 처리
    }

    public List<DeviceDto> getDeviceStatus(String phone) {
        List<DeviceDto> list = new ArrayList<>();
        GatewayStatusEntity gatewayStatus = gatewayStatusRepository.findById(phone).orElse(null);
        List<SensorStatusEntity> sensorStatus = sensorStatusRepository.findByPhone(phone);

        list.add(gatewayStatus.toDeviceDto());

        for (SensorStatusEntity entity : sensorStatus) {
            list.add(entity.toDeviceDto());
        }

        return list;
    }

    private SensorFailure createFailEntity(DeviceId id, SensorFailureStatus networkBad) {
        return SensorFailure.builder()
                .devType(id.getDevType())
                .phone(id.getPhone())
                .sensorFailureStatus(SensorFailureStatus.NETWORK_BAD)
                .build();
    }

    private void UpdateGatewayStatus(CycleDto cycle, GatewayFailureStatus gatewayStatus) {
/*        Optional<GatewayFailure> gatewayFailure = gatewayFailureRepository.findByPhoneAndClosedAndGatewayFailureStatus(cycle.getPhone(),false,gatewayStatus.ordinal());
        gatewayFailure.get().setClosed(true);
        gatewayFailureRepository.save(gatewayFailure.get());*/
    }

    private void UpdateSensoryStatus(SensorStatusEntity sensorStatusEntity, SensorFailureStatus sensorFailureStatus) {
       /* SensorFailure sensorFailure = sensorFailureRepository
                .findByPhoneAndDevTypeAndSensorStateAndClosed(
                        sensorStatusEntity.getId().getPhone(),
                        sensorStatusEntity.getId().getDevType(),
                        sensorFailureStatus,
                        false
                );
        sensorFailure.setClosed(true);
        sensorFailureRepository.save(sensorFailure);*/
    }


    public GatewayFailure unPlugProcess(String phone) {
        List<GatewayFailure> gatewayFailureList = gatewayFailureRepository.findByPhoneAndGatewayFailureStatusAndIsClosed(phone, GatewayFailureStatus.Unplug, false);

        GatewayFailure gatewayFailure = getLastFailiure(gatewayFailureList);

        if (gatewayFailure == null) {
            //신규 장애 생성
            gatewayFailure = gatewayFailureRepository.save(
                    GatewayFailure.builder()
                            .phone(phone)
                            .gatewayFailureStatus(GatewayFailureStatus.Unplug)
                            .build()
            );
        }

        return gatewayFailure;
    }

    @Transactional
    public GatewayFailure getLastFailiure(List<GatewayFailure> list) {
        if (list.isEmpty()) {
            return null;
        } else if (list.size() > 1) {
            for (int i = 1; i < list.size(); i++) {
                gatewayFailureRepository.delete(list.get(i));
            }
        }
        return list.get(0);
    }

    public void createNewFailure(String phone, GatewayFailureStatus gatewayFailureStatus) {
        List<GatewayFailure> list = gatewayFailureRepository.findByPhoneAndGatewayFailureStatusAndIsClosed(phone, gatewayFailureStatus, false);
        GatewayFailure gatewayFailure = getLastFailiure(list);
        if (gatewayFailure == null) {
            gatewayFailureRepository.save(
                    GatewayFailure.builder()
                            .phone(phone)
                            .gatewayFailureStatus(gatewayFailureStatus)
                            .build());
        }
    }

    public void plugProcess(String phone) {
        List<GatewayFailure> list = gatewayFailureRepository.findByPhoneAndGatewayFailureStatusAndIsClosed(phone, GatewayFailureStatus.Unplug, false);
        GatewayFailure gatewayFailure = getLastFailiure(list);
        if (gatewayFailure != null) {
            failureService.deviceFailureClose(gatewayFailure, true, "System");
        }
    }

    public void failureClose(String phone, GatewayFailureStatus gatewayFailureStatus, String user) {
        List<GatewayFailure> list = gatewayFailureRepository.findByPhoneAndGatewayFailureStatusAndIsClosed(phone, gatewayFailureStatus, false);
        GatewayFailure gatewayFailure = getLastFailiure(list);
        if (gatewayFailure != null) {
            failureService.deviceFailureClose(gatewayFailure, true, user);
        }
    }

    public GatewayFailureStatus transGatewayFailureStatus(GatewayStatus gatewayStatus) {
        if (gatewayStatus == GatewayStatus.Unplug) {
            return GatewayFailureStatus.Unplug;
        } else if (gatewayStatus == GatewayStatus.Non_Receive) {
            return GatewayFailureStatus.Non_Receive;
        } else {
            return GatewayFailureStatus.Non_Receive;
        }
    }

    @Transactional
    public void deleteSensor(String phone) {
        List<SensorStatusEntity> list = sensorStatusRepository.findByPhone(phone);
        if (!list.isEmpty()) {
            sensorStatusRepository.deleteByPhone(phone);
        }
    }

    public boolean Save4HourSensorDate(DateBaseEntity saveEntity, DateBaseEntity lastEntity) {
        if (lastEntity == null)
            return true;

        Duration d = Duration.between(lastEntity.getSvRegDate(), saveEntity.getSvRegDate());

        if (d.toHours() >= 4) {
            return true;
        }

        return false;
    }

    public DateBaseEntity setDate(DateBaseEntity entity, LocalDateTime regDate) {
        entity.setGwRegDate(regDate);
        entity.setSvRegDate(regDate.withMinute(0).withSecond(0));
        return entity;
    }


    public void saveAct(ActivityEntity activityEntity, RadarEntity radarEntity) {
        activityRepository.save(activityEntity);
        radarRepository.save(radarEntity);
    }

    public void saveAct(EventDto[] activity, String phone, LocalDateTime regDate) {
        List<EventDto> list = Arrays.asList(activity);
        list.forEach(x -> activityRepository.save(x.toActivity(phone,regDate)));
    }

    public void saveAct(EventDto radar, String phone, LocalDateTime regDate) {
        radarRepository.save(radar.toRadar(phone,regDate));
    }
}
