package com.telefield.ntsp.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.telefield.ntsp.define.FcmType;
import com.telefield.ntsp.define.GatewayFailureStatus;
import com.telefield.ntsp.domain.admin.fcm.FcmKey;
import com.telefield.ntsp.domain.admin.fcm.FcmKeyRepository;
import com.telefield.ntsp.domain.admin.remote.RemoteRequestRepository;
import com.telefield.ntsp.domain.event.entity.Event;
import com.telefield.ntsp.domain.recipient.notice.Notice;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class FCMService {
	private static final String FIREBASE_SERVER_KEY = "AIzaSyCN5QXzFBX5RlSDE5GqY9rz8_LHCUny49o";
	private static final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";

	private Logger logger = LoggerFactory.getLogger(FCMService.class);

	private final FcmKeyRepository fcmKeyRepository;
	private final RemoteRequestRepository remoteRequestRepository;

	@Async
	CompletableFuture<String> send(HttpEntity<String> entity) {
		RestTemplate restTemplate = new RestTemplate();
		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json;UTF-8"));
		restTemplate.setInterceptors(interceptors);		
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
		String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);
		return CompletableFuture.completedFuture(firebaseResponse);
	}

	public void sendNotice(Notice ret, List<String> fcmIdSet, String type) throws ExecutionException, InterruptedException {
		String url = "";

		// PUSH
		JSONObject body = new JSONObject();
		JSONObject data = new JSONObject();

		data.put("title", "notice");
		data.put("body", ret.getTitle() + ";" + ret.getContent() + ";" + ret.getType() + ";" + ret.getSeq() + ";" + type + ";" + url);

		body.put("registration_ids", fcmIdSet);
		body.put("data", data);
		body.put("priority", "high");

		HttpEntity<String> fcmRequest = new HttpEntity<>(body.toString());
		CompletableFuture<String> push = send(fcmRequest);
		CompletableFuture.allOf(push).join();

		logger.info("SEND FCM: " + push.get());
	}

	public void sendEmergencyFCM(long emergencyId, Event event, String name){
		JSONObject body = new JSONObject();
		JSONObject data = new JSONObject();

		data.put("title", "응급발생");
		data.put("phone", event.getPhone());
		data.put("name", name);
		data.put("eventType", event.getEventType());
		data.put("emergencyId", emergencyId);

		body.put("registration_ids", getAdminFCMList());
		body.put("data", data);
		body.put("priority", "high");

		if(!getAdminFCMList().isEmpty()){
			HttpEntity<String> fcmRequest = new HttpEntity<>(body.toString());
			CompletableFuture<String> push = send(fcmRequest);
			CompletableFuture.allOf(push).join();
			try {
				logger.info("SEND FCM: " + push.get());
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
	}

	public void sendFailureFCM(long failureId, GatewayFailureStatus gatewayFailureStatus, String phone, String name) {
		JSONObject body = new JSONObject();
		JSONObject data = new JSONObject();

		data.put("title", "장애발생");
		data.put("phone", phone);
		data.put("name", name);
		data.put("failureType", gatewayFailureStatus);
		data.put("failureId", failureId);

		body.put("registration_ids", getAdminFCMList());
		body.put("data", data);
		body.put("priority", "high");

		if(!getAdminFCMList().isEmpty()){
			HttpEntity<String> fcmRequest = new HttpEntity<>(body.toString());
			CompletableFuture<String> push = send(fcmRequest);
			CompletableFuture.allOf(push).join();
			try {
				logger.info("SEND FCM: " + push.get());
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
	}

	public void requestLog(String startDate, String endDate, String fcmId, long seq){
		// PUSH
		JSONObject body = new JSONObject();
		JSONObject data = new JSONObject();

		data.put("title", FcmType.PUSH_LOG);
		data.put("body", startDate + ";" + endDate);
		data.put("remoteSeq", seq);
		body.put("to", fcmId);
		body.put("data", data);
		body.put("priority", "high");

		HttpEntity<String> fcmRequest = new HttpEntity<>(body.toString());
		CompletableFuture<String> push = send(fcmRequest);
		CompletableFuture.allOf(push).join();

		try {
			logger.info("SEND FCM: " + push.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public void fcmRequest(String fcmId, String title, String sBody, long seq) {
		// PUSH
		JSONObject body = new JSONObject();
		JSONObject data = new JSONObject();

		data.put("title", title);
		data.put("body", sBody);
		data.put("remoteSeq", seq);
		body.put("to", fcmId);
		body.put("data", data);
		body.put("priority", "high");

		HttpEntity<String> fcmRequest = new HttpEntity<>(body.toString());
		CompletableFuture<String> push = send(fcmRequest);
		CompletableFuture.allOf(push).join();

		try {
			logger.info("SEND FCM: " + push.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	public void fcmRequest(String fcmId, String title, String sBody) {
		// PUSH
		JSONObject body = new JSONObject();
		JSONObject data = new JSONObject();

		data.put("title", title);
		data.put("body", sBody);
		body.put("to", fcmId);
		body.put("data", data);
		body.put("priority", "high");

		HttpEntity<String> fcmRequest = new HttpEntity<>(body.toString());
		CompletableFuture<String> push = send(fcmRequest);
		CompletableFuture.allOf(push).join();

		try {
			logger.info("SEND FCM: " + push.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}


	private List<String> getAdminFCMList(){
		List<FcmKey> fcmKey = fcmKeyRepository.findAll();
		List<String> ret = new ArrayList<>();
		fcmKey.forEach(x->ret.add(x.getFcmKey()));
		return ret;
	}
}