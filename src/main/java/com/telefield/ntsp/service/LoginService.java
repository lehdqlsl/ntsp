package com.telefield.ntsp.service;

import com.telefield.ntsp.domain.admin.account.User;
import com.telefield.ntsp.domain.admin.account.UserRepository;
import com.telefield.ntsp.security.JwtAuthTokenProvider;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final UserRepository userRepository;
    private final JwtAuthTokenProvider jwtAuthTokenProvider;

    Logger logger = LoggerFactory.getLogger(LoginService.class);

    public User login(String userId, String password) {
        User user = userRepository.findByUserIdAndPassword(userId, password).orElse(null);
        return userRepository.findByUserIdAndPassword(userId, password).orElse(null);
    }

    public Optional<User> getUser(String userId) {
        return userRepository.findByUserId(userId);
    }

    public User save(User user){
        return userRepository.save(user);
    }
}
