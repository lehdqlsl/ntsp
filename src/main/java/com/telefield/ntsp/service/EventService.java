package com.telefield.ntsp.service;

import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import com.telefield.ntsp.define.EventType;
import com.telefield.ntsp.define.GatewayFailureStatus;
import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusRepository;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentRepository;
import com.telefield.ntsp.domain.event.emergency.Emergency;
import com.telefield.ntsp.domain.event.emergency.EmergencyQueryRepository;
import com.telefield.ntsp.domain.event.emergency.EmergencyRepository;
import com.telefield.ntsp.domain.event.entity.*;
import com.telefield.ntsp.domain.recipient.info.RecipientInfo;
import com.telefield.ntsp.domain.recipient.info.RecipientInfoRepository;
import com.telefield.ntsp.domain.recipient.status.RecipientStatus;
import com.telefield.ntsp.domain.recipient.status.RecipientStatusRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EventService {
    private final EmergencyQueryRepository emergencyQueryRepository;
    private final EmergencyRepository emergencyRepository;
    private final EnvironmentRepository environmentRepository;
    private final EventRepository eventRepository;
    private final RecipientStatusRepository recipientStatusRepository;
    private final GatewayStatusRepository gatewayStatusRepository;
    private final DeviceService deviceService;
    private final EventTestRepository eventTestRepository;
    private final EmergencyService emergencyService;
    private final FCMService fcmService;
    private final RecipientInfoRepository recipientInfoRepository;
    private final EventQueryRepository eventQueryRepository;

    @Transactional(readOnly = true)
    public Map<String, Long> getEmergencyCount() {

        long fireCount = emergencyQueryRepository.countTodayEmergencyByEventType(EventType.FIRE_START);
        long emerCount = emergencyQueryRepository.countTodayEmergencyByEventType(EventType.EMERGENCY_CALL);
        long _119Count = emergencyQueryRepository.countTodayEmergencyByEventType(EventType._119_CALL);
        long no_activity = emergencyQueryRepository.countTodayEmergencyByEventType(EventType.NO_ACTIVITY);

        Map<String, Long> map = new HashMap<>();
        map.put("emergency", emerCount);
        map.put("fire", fireCount);
        map.put("_119", _119Count);
        map.put("no_activity", no_activity);

        return map;
    }

    @Transactional(readOnly = true)
    public Map<String, Long> getEmergencyCountThisMonth() {
        //get This Month
        LocalDateTime start = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay();
        LocalDateTime end = LocalDate.now().plusMonths(1).with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay();

        long fireCount = emergencyQueryRepository.countTodayEmergencyByEventTypeAndDate(EventType.FIRE_START,start,end);
        long emerCount = emergencyQueryRepository.countTodayEmergencyByEventTypeAndDate(EventType.EMERGENCY_CALL,start,end);
        long _119Count = emergencyQueryRepository.countTodayEmergencyByEventTypeAndDate(EventType._119_CALL,start,end);
        long no_activity = emergencyQueryRepository.countTodayEmergencyByEventTypeAndDate(EventType.NO_ACTIVITY,start,end);

        Map<String, Long> map = new HashMap<>();
        map.put("emergency", emerCount);
        map.put("fire", fireCount);
        map.put("_119", _119Count);
        map.put("no_activity", no_activity);

        return map;
    }

    @Transactional(readOnly = true)
    public Map<String, Long> getEmergencyCountLastMonth() {
        //get This Month
        LocalDateTime start = LocalDate.now().minusMonths(1).with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay();
        LocalDateTime end = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay();

        long fireCount = emergencyQueryRepository.countTodayEmergencyByEventTypeAndDate(EventType.FIRE_START,start,end);
        long emerCount = emergencyQueryRepository.countTodayEmergencyByEventTypeAndDate(EventType.EMERGENCY_CALL,start,end);
        long _119Count = emergencyQueryRepository.countTodayEmergencyByEventTypeAndDate(EventType._119_CALL,start,end);
        long no_activity = emergencyQueryRepository.countTodayEmergencyByEventTypeAndDate(EventType.NO_ACTIVITY,start,end);

        Map<String, Long> map = new HashMap<>();
        map.put("emergency", emerCount);
        map.put("fire", fireCount);
        map.put("_119", _119Count);
        map.put("no_activity", no_activity);

        return map;
    }

    @Transactional(readOnly = true)
    public List<Emergency> getEmergencyList() {
        return emergencyQueryRepository.findTodayEmergencyByClose(false);
    }

    @Transactional(readOnly = true)
    public HashMap<String, List<HashMap<String, Object>>> getEnvironmentList(String phone) {
        HashMap<String, List<HashMap<String, Object>>> retObj = new HashMap();
        List<HashMap<String, Object>> tempArr = new ArrayList<>();
        List<HashMap<String, Object>> humiArr = new ArrayList<>();
        List<HashMap<String, Object>> illuArr = new ArrayList<>();

        List<EnvironmentEntity> list = environmentRepository.findByPhone(phone);

        for (EnvironmentEntity entity : list) {

        }

        retObj.put("temperature", tempArr);
        retObj.put("humidity", humiArr);
        retObj.put("illumination", illuArr);
        return retObj;
    }

    public Event saveEvent(Event event) {
        return eventRepository.save(event);
    }

    public EventTest saveEvent(EventTest event) {
        return eventTestRepository.save(event);
    }

    public void changeUserStatus(Event event) {
        // 상태 이벤트

        // - 이전 상태가 미감지 => 활동이벤트
        // - 응급처리 반영
        // - 대상자 상태 변경
        RecipientStatus recipientStatus = recipientStatusRepository.findById(event.getPhone()).orElse(null);

        if (recipientStatus == null)
            return;

        if (recipientStatus.getCurrent_status() == EventType.NO_ACTIVITY && event.getEventType() == EventType.ACTIVITY) {
            // 응급처리 상태 해제
            emergencyService.closedNoActFailure(event.getPhone(), event);
        }

        // 상태 변경
        recipientStatus.setSend_reg_date(event.getGwRegDate());
        recipientStatus.setCurrent_status(event.getEventType());
        recipientStatusRepository.save(recipientStatus);

    }

    public void processEmergencyEvent(Event event) {
        // 응급 이벤트

        // 응급 처리
        RecipientInfo recipientInfo = recipientInfoRepository.findByPhone(event.getPhone()).get(0);
        Emergency emergency = emergencyRepository.save(event.toEmergency());

        // 푸시 발송
        fcmService.sendEmergencyFCM(emergency.getEmergencyId(), event, recipientInfo.getName());
    }

    public void processPowerPlug(Event event) {
        GatewayStatusEntity beforeGatwayStatus = gatewayStatusRepository.findById(event.getPhone()).orElse(null);

        if (beforeGatwayStatus == null)
            return;

        deviceService.plugProcess(event.getPhone());
        beforeGatwayStatus.setGatewayStatus(GatewayStatus.Normal);
        gatewayStatusRepository.save(beforeGatwayStatus);
    }

    public void processPowerUnplug(Event event) {
        GatewayStatusEntity beforeGatwayStatus = gatewayStatusRepository.findById(event.getPhone()).orElse(null);

        GatewayFailure gatewayFailure = deviceService.unPlugProcess(event.getPhone());
        beforeGatwayStatus.setGatewayStatus(GatewayStatus.Unplug);
        gatewayStatusRepository.save(beforeGatwayStatus);

        RecipientInfo recipientInfo = recipientInfoRepository.findByPhone(event.getPhone()).get(0);
        fcmService.sendFailureFCM(gatewayFailure.getFailureId(), GatewayFailureStatus.Unplug, recipientInfo.getPhone(), recipientInfo.getName());
    }

    public void processEmergencyClear(Event event) {
        // 미감지해제는 changeUserStatus에서 진행

        if (EventType.EMERGENCY_CALL_CANCEL == event.getEventType()) {
            // 응급취소
            emergencyService.closedEmerCallFailure(event.getPhone(), event);
        } else if (EventType.FIRE_END == event.getEventType()) {
            // 화재해제
            emergencyService.closedFireFailure(event.getPhone(), event);
        }
    }

    public Map<Byte, Integer> getTodayEmergencyCount() {
        LocalDate start = LocalDate.now().minusDays(1);
        LocalDate end = LocalDate.now();
        QueryResults<Tuple> queryResults = eventQueryRepository.countGroupByEventType(start,end);
        return getByteIntegerMap(queryResults);
    }

    private Map<Byte, Integer> getByteIntegerMap(QueryResults<Tuple> queryResults) {
        List<Tuple> list = queryResults.getResults();

        Map<Byte, Integer> map = new HashMap<>();

        for(Tuple tuple : list){
            byte eventType = tuple.get(0,Byte.class);
            long count = tuple.get(1,Long.class);
            if(EventType.isEmergencyEvent(eventType) || eventType == 7){
                map.put(eventType,(int)count);
            }
        }

        return map;
    }

    public Map<Byte, Integer> getTodayEmergencyCount(LocalDate start, LocalDate end) {
        QueryResults<Tuple> queryResults = eventQueryRepository.countGroupByEventType(start,end);
        return getByteIntegerMap(queryResults);
    }
}
