package com.telefield.ntsp.domain.search;

import com.telefield.ntsp.define.GatewayFailureStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class HistorySearchDto {
    String searchType;
    String phone="";
    String startDate;
    String endDate;

    String closeType;
    String failureType;

    boolean isClosed;
    GatewayFailureStatus failureValue;

    // 응급
    byte eventValue;
    // 응급
}
