package com.telefield.ntsp.domain.search;

import com.telefield.ntsp.define.GatewayFailureStatus;
import com.telefield.ntsp.define.GatewayStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class GatewaySearchDto {
    @Enumerated(EnumType.ORDINAL)
    GatewayStatus gatewayStatus;
    String phone="";
}
