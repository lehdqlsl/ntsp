package com.telefield.ntsp.domain.search;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.domain.recipient.info.RecipientInfo;
import com.telefield.ntsp.domain.recipient.status.RecipientStatus;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class RecipientSearchDto {

    private RecipientInfo recipientInfo;
    private RecipientStatus recipientStatus;

    @Builder
    public RecipientSearchDto(RecipientInfo recipientInfo, RecipientStatus recipientStatus) {
        this.recipientInfo = recipientInfo;
        this.recipientStatus = recipientStatus;
    }
}
