package com.telefield.ntsp.domain.device.sensor_cycle.fire;

import com.telefield.ntsp.domain.device.sensor_cycle.CommonSensorInfo;

import javax.persistence.*;

@Entity
@Table(name = "SENSOR_FIRE_HISTORY")
public class FireEntity extends CommonSensorInfo {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

}
