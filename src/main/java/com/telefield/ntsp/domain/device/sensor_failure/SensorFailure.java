package com.telefield.ntsp.domain.device.sensor_failure;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.define.SensorFailureStatus;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.device.sensor_status.SensorStatusEntity;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Setter
@Getter
@NoArgsConstructor
@Table(name="SERVER_SENSOR_FAILURE")
@Entity
@ToString
public class SensorFailure {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long failure_id;

    @Column(length = 25)
    private String phone;

    @Column(columnDefinition = "comment '1: 통신 불량\r\r\n2:배터리 교체'")
    @Enumerated(EnumType.ORDINAL)
    private SensorFailureStatus sensorState;

    private byte devType;

    @Column(length = 25)
    private String macAddr;

    @Column(columnDefinition = "boolean default 0 comment '0: false\r\r\n1: true'")
    private boolean isAware;

    @Column(columnDefinition = "boolean default 0 comment '0: false\r\r\n1: true'")
    private boolean isClosed;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime closedDate;

    @Builder
    public SensorFailure(String phone, byte devType, SensorFailureStatus sensorFailureStatus, String macAddr) {
        this.phone = phone;
        this.devType = devType;
        this.sensorState = sensorFailureStatus;
        this.macAddr = macAddr;
    }
}
