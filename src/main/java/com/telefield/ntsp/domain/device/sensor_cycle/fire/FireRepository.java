package com.telefield.ntsp.domain.device.sensor_cycle.fire;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FireRepository extends JpaRepository<FireEntity, Long> {

}
