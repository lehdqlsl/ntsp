package com.telefield.ntsp.domain.device.gateway_status;

import com.telefield.ntsp.define.DeviceType;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


@Getter
@Setter
@ToString
public class DeviceId {

    private String phone;

    private byte devType;

    public DeviceId(String phone, byte devType) {
        this.phone = phone;
        this.devType = devType;
    }
}
