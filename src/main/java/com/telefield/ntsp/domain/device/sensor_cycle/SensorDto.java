package com.telefield.ntsp.domain.device.sensor_cycle;

import com.telefield.ntsp.domain.device.sensor_cycle.activity.ActivityEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.radar.RadarEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Arrays;
import java.util.List;

@ToString
@Getter
@Setter
public class SensorDto {
    private byte devType;

    private String macAddr;

    private byte battery;

    private byte network;

    private byte beforeNetwork;

    private short sensitivity;

    private List<Long> temperature;

    private List<Long> humidity;

    private List<Integer> illumination;

    private String location;

    private List<Integer> count;

    private int breath;

    private int heartRate;

    public EnvironmentEntity toEnvironment(String phone) {
        return EnvironmentEntity.builder()
                .humidity(parse(humidity))
                .illumination(listToString(illumination))
                .temperature(parse(temperature))
                .phone(phone)
                .build();
    }

    public ActivityEntity toActivity(String phone) {
        return ActivityEntity.builder()
                .location(location)
                .phone(phone)
                .build();
    }

    private String parse(List<Long> list) {
        StringBuilder sb = new StringBuilder();

        if(list.isEmpty()){
            return "";
        } else{
            for (Long i : list) {
                double data = i / 100.0;
                String str = String.format("%.1f", data);
                sb.append(str + ";");
            }
            return sb.substring(0, sb.length() - 1);
        }
    }

    private String listToString(List<Integer> list) {
        StringBuilder sb = new StringBuilder();
        if(list.isEmpty()){
            return "";
        } else{
            for (int i : list) {
                sb.append(i + ";");
            }
            return sb.substring(0, sb.length() - 1);
        }
    }

    public RadarEntity toRadar(String phone) {
        return RadarEntity.builder()
                .location(location)
                .breath(breath)
                .heartRate(heartRate)
                .phone(phone)
                .build();
    }
}
