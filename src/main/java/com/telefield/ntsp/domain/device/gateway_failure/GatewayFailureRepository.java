package com.telefield.ntsp.domain.device.gateway_failure;

import com.telefield.ntsp.define.GatewayFailureStatus;
import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GatewayFailureRepository extends JpaRepository<GatewayFailure, Long> {
    List<GatewayFailure> findByIsClosed(boolean isClosed);
    List<GatewayFailure> findByPhoneOrderByRegDateDesc(String phone);
    List<GatewayFailure> findByPhoneAndGatewayFailureStatusAndIsClosed(String phone, GatewayFailureStatus gatewayFailureStatus, boolean isClosed);
}
