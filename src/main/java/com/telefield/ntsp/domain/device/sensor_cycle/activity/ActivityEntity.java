package com.telefield.ntsp.domain.device.sensor_cycle.activity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.domain.device.sensor_cycle.CommonSensorInfo;
import com.telefield.ntsp.domain.device.sensor_cycle.DateBaseEntity;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;


@NoArgsConstructor
@ToString
@Setter
@Getter
@Entity
@Table(name = "SENSOR_ACTIVITY_HISTORY")
public class ActivityEntity extends DateBaseEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String phone;

    @Column (length = 10)
    private String location;

    private int count;

    @Builder
    public ActivityEntity(int count, String phone, String location, LocalDateTime gwRegDate){
        this.setGwRegDate(gwRegDate);
        this.count = count;
        this.phone = phone;
        this.location = location;
    }
}
