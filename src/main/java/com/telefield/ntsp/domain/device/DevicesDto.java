package com.telefield.ntsp.domain.device;

import com.telefield.ntsp.define.GatewayStatus;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class DevicesDto {
    private String phone;

    private String macAddr;

    private byte devType;

    private byte power;

    private short battery;

    private byte network;

    private short sensitivity;


    @Builder
    public DevicesDto(String phone, String macAddr, byte devType, byte power, byte network, short sensitivity, short battery) {
        this.phone = phone;
        this.macAddr = macAddr;
        this.devType = devType;
        this.power = power;
        this.network = network;
        this.sensitivity = sensitivity;
        this.battery = battery;
    }
}
