package com.telefield.ntsp.domain.device.sensor_status;

import com.telefield.ntsp.define.DeviceType;
import com.telefield.ntsp.domain.device.gateway_status.DeviceId;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class SensorStatusDto {
    private long seq;

    private String phone;

    private String macAddr;

    private byte devType;

    private byte battery;

    private byte network;

    private short sensitivity;

    private LocalDateTime send_reg_date;

    public SensorStatusEntity toEntity() {
        return SensorStatusEntity.builder()
                .phone(phone)
                .devType(devType)
                .network(network)
                .macAddr(macAddr)
                .battery(battery)
                .send_reg_date(send_reg_date)
                .sensitivity(sensitivity)
                .build();
    }
}
