package com.telefield.ntsp.domain.device.sensor_cycle.door;

import com.telefield.ntsp.domain.device.sensor_cycle.CommonSensorInfo;

import javax.persistence.*;

@Entity
@Table(name = "SENSOR_DOOR_HISTORY")
public class DoorEntity extends CommonSensorInfo {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

}
