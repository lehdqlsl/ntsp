package com.telefield.ntsp.domain.device.sensor_failure;

import com.telefield.ntsp.define.BatteryState;
import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.define.NetworkState;
import com.telefield.ntsp.define.SensorFailureStatus;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_failure.QGatewayFailure;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class SensorFailureQueryRepository extends QuerydslRepositorySupport {
    public SensorFailureQueryRepository() {
        super(SensorFailure.class);
    }

    public List<SensorFailure> getFailureList() {
        com.telefield.ntsp.domain.device.sensor_failure.QSensorFailure qSensorFailure = com.telefield.ntsp.domain.device.sensor_failure.QSensorFailure.sensorFailure;
        List<SensorFailure> list = from(qSensorFailure)
                .where(
                        qSensorFailure.isClosed.eq(false)
                )
                .fetch();
        return list;
    }

    public Optional<SensorFailure> getSensorFailureOne(String macAddr, SensorFailureStatus sensorFailureStatus) {
        QSensorFailure qSensorFailure = QSensorFailure.sensorFailure;
        List<SensorFailure> sensorFailure = from(qSensorFailure)
                .where(qSensorFailure.macAddr.eq(macAddr)
                        .and(qSensorFailure.sensorState.eq(sensorFailureStatus))
                        .and(qSensorFailure.isClosed.eq(false)
                        )).fetch();

        if(sensorFailure.isEmpty()){
            return Optional.ofNullable(null);
        }else if(sensorFailure.size() > 1){
            deleteSensorFailures(sensorFailure);
        }
        return Optional.ofNullable(sensorFailure.get(0));
    }

    @Transactional
    public void deleteSensorFailures(List<SensorFailure> list){
        List<Long> ids = new ArrayList<>();
        for(int i=1;i<list.size();i++){
            ids.add(list.get(i).getFailure_id());
        }
        QSensorFailure qSensorFailure = QSensorFailure.sensorFailure;
        delete(qSensorFailure).where(qSensorFailure.failure_id.in(ids));
    }
}
