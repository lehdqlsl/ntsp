package com.telefield.ntsp.domain.device.sensor_status;

import com.querydsl.core.BooleanBuilder;
import com.telefield.ntsp.define.BatteryState;
import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.define.NetworkState;
import com.telefield.ntsp.domain.device.gateway_status.QGatewayStatusEntity;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Transactional(readOnly = true)
@Repository
public class SensorStatusQueryRepository extends QuerydslRepositorySupport {
    public SensorStatusQueryRepository() {
        super(SensorStatusEntity.class);
    }

    public long countSensorNotReceive(byte devType) {
        com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity qSensorStatusEntity = com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity.sensorStatusEntity;

        long count = from(qSensorStatusEntity)
                .where(qSensorStatusEntity.send_reg_date.before(LocalDateTime.now().minusDays(1))
                        .and(qSensorStatusEntity.devType.eq(devType))
                )
                .fetchCount();
        return count;
    }

    public long countSensorBadNetwork(byte devType) {
        com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity qSensorStatusEntity = com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity.sensorStatusEntity;

        long count = from(qSensorStatusEntity)
                .where(qSensorStatusEntity.send_reg_date.after(LocalDateTime.now().minusDays(1))
                        .and(qSensorStatusEntity.devType.eq(devType))
                        .and(qSensorStatusEntity.network.eq(NetworkState.BAD))
                )
                .fetchCount();

        return count;
    }

    public long countSensorLowestBattery(byte devType) {
        com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity qSensorStatusEntity = com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity.sensorStatusEntity;

        long count = from(qSensorStatusEntity)
                .where(qSensorStatusEntity.send_reg_date.after(LocalDateTime.now().minusDays(1))
                        .and(qSensorStatusEntity.devType.eq(devType))
                        .and(qSensorStatusEntity.battery.eq(BatteryState.LOWEST))
                )
                .fetchCount();

        return count;
    }

    public long countSensorLowBattery(byte devType) {
        com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity qSensorStatusEntity = com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity.sensorStatusEntity;

        long count = from(qSensorStatusEntity)
                .where(qSensorStatusEntity.send_reg_date.after(LocalDateTime.now().minusDays(1))
                        .and(qSensorStatusEntity.devType.eq(devType))
                        .and(qSensorStatusEntity.battery.eq(BatteryState.LOW))
                )
                .fetchCount();

        return count;
    }

    public long countSensorNormal(byte devType) {
        com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity qSensorStatusEntity = com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity.sensorStatusEntity;

        long count = from(qSensorStatusEntity)
                .where(qSensorStatusEntity.send_reg_date.after(LocalDateTime.now().minusDays(1))
                        .and(qSensorStatusEntity.devType.eq(devType))
                        .and(qSensorStatusEntity.battery.eq(BatteryState.NORMAL))
                        .and(qSensorStatusEntity.network.eq(NetworkState.GOOD))
                )
                .fetchCount();

        return count;
    }

    public List<SensorStatusEntity> getSearchSensor(byte devType, String phone, int network, int battery) {
        com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity qSensorStatusEntity = com.telefield.ntsp.domain.device.sensor_status.QSensorStatusEntity.sensorStatusEntity;

        BooleanBuilder builder = new BooleanBuilder();

        if (!phone.equals("")) {
            builder.and(qSensorStatusEntity.phone.eq(phone));
        }

        if (devType != 0) {
            builder.and(qSensorStatusEntity.devType.eq(devType));
        }

        if (network != 2) {
            builder.and(qSensorStatusEntity.network.eq((byte) network));
        }

        if (battery != 3) {
            builder.and(qSensorStatusEntity.battery.eq((byte) battery));
        }
        return from(qSensorStatusEntity)
                .where(builder).fetch();
    }
}
