package com.telefield.ntsp.domain.device.sensor_cycle;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import net.bytebuddy.asm.Advice;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;



@Setter
@Getter
@Entity
@NoArgsConstructor
@ToString
@Table(name="SENSOR_CYCLE_HISTORY")
public class SensorCycle {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    private long cycleId;

    private byte devType;

    @Column(length = 25)
    private String macAddr;

    @Column(columnDefinition = "tinyint comment '0: 배터리 교체\r\r\n1: 배터리 부족\r\r\n2:배터리충만'")
    private byte battery;

    @Column(columnDefinition = "tinyint comment '0: bad\r\r\n1: good'")
    private byte network;

    @Column(columnDefinition = "tinyint comment '0: bad\r\r\n1: good'")
    private byte beforeNetwork;

    private short sensitivity;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime gwRegDate;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime svRegDate;

    @Builder
    public SensorCycle(long cycleId, byte devType, String macAddr, byte battery, byte network, short sensitivity, LocalDateTime gwRegDate, byte beforeNetwork){
        this.cycleId = cycleId;
        this.devType = devType;
        this.macAddr = macAddr;
        this.battery = battery;
        this.network = network;
        this.sensitivity = sensitivity;
        this.gwRegDate = gwRegDate;
        this.beforeNetwork = beforeNetwork;
    }
}
