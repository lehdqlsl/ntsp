package com.telefield.ntsp.domain.device.gw_cycle;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CycleRepository extends JpaRepository<Cycle, Long> {
    List<Cycle> findByPhoneOrderByRegDateDesc(String phone);
}
