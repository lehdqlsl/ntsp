package com.telefield.ntsp.domain.device.sensor_cycle.activity;

import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.QEnvironmentEntity;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class ActivityQueryRepository extends QuerydslRepositorySupport {
    public ActivityQueryRepository() {
        super(ActivityEntity.class);
    }

    public List<ActivityEntity> getListByDate(String phone, LocalDateTime searchDate){
        QActivityEntity qActivityEntity = QActivityEntity.activityEntity;
        return from(qActivityEntity)
                .where(qActivityEntity.phone.eq(phone)
                .and(qActivityEntity.svRegDate.after(searchDate))
                ).orderBy(qActivityEntity.svRegDate.desc()).fetch();
    }

}
