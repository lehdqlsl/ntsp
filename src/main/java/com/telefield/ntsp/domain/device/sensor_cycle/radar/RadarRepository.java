package com.telefield.ntsp.domain.device.sensor_cycle.radar;

import com.telefield.ntsp.domain.device.sensor_cycle.activity.ActivityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RadarRepository extends JpaRepository<RadarEntity, Long> {
    Optional<RadarEntity> findTopByPhoneOrderBySvRegDateDesc(String phone);
}
