package com.telefield.ntsp.domain.device.sensor_cycle.environment;

import java.util.Optional;

public interface CommonRepository {
    Optional<EnvironmentEntity> findTopByPhoneOrderBySvRegDateDesc(String phone);
}
