package com.telefield.ntsp.domain.device.sensor_cycle;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;


@Setter
@Getter
@ToString
@MappedSuperclass
public abstract class CommonSensorInfo {
    @Column(length = 25)
    private String phone;

    private byte devType;

    @Column(length = 25)
    private String macAddr;

    @Column(columnDefinition = "tinyint comment '0: 배터리 교체\r\r\n1: 배터리 부족\r\r\n2:배터리충만'")
    private byte battery;

    @Column(columnDefinition = "tinyint comment '0: bad\r\r\n1: good'")
    private byte network;

    private short sensitivity;

    private LocalDateTime send_reg_date;

}
