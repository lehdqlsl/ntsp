package com.telefield.ntsp.domain.device.pattern;

import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public abstract class AbstractSensorFailure {

}
