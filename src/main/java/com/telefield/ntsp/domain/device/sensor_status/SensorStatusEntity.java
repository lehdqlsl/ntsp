package com.telefield.ntsp.domain.device.sensor_status;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.define.SensorFailureStatus;
import com.telefield.ntsp.domain.device.DeviceDto;
import com.telefield.ntsp.domain.device.gateway_status.DeviceId;
import com.telefield.ntsp.domain.device.sensor_failure.SensorFailure;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@NoArgsConstructor
@ToString
@Table(name = "SERVER_SENSOR_STATUS", indexes = {@Index(columnList = "phone"),@Index(columnList = "devType"),@Index(columnList = "send_reg_date")})
public class SensorStatusEntity {
    @Id
    @Column(length = 25)
    private String macAddr;

    private String phone;

    private byte devType;

    @Column(columnDefinition = "tinyint comment '0: 배터리 교체\r\r\n1: 배터리 부족\r\r\n2:배터리충만'")
    private byte battery;

    @Column(columnDefinition = "tinyint comment '0: bad\r\r\n1: good'")
    private byte network;

    @Column(columnDefinition = "tinyint comment '0: bad\r\r\n1: good'")
    private byte beforeNetwork;

    private short sensitivity;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime send_reg_date;

    @Builder
    public SensorStatusEntity(String phone, byte devType, String macAddr, byte battery, byte network, short sensitivity, LocalDateTime send_reg_date, byte beforeNetwork) {
        this.phone = phone;
        this.devType = devType;
        this.macAddr = macAddr;
        this.battery = battery;
        this.network = network;
        this.sensitivity = sensitivity;
        this.send_reg_date = send_reg_date;
        this.beforeNetwork = beforeNetwork;
    }

    public DeviceDto toDeviceDto(){
        return DeviceDto.builder()
                .devType(devType)
                .macAddr(macAddr)
                .network(network)
                .phone(phone)
                .power(battery)
                .send_reg_date(send_reg_date)
                .sensitivity(sensitivity)
                .build();
    }

    public SensorFailure toSensorFailureEntity(SensorFailureStatus sensorFailureStatus){
        return SensorFailure.builder()
                .sensorFailureStatus(sensorFailureStatus)
                .phone(phone)
                .devType(devType)
                .macAddr(macAddr)
                .build();
    }

    public DeviceId getId(){
        return new DeviceId(phone,devType);
    }
}
