package com.telefield.ntsp.domain.device;

import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class DeviceDto {
    private String phone;

    private String macAddr;

    private byte devType;

    private GatewayStatus gatewayStatus;

    private byte power;

    private byte network;

    private short sensitivity;

    private String app_ver;

    private String fw_ver;

    private LocalDateTime send_reg_date;

    @Builder
    public DeviceDto(String phone, String macAddr, byte devType, GatewayStatus gatewayStatus, byte power, byte network, short sensitivity, String app_ver, String fw_ver, LocalDateTime send_reg_date) {
        this.phone = phone;
        this.macAddr = macAddr;
        this.devType = devType;
        this.gatewayStatus = gatewayStatus;
        this.power = power;
        this.network = network;
        this.sensitivity = sensitivity;
        this.app_ver = app_ver;
        this.fw_ver = fw_ver;
        this.send_reg_date = send_reg_date;
    }
}
