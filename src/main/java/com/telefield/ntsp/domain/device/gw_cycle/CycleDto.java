package com.telefield.ntsp.domain.device.gw_cycle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.define.DeviceType;
import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.domain.device.gateway_status.DeviceId;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.SensorDto;
import com.telefield.ntsp.domain.device.sensor_cycle.activity.ActivityEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.environment.EnvironmentEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.radar.RadarEntity;
import com.telefield.ntsp.domain.device.sensor_status.SensorStatusEntity;
import com.telefield.ntsp.domain.recipient.numbers.Numbers;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.lang.reflect.Array;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class CycleDto {
    private long seq;

    private String phone;

    private String macAddr;

    private String fcmId;

    private byte devType;

    private byte power_state;

    private byte comm_state;

    private byte codiState;

    private short battery;

    private short sensitivity;

    private String app_ver;

    private String fw_ver;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;

    private SensorDto[] sensorCycle;

    private Numbers numbers;

    public Cycle toCycleEntity() {
        return Cycle.builder()
                .codiState(codiState)
                .app_ver(app_ver)
                .comm_state(comm_state)
                .devType((byte) 0)
                .fw_ver(fw_ver)
                .mac_addr(macAddr)
                .phone(phone)
                .power_state(power_state)
                .send_reg_date(regDate)
                .sensitivity(sensitivity)
                .battery(battery)
                .build();
    }

    public GatewayStatusEntity toGatewayStatusEntity(GatewayStatus gatewayStatus) {
        return GatewayStatusEntity.builder()
                .codiState(codiState)
                .fcmId(fcmId)
                .app_ver(app_ver)
                .network(comm_state)
                .fw_ver(fw_ver)
                .mac_addr(macAddr)
                .phone(phone)
                .power(power_state)
                .send_reg_date(regDate)
                .sensitivity(sensitivity)
                .gatewayStatus(gatewayStatus)
                .build();
    }

    public GatewayStatusEntity toGatewayStatusEntity() {
        return GatewayStatusEntity.builder()
                .codiState(codiState)
                .app_ver(app_ver)
                .network(comm_state)
                .fcmId(fcmId)
                .fw_ver(fw_ver)
                .mac_addr(macAddr)
                .phone(phone)
                .power(power_state)
                .send_reg_date(regDate)
                .sensitivity(sensitivity)
                .battery(battery)
                .build();
    }

    public List<SensorStatusEntity> toSensorListEntity(){
        List<SensorStatusEntity> list = new ArrayList<>();

        for(SensorDto dto : sensorCycle){
            if(dto.getDevType() != DeviceType.ENVIRONMENT_SENSOR){
                SensorStatusEntity entity = SensorStatusEntity.builder()
                        .beforeNetwork(dto.getBeforeNetwork())
                        .battery(dto.getBattery())
                        .macAddr(dto.getMacAddr())
                        .network(dto.getNetwork())
                        .send_reg_date(regDate)
                        .sensitivity(dto.getSensitivity())
                        .phone(phone)
                        .devType(dto.getDevType())
                        .build();
                list.add(entity);
            }
        }

        return list;
    }

    public EnvironmentEntity toEnvironment(){
        for(SensorDto dto : sensorCycle){
            if(dto.getDevType() == DeviceType.ENVIRONMENT_SENSOR){
                return dto.toEnvironment(phone);
            }
        }
        return null;
    }

    public ActivityEntity toActivity(){
        for(SensorDto dto : sensorCycle){
            if(dto.getDevType() == DeviceType.ACTIVITY_SENSOR){
                return dto.toActivity(phone);
            }
        }
        return null;
    }

    public RadarEntity toRadar(){
        for(SensorDto dto : sensorCycle){
            if(dto.getDevType() == DeviceType.RADAR_SENSOR){
                return dto.toRadar(phone);
            }
        }
        return null;
    }
}
