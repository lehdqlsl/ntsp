package com.telefield.ntsp.domain.device.sensor_cycle.door;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoorRepository extends JpaRepository<DoorEntity, Long> {

}
