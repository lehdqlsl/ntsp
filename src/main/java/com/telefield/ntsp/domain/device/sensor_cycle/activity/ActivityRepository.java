package com.telefield.ntsp.domain.device.sensor_cycle.activity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActivityRepository extends JpaRepository<ActivityEntity, Long> {
    Optional<ActivityEntity> findTopByPhoneOrderBySvRegDateDesc(String phone);
}
