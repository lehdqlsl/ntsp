package com.telefield.ntsp.domain.device.sensor_cycle.environment;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.domain.device.sensor_cycle.CommonSensorInfo;
import com.telefield.ntsp.domain.device.sensor_cycle.DateBaseEntity;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.json.JSONObject;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;



@ToString
@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "SENSOR_ENVIRONMENT_HISTORY")
public class EnvironmentEntity extends DateBaseEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String phone;

    private String temperature;

    private String humidity;

    private String illumination;

    @Builder
    public EnvironmentEntity(String phone, String temperature, String humidity, String illumination) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.illumination = illumination;
        this.phone = phone;
    }
}
