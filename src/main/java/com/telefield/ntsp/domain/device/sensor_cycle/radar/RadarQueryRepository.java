package com.telefield.ntsp.domain.device.sensor_cycle.radar;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class RadarQueryRepository extends QuerydslRepositorySupport {
    public RadarQueryRepository() {
        super(RadarEntity.class);
    }

    public List<RadarEntity> getToday(String phone, LocalDateTime date) {
        QRadarEntity qRadarEntity = QRadarEntity.radarEntity;
        return from(qRadarEntity)
                .where(qRadarEntity.phone.eq(phone)
                        .and(qRadarEntity.svRegDate.after(date))
                ).orderBy(qRadarEntity.svRegDate.desc()).fetch();
    }
}
