package com.telefield.ntsp.domain.device.pattern.define_gateway_failure;

import com.telefield.ntsp.define.PowerState;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureRepository;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusRepository;
import com.telefield.ntsp.domain.device.gw_cycle.CycleDto;
import com.telefield.ntsp.domain.device.pattern.AbstractGatewayFailure;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


@Component
public class GatewayUnplugFailure extends AbstractGatewayFailure {


    public GatewayUnplugFailure(GatewayFailureRepository gatewayFailureRepository, GatewayStatusRepository gatewayStatusRepository) {
        super(gatewayFailureRepository, gatewayStatusRepository);
    }

    @Override
    public boolean isFailure(CycleDto cycleDto) {
        return (cycleDto.getPower_state() == PowerState.UNPLUG);
    }
}
