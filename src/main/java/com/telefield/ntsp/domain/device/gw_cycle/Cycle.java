package com.telefield.ntsp.domain.device.gw_cycle;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@NoArgsConstructor
@ToString
@Table(name = "GW_CYCLE_HISTORY")
public class Cycle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 25)
    private String phone;

    @Column(length = 25)
    private String macAddr;

    private byte devType;

    private byte power;

    private byte network;

    private short sensitivity;

    private byte codiState;

    private short battery;

    @Column(length = 10)
    private String app_ver;

    @Column(length = 5)
    private String fw_ver;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime send_reg_date;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;

    @Builder
    public Cycle(String phone, String mac_addr, byte codiState, byte devType, byte power_state, byte comm_state, short sensitivity, short battery, String app_ver, String fw_ver, LocalDateTime send_reg_date) {
        this.phone = phone;
        this.macAddr = mac_addr;
        this.power = power_state;
        this.network = comm_state;
        this.sensitivity = sensitivity;
        this.app_ver = app_ver;
        this.fw_ver = fw_ver;
        this.send_reg_date = send_reg_date;
        this.devType = devType;
        this.battery = battery;
        this.codiState = codiState;
    }
}
