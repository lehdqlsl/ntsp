package com.telefield.ntsp.domain.device.gateway_failure;

import com.querydsl.core.BooleanBuilder;
import com.telefield.ntsp.define.GatewayFailureStatus;
import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.domain.device.gateway_status.QGatewayStatusEntity;
import com.telefield.ntsp.domain.search.HistorySearchDto;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class GatewayFailureQueryRepository extends QuerydslRepositorySupport {
    public GatewayFailureQueryRepository() {
        super(GatewayFailure.class);
    }

    public List<GatewayFailure> getFailureList() {
        QGatewayFailure qGatewayFailure = QGatewayFailure.gatewayFailure;
        List<GatewayFailure> list = from(qGatewayFailure)
                .where(
                        qGatewayFailure.isClosed.eq(false)
                                .and(qGatewayFailure.gatewayFailureStatus.in(GatewayFailureStatus.Non_Receive, GatewayFailureStatus.Unplug))

                ).orderBy(qGatewayFailure.regDate.desc())
                .fetch();
        return list;
    }

    public GatewayFailure getFailureByPhone(String phone) {
        QGatewayFailure qGatewayFailure = QGatewayFailure.gatewayFailure;
        GatewayFailure list = from(qGatewayFailure)
                .where(qGatewayFailure.phone.eq(phone)
                        .and(qGatewayFailure.isClosed.eq(false))
                        .and(qGatewayFailure.gatewayFailureStatus.in(GatewayFailureStatus.Non_Receive, GatewayFailureStatus.Unplug))

                ).orderBy(qGatewayFailure.regDate.desc()).limit(1)
                .fetchOne();
        return list;
    }

    public List<GatewayFailure> searchHistory(LocalDate startDate, LocalDate endDate) {
        QGatewayFailure qGatewayFailure = QGatewayFailure.gatewayFailure;
        return from(qGatewayFailure)
                .where(qGatewayFailure.regDate.between(startDate.atStartOfDay(), endDate.atTime(23,59))
                ).orderBy(qGatewayFailure.regDate.desc())
                .fetch();
    }

    public List<GatewayFailure> searchHistory(String phone, LocalDate startDate, LocalDate endDate) {
        QGatewayFailure qGatewayFailure = QGatewayFailure.gatewayFailure;
        return from(qGatewayFailure)
                .where(qGatewayFailure.phone.eq(phone)
                        .and(qGatewayFailure.regDate.between(startDate.atStartOfDay(), endDate.atTime(23,59))
                        )).orderBy(qGatewayFailure.regDate.desc())
                .fetch();
    }

    public List<GatewayFailure> failureSearch(HistorySearchDto historySearchDto) {
        LocalDate startDate = LocalDate.parse(historySearchDto.getStartDate());
        LocalDate endDate = LocalDate.parse(historySearchDto.getEndDate());

        QGatewayFailure qGatewayFailure = QGatewayFailure.gatewayFailure;

        BooleanBuilder builder = new BooleanBuilder();
        if(!historySearchDto.getPhone().equals("")){
            builder.and(qGatewayFailure.phone.eq(historySearchDto.getPhone()));
        }
        builder.and(qGatewayFailure.regDate.between(startDate.atStartOfDay(), endDate.atTime(23,59)));
        if(!historySearchDto.getCloseType().equals("0")){
            builder.and(qGatewayFailure.isClosed.eq(historySearchDto.isClosed()));
        }
        if(!historySearchDto.getFailureType().equals("0")){
            builder.and(qGatewayFailure.gatewayFailureStatus.eq(historySearchDto.getFailureValue()));
        }

        return from(qGatewayFailure)
                .where(builder).orderBy(qGatewayFailure.regDate.desc())
                .fetch();
    }
}
