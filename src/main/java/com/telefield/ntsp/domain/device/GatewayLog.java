package com.telefield.ntsp.domain.device;

import com.telefield.ntsp.define.GatewayStatus;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class GatewayLog {
    private String file_name;

    private String phone;

    private String regDate;

    private String size;

}
