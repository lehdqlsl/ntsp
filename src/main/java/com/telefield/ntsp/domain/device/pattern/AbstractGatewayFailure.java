package com.telefield.ntsp.domain.device.pattern;

import com.telefield.ntsp.define.GatewayFailureStatus;
import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailureRepository;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusRepository;
import com.telefield.ntsp.domain.device.gw_cycle.CycleDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public abstract class AbstractGatewayFailure {
    private final GatewayFailureRepository gatewayFailureRepository;
    private final GatewayStatusRepository gatewayStatusRepository;

    public abstract boolean isFailure(CycleDto cycleDto);
}
