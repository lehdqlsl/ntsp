package com.telefield.ntsp.domain.device.gateway_status;

import com.telefield.ntsp.define.DeviceType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class GatewayStatusDto {
    private long seq;

    private String phone;

    private String macAddr;

    private byte devType;

    private byte power;

    private byte network;

    private short sensitivity;

    private String app_ver;

    private String fw_ver;

    private LocalDateTime send_reg_date;

    public GatewayStatusEntity toGatewayStatus() {
        return GatewayStatusEntity.builder()
                .phone(phone)
                .app_ver(app_ver)
                .network(network)
                .fw_ver(fw_ver)
                .mac_addr(macAddr)
                .power(power)
                .send_reg_date(send_reg_date)
                .sensitivity(sensitivity)
                .build();
    }
}
