package com.telefield.ntsp.domain.device.sensor_failure;

import com.telefield.ntsp.define.SensorFailureStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SensorFailureRepository extends JpaRepository<SensorFailure, Long> {
   List<SensorFailure> findByIsClosed(boolean isClosed);

   //SensorFailure findByPhoneAndDevTypeAndSensorStateAndClosed(String phone, byte devType, SensorFailureStatus sensorState, boolean isClosed);
}
