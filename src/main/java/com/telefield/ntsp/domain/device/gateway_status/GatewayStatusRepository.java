package com.telefield.ntsp.domain.device.gateway_status;

import com.telefield.ntsp.define.GatewayStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GatewayStatusRepository extends JpaRepository<GatewayStatusEntity, String> {
    List<GatewayStatusEntity> findByGatewayStatus(GatewayStatus as);
    long countByGatewayStatus(GatewayStatus normal);
}
