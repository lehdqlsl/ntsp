package com.telefield.ntsp.domain.device.sensor_cycle.environment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EnvironmentRepository extends JpaRepository<EnvironmentEntity, Long> {
    List<EnvironmentEntity> findByPhone(String phone);
    Optional<EnvironmentEntity> findTopByPhoneOrderBySvRegDateDesc(String phone);
}
