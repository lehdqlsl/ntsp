package com.telefield.ntsp.domain.device.sensor_cycle.radar;

import com.telefield.ntsp.domain.device.sensor_cycle.DateBaseEntity;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;


@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "SENSOR_RADAR_HISTORY")
public class RadarEntity extends DateBaseEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String phone;

    @Column (length = 10)
    private String location;

    private int count;

    private int breath;

    private int heartRate;

    private int x;

    private int y;

    @Builder
    public RadarEntity(int count, String phone, String location, int breath, int heartRate, LocalDateTime gwRegDate, int x, int y){
        this.setGwRegDate(gwRegDate);
        this.count = count;
        this.phone = phone;
        this.location = location;
        this.breath = breath;
        this.heartRate = heartRate;
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "RadarEntity{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", location='" + location + '\'' +
                ", count=" + count +
                ", breath=" + breath +
                ", heartRate=" + heartRate +
                ", x=" + x +
                ", y=" + y +
                ", svRegDate=" + svRegDate +
                ", gwRegDate=" + gwRegDate +
                '}';
    }
}
