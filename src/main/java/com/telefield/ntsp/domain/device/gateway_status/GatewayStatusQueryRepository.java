package com.telefield.ntsp.domain.device.gateway_status;

import com.querydsl.core.BooleanBuilder;
import com.telefield.ntsp.define.DeviceType;
import com.telefield.ntsp.define.GatewayStatus;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Transactional(readOnly = true)
@Repository
public class GatewayStatusQueryRepository extends QuerydslRepositorySupport {
    public GatewayStatusQueryRepository() {
        super(GatewayStatusEntity.class);
    }

    public long countByGatewayStatus(GatewayStatus gatewayStatus) {
        QGatewayStatusEntity qGatewayStatus = QGatewayStatusEntity.gatewayStatusEntity;
        long count = from(qGatewayStatus).where(qGatewayStatus.gatewayStatus.eq(gatewayStatus)).fetchCount();
        return count;
    }

    public List<GatewayStatusEntity> findNotReceive() {
        QGatewayStatusEntity qGatewayStatus = QGatewayStatusEntity.gatewayStatusEntity;
        return from(qGatewayStatus)
                .where(qGatewayStatus.send_reg_date.before(LocalDateTime.now().minusDays(1))
                        .and(qGatewayStatus.gatewayStatus.eq(GatewayStatus.Normal))).fetch();
    }

    public List<GatewayStatusEntity> findByStatus(String gatewayStatus, String phone) {
        QGatewayStatusEntity qGatewayStatus = QGatewayStatusEntity.gatewayStatusEntity;

        BooleanBuilder builder = new BooleanBuilder();
        if (!phone.equals("")) {
            builder.and(qGatewayStatus.phone.eq(phone));
        }

        if (!gatewayStatus.equals("4")) {
            GatewayStatus gatewayStatus1 = intToGatewayStatus(gatewayStatus);
            builder.and(qGatewayStatus.gatewayStatus.eq(gatewayStatus1));
        }

        return from(qGatewayStatus)
                .where(builder).fetch();
    }

    private GatewayStatus intToGatewayStatus(String gatewayStatus) {
        switch (gatewayStatus) {
            case "0":
                return GatewayStatus.Normal;
            case "1":
                return GatewayStatus.Unplug;
            case "2":
                return GatewayStatus.Non_Receive;
            case "3":
                return GatewayStatus.AS;
        }
        return GatewayStatus.Normal;
    }
}
