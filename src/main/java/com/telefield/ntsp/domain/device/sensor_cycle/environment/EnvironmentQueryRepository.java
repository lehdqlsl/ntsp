package com.telefield.ntsp.domain.device.sensor_cycle.environment;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class EnvironmentQueryRepository extends QuerydslRepositorySupport {
    public EnvironmentQueryRepository() {
        super(EnvironmentEntity.class);
    }

    public List<EnvironmentEntity> getListByDate(String phone, LocalDateTime searchDate){
        QEnvironmentEntity qEnvironmentEntity = QEnvironmentEntity.environmentEntity;
        return from(qEnvironmentEntity)
                .where(qEnvironmentEntity.phone.eq(phone)
                .and(qEnvironmentEntity.svRegDate.after(searchDate))
                ).orderBy(qEnvironmentEntity.svRegDate.desc()).fetch();
    }

}
