package com.telefield.ntsp.domain.device.sensor_cycle.emergency_caller;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmergencyCallerRepository extends JpaRepository<EmergencyCallerEntity, Long> {

}
