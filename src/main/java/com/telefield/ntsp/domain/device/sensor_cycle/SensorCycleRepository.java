package com.telefield.ntsp.domain.device.sensor_cycle;

import com.telefield.ntsp.domain.device.gw_cycle.Cycle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SensorCycleRepository extends JpaRepository<SensorCycle, Long> {
    List<SensorCycle> findByCycleId(long id);
}
