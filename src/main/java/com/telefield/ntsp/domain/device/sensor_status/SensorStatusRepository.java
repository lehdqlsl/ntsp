package com.telefield.ntsp.domain.device.sensor_status;

import com.telefield.ntsp.domain.device.gateway_status.DeviceId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SensorStatusRepository extends JpaRepository<SensorStatusEntity, String> {
    List<SensorStatusEntity> findByPhone(String phone);
    void deleteByPhone(String phone);
}
