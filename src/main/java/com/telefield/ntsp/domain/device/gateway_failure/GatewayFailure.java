package com.telefield.ntsp.domain.device.gateway_failure;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telefield.ntsp.define.GatewayFailureStatus;
import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Setter
@Getter
@NoArgsConstructor
@Table(name = "SERVER_GATEWAY_FAILURE")
@ToString
@Entity
public class GatewayFailure {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long failureId;

    @Column(length = 25)
    private String phone;

    @Column(columnDefinition = "comment '1: 차단\r\r\n2:미수신\r\r\n3:배터리없음")
    @Enumerated(EnumType.ORDINAL)
    private GatewayFailureStatus gatewayFailureStatus;

    @Column(columnDefinition = "boolean default 0 comment '0: false\r\r\n1: true'")
    private boolean isAware;

    @Column(columnDefinition = "boolean default 0 comment '0: false\r\r\n1: true'")
    private boolean isClosed;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @CreationTimestamp
    private LocalDateTime regDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime closedDate;

    @Builder
    public GatewayFailure(String phone, GatewayFailureStatus gatewayFailureStatus, LocalDateTime regDate) {
        this.phone = phone;
        this.gatewayFailureStatus = gatewayFailureStatus;
        this.regDate = regDate;
    }

    public GatewayFailure(long failure_id) {
        this.failureId = failure_id;
    }
}
