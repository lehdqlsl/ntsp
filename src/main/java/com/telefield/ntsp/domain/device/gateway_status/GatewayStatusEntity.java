package com.telefield.ntsp.domain.device.gateway_status;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.domain.device.DeviceDto;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@NoArgsConstructor
@ToString
@Table(name = "SERVER_GATEWAY_STATUS", indexes = {@Index(columnList = "phone"),@Index(columnList = "gatewayStatus"),@Index(columnList = "send_reg_date")})
public class GatewayStatusEntity {
    @Id
    @Column(length = 25)
    private String phone;

    @Column(length = 25)
    private String mac_addr;

    @Column(columnDefinition = "comment '1: 구동중\r\r\n2:차단\r\r\n3:미수신\r\r\n4:AS'")
    @Enumerated(EnumType.ORDINAL)
    private GatewayStatus gatewayStatus;

    @Column(columnDefinition = "tinyint comment '0: power off\r\r\n1: power on'")
    private byte power;

    @Column(columnDefinition = "tinyint comment '0: bad\r\r\n1: good'")
    private byte network;

    private byte codiState;

    private short sensitivity;

    @Column(length = 10)
    private String app_ver;

    @Column(length = 5)
    private String fw_ver;

    private String fcmId;

    private short battery;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime send_reg_date;

    @Builder
    public GatewayStatusEntity(byte codiState, String phone, String mac_addr, byte power, byte network, short sensitivity, short battery, String app_ver, String fw_ver, LocalDateTime send_reg_date, String fcmId) {
        this.phone = phone;
        this.mac_addr = mac_addr;
        this.power = power;
        this.network = network;
        this.sensitivity = sensitivity;
        this.app_ver = app_ver;
        this.fw_ver = fw_ver;
        this.send_reg_date = send_reg_date;
        this.fcmId = fcmId;
        this.battery = battery;
        this.codiState = codiState;
    }

    @Builder
    public GatewayStatusEntity(byte codiState, String fcmId, GatewayStatus gatewayStatus, String phone, String mac_addr, byte power, byte network, short battery, short sensitivity, String app_ver, String fw_ver, LocalDateTime send_reg_date) {
        this.phone = phone;
        this.mac_addr = mac_addr;
        this.power = power;
        this.network = network;
        this.sensitivity = sensitivity;
        this.app_ver = app_ver;
        this.fw_ver = fw_ver;
        this.send_reg_date = send_reg_date;
        this.gatewayStatus = gatewayStatus;
        this.battery = battery;
        this.codiState = codiState;
    }

    public DeviceDto toDeviceDto(){
        return DeviceDto.builder()
        .app_ver(app_ver)
        .fw_ver(fw_ver)
        .devType((byte) 0)
        .gatewayStatus(gatewayStatus)
        .macAddr(mac_addr)
        .network(network)
        .phone(phone)
        .power(power)
        .send_reg_date(send_reg_date)
        .sensitivity(sensitivity)
        .build();
    }
}
