package com.telefield.ntsp.domain.device.sensor_cycle.emergency_caller;

import com.telefield.ntsp.domain.device.sensor_cycle.CommonSensorInfo;

import javax.persistence.*;

@Entity
@Table(name = "SENSOR_EMERCALLER_HISTORY")
public class EmergencyCallerEntity extends CommonSensorInfo {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

}
