package com.telefield.ntsp.domain.device.gw_cycle;

import com.telefield.ntsp.define.GatewayStatus;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.device.gateway_status.QGatewayStatusEntity;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Transactional(readOnly = true)
@Repository
public class CycleQueryRepository extends QuerydslRepositorySupport {
    public CycleQueryRepository() {
        super(Cycle.class);
    }

    public List<Cycle> getCycleList(String phone, LocalDate startDate, LocalDate endDate){
        QCycle qCycle = QCycle.cycle;
        return from(qCycle)
                .where(qCycle.phone.eq(phone)
                        .and(qCycle.send_reg_date.between(startDate.atStartOfDay(),endDate.atTime(23,59))))
                .orderBy(qCycle.send_reg_date.desc())
                .fetch();
    }

}
