package com.telefield.ntsp.domain.statistics;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@Table(name = "SERVER_STATISTICS_EMERGENCY")
@ToString
@Entity
public class StatisticsEmergency {

    @Id
    private long id;

    private int fire;

    private int _119;

    private int noActivity;

    private int emergency;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate regDate;

    @Builder
    public StatisticsEmergency(int fire, int _119, int noActivity, int emergency, LocalDate regDate){
        this.fire = fire;
        this._119 = _119;
        this.noActivity = noActivity;
        this.emergency = emergency;
        this.regDate = regDate;
    }

}
