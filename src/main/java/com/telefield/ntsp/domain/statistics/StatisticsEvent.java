package com.telefield.ntsp.domain.statistics;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@Table(name = "SERVER_STATISTICS_EVENT")
@ToString
@Entity
public class StatisticsEvent {

    @Id
    private long id;

    private int eventType;

    private int count;

    private String phone;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate regDate;

}
