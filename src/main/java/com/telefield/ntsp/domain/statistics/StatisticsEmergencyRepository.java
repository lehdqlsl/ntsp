package com.telefield.ntsp.domain.statistics;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface StatisticsEmergencyRepository extends JpaRepository<StatisticsEmergency, Long> {
    List<StatisticsEmergency> findAllByOrderByRegDateDesc();
    List<StatisticsEmergency> findByRegDateBetweenOrderByRegDateDesc(LocalDate start, LocalDate end);
}
