package com.telefield.ntsp.domain.statistics;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StatisticsEventRepository extends JpaRepository<StatisticsEvent, Long> {

}
