package com.telefield.ntsp.domain.recipient.info;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.telefield.ntsp.domain.recipient.status.QRecipientStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class RecipientInfoQueryRepository extends QuerydslRepositorySupport {

    @Autowired
    EntityManager entityManager            ;

    public RecipientInfoQueryRepository() {
        super(RecipientInfo.class);
    }

    public List<Tuple> getSearchRecipient(String phone, String name, byte status, String address) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);

        QRecipientInfo qRecipientInfo = QRecipientInfo.recipientInfo;
        QRecipientStatus qRecipientStatus = QRecipientStatus.recipientStatus;

        BooleanBuilder builder = new BooleanBuilder();

        if (!phone.equals("")) {
            builder.and(qRecipientInfo.phone.eq(phone));
        }

        if (!name.equals("")) {
            builder.and(qRecipientInfo.name.eq(name));
        }

        if (status != -1) {
            builder.and(qRecipientStatus.current_status.eq((byte) status));
        }

        if (!address.equals("")) {
            builder.and(qRecipientInfo.address1.like("%"+address + "%"));
        }

        return queryFactory.select(qRecipientStatus,qRecipientInfo).from(qRecipientInfo).join(qRecipientStatus).on(qRecipientInfo.phone.eq(qRecipientStatus.phone))
                .where(builder).fetch();
    }

}
