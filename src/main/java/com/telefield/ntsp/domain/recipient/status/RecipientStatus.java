package com.telefield.ntsp.domain.recipient.status;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name="SERVER_RECIPIENT_STATUS")
@ToString
public class RecipientStatus {
    @Id
    @Column(length = 25)
    private String phone;

    @Column(length = 25)
    private String mac_addr;

    @Column(columnDefinition = "tinyint comment 'Current Status\r\r\n" +
            "0: 이벤트 없음\n\r\n" +
            "7: 활동 미감지\n\r\n" +
            "8: 활동\n\r\n" +
            "23: 귀가\n\r\n" +
            "24: 외출'")
    private byte current_status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime send_reg_date;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime server_reg_date;

    @Builder
    public RecipientStatus(String phone, String mac_addr, byte current_status, LocalDateTime send_reg_date) {
        this.phone = phone;
        this.mac_addr = mac_addr;
        this.current_status = current_status;
        this.send_reg_date = send_reg_date;
    }
}
