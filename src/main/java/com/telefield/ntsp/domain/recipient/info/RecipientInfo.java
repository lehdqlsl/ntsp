package com.telefield.ntsp.domain.recipient.info;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.recipient.status.RecipientStatus;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@Table(name = "SERVER_RECIPIENT_INFO")
@ToString
@Entity
public class RecipientInfo {

    @Id
    private String phone;

    private String address1;

    private String address2;

    private LocalDate birth;

    private String mobile;

    private String name;

    private String imageFile;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;

    @Builder
    public RecipientInfo(String address1, String address2, LocalDate birth, String mobile, String name, String phone, String imageFile) {
        this.address1 = address1;
        this.address2 = address2;
        this.birth = birth;
        this.mobile = mobile;
        this.name = name;
        this.phone = phone;
        this.imageFile = imageFile;
    }

    public RecipientInfoDto toEntity(RecipientInfo recipientInfo, RecipientStatus recipientStatus, GatewayStatusEntity gatewayStatusEntity){
        return RecipientInfoDto.builder()
                .address1(recipientInfo.getAddress1())
                .address2(recipientInfo.getAddress2())
                .birth(recipientInfo.getBirth())
                .mobile(recipientInfo.getMobile())
                .name(recipientInfo.getName())
                .phone(recipientInfo.getPhone())
                .current_status(recipientStatus.getCurrent_status())
                .imageFile(recipientInfo.getImageFile())
                .regDate(recipientInfo.getRegDate())
                .gateway_status((byte) gatewayStatusEntity.getGatewayStatus().ordinal())
                .build();
    }
}
