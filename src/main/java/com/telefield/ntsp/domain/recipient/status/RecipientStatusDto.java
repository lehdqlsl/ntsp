package com.telefield.ntsp.domain.recipient.status;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class RecipientStatusDto {

    private String phone;

    private String mac_addr;

    private byte current_status;

    private LocalDateTime send_reg_date;

    public RecipientStatus toEntity() {
        return RecipientStatus.builder()
                .mac_addr(mac_addr)
                .phone(phone)
                .send_reg_date(send_reg_date)
                .current_status(current_status)
                .build();
    }
}
