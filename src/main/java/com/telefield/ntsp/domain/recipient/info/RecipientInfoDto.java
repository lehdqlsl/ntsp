package com.telefield.ntsp.domain.recipient.info;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.domain.device.gateway_status.GatewayStatusEntity;
import com.telefield.ntsp.domain.recipient.status.RecipientStatus;
import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class RecipientInfoDto {

    private String address1;

    private String address2;

    private LocalDate birth;

    private String mobile;

    private String name;

    private String phone;

    private byte current_status;

    private byte gateway_status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;

    private String imageFile;

    @Builder
    public RecipientInfoDto(String address1, String address2, LocalDate birth, String mobile, String name, String phone, byte current_status, LocalDateTime regDate, String imageFile, byte gateway_status) {
        this.address1 = address1;
        this.address2 = address2;
        this.birth = birth;
        this.mobile = mobile;
        this.name = name;
        this.phone = phone;
        this.current_status = current_status;
        this.regDate = regDate;
        this.imageFile = imageFile;
        this.gateway_status = gateway_status;
    }

    public RecipientInfo toEntity() {
        return RecipientInfo.builder()
                .phone(phone)
                .address1(address1)
                .address2(address2)
                .birth(birth)
                .mobile(mobile)
                .name(name)
                .build();
    }
}
