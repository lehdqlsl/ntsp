package com.telefield.ntsp.domain.recipient.status;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipientStatusRepository extends JpaRepository<RecipientStatus, String>, CustomRecipientStatusRepository {

}
