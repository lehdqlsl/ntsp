package com.telefield.ntsp.domain.recipient.status;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

interface CustomRecipientStatusRepository {

}

public class RecipientStatusFragment extends QuerydslRepositorySupport implements CustomRecipientStatusRepository {
    public RecipientStatusFragment() {
        super(RecipientStatus.class);
    }
}
