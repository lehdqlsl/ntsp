package com.telefield.ntsp.domain.recipient.qrheck;

import com.telefield.ntsp.domain.recipient.info.RecipientInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QRCheckRepository extends JpaRepository<QRCheck, Long> {
    List<QRCheck> findAllByOrderByIdDesc();
}
