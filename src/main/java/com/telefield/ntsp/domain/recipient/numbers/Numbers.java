package com.telefield.ntsp.domain.recipient.numbers;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@ToString
@Table(name = "SERVER_RECIPIENT_NUMBERS", indexes =  {@Index(columnList = "phone")})
public class Numbers {
    @Id
    @Column(length = 25)
    private String phone;

    @Column(columnDefinition = "varchar(25) default ''")
    private String _119;

    @Column(columnDefinition = "varchar(25) default ''")
    private String helper;

    @Column(columnDefinition = "varchar(25) default ''")
    private String center;

    @Column(columnDefinition = "varchar(25) default ''")
    private String care1;

    @Column(columnDefinition = "varchar(25) default ''")
    private String care2;

    @Column(columnDefinition = "varchar(25) default ''")
    private String etc1;

    @Column(columnDefinition = "varchar(25) default ''")
    private String etc2;

    @Column(columnDefinition = "varchar(25) default ''")
    private String etc3;

    @Column(columnDefinition = "varchar(25) default ''")
    private String etc4;

    @Column(columnDefinition = "varchar(25) default ''")
    private String etc5;

    @Column(columnDefinition = "varchar(25) default ''")
    private String etc6;

    @Column(columnDefinition = "varchar(25) default ''")
    private String etc7;

    @Column(columnDefinition = "varchar(25) default ''")
    private String etc8;

    @Column(columnDefinition = "varchar(25) default ''")
    private String etc9;

    @Column(columnDefinition = "varchar(25) default ''")
    private String etc10;

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void set_119(String _119) {
        this._119 = _119;
    }

    public void setHelper(String helper) {
        this.helper = helper;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public void setCare1(String care1) {
        this.care1 = care1;
    }

    public void setCare2(String care2) {
        this.care2 = care2;
    }

    public void setEtc1(String etc1) {
        this.etc1 = etc1;
    }

    public void setEtc2(String etc2) {
        this.etc2 = etc2;
    }

    public void setEtc3(String etc3) {
        this.etc3 = etc3;
    }

    public void setEtc4(String etc4) {
        this.etc4 = etc4;
    }

    public void setEtc5(String etc5) {
        this.etc5 = etc5;
    }

    public void setEtc6(String etc6) {
        this.etc6 = etc6;
    }

    public void setEtc7(String etc7) {
        this.etc7 = etc7;
    }

    public void setEtc8(String etc8) {
        this.etc8 = etc8;
    }

    public void setEtc9(String etc9) {
        this.etc9 = etc9;
    }

    public void setEtc10(String etc10) {
        if(etc10 == null)
            etc10="";
        this.etc10 = etc10;
    }
}
