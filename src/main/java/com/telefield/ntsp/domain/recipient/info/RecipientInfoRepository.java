package com.telefield.ntsp.domain.recipient.info;

import com.telefield.ntsp.domain.event.emergency.Emergency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecipientInfoRepository extends JpaRepository<RecipientInfo, String> {
    List<RecipientInfo> findByPhoneEndingWith(String phone);
    List<RecipientInfo> findByPhone(String phone);
    List<RecipientInfo> findByName(String name);
}
