package com.telefield.ntsp.domain.recipient.qrheck;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@Table(name = "SERVER_RECIPIENT_QRCHECK")
@ToString
@Entity
public class QRCheck {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String phone;

    private String mobile;

    private String userId;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;

    @Builder
    public QRCheck(String phone, String userId) {
        this.phone = phone;
        this.userId = userId;
    }
}
