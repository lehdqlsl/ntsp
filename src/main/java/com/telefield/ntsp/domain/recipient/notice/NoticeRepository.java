package com.telefield.ntsp.domain.recipient.notice;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NoticeRepository extends JpaRepository<Notice, Integer> {

	List<Notice> findByType(int type);
	List<Notice> findAllByOrderBySeqDesc();
	List<Notice> findByTypeOrderBySeqDesc(int type);
	Optional<Notice> findTopByOrderBySeqDesc();
}
