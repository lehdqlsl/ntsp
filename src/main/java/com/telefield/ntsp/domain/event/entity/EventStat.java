package com.telefield.ntsp.domain.event.entity;

import lombok.ToString;

public interface EventStat {
    int getCount();
    String getDate();
}
