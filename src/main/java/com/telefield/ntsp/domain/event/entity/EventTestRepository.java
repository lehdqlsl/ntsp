package com.telefield.ntsp.domain.event.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventTestRepository extends JpaRepository<EventTest, Long> {

}
