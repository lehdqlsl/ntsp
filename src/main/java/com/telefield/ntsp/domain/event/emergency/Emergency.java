package com.telefield.ntsp.domain.event.emergency;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.domain.event.entity.Event;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@Table(name="SERVER_RECIPIENT_EMERGENCY")
@Entity
@ToString
public class Emergency {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long emergencyId;

    @ManyToOne
    @JoinColumn(name="eventId")
    private Event eventId;

    @Column(columnDefinition = "boolean default 0 comment '0: false\r\r\n1: true'")
    private boolean isAware;

    @Column(columnDefinition = "boolean default 0 comment '0: false\r\r\n1: true'")
    private boolean isClosed;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime closedDate;

    @Builder
    public Emergency(long emergencyId, Event eventId) {
        this.eventId = eventId;
        this.emergencyId = emergencyId;
    }
}
