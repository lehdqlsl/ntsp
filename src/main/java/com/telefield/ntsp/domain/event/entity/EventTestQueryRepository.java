package com.telefield.ntsp.domain.event.entity;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Transactional(readOnly = true)
@Repository
public class EventTestQueryRepository extends QuerydslRepositorySupport {
    public EventTestQueryRepository() {
        super(EventTest.class);
    }

    public List<EventTest> findByPhoneLimit(String phone, int offset){
        QEventTest qEventTest = QEventTest.eventTest;
        return from(qEventTest)
                .where(qEventTest.phone.eq(phone))
                .orderBy(qEventTest.eventId.desc())
                .limit(20)
                .offset(offset)
                .fetch();
    }
}
