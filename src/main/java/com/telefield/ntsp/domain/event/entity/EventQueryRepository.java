package com.telefield.ntsp.domain.event.entity;


import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Transactional(readOnly = true)
@Repository
public class EventQueryRepository extends QuerydslRepositorySupport {
    public EventQueryRepository() {
        super(Event.class);
    }

    public List<Event> findByPhoneLimit(String phone, int offset) {
        QEvent qEvent = QEvent.event;
        return from(qEvent)
                .where(qEvent.phone.eq(phone))
                .orderBy(qEvent.eventId.desc())
                .limit(20)
                .offset(offset)
                .fetch();
    }

    public List<Event> GetOpenEvent() {
        QEvent qEvent = QEvent.event;
        return from(qEvent)
                .where(qEvent.eventType.eq((byte) 100))
                .where(qEvent.svRegDate.after(LocalDate.now().atStartOfDay()))
                .orderBy(qEvent.eventId.desc())
                .fetch();
    }

    public Event getLastEmergencyEvent(String phone) {
        QEvent qEvent = QEvent.event;
        return from(qEvent)
                .where(qEvent.phone.eq(phone)
                        .and(qEvent.eventType.in(11, 15))
                )
                .orderBy(qEvent.gwRegDate.desc())
                .limit(1)
                .fetchOne();
    }

    public QueryResults<Tuple> countGroupByEventType(LocalDate start, LocalDate end) {
        QEvent qEvent = QEvent.event;
        return from(qEvent).where(qEvent.gwRegDate.between(start.atStartOfDay(), end.atStartOfDay())).groupBy(qEvent.eventType)
                .select(qEvent.eventType, qEvent.count()).fetchResults();
    }
}
