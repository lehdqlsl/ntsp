package com.telefield.ntsp.domain.event.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.domain.event.emergency.Emergency;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.json.JSONObject;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashMap;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "GW_EVENT_HISTORY_TESTMODE")
@ToString
public class EventTest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long eventId;

    @Column(length = 25)
    private String phone;

    @Column(length = 25)
    private String mac_addr;

    private byte devType;

    private byte eventType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime gwRegDate;

    @CreationTimestamp
    private LocalDateTime svRegDate;

    private String data;

    @Builder
    public EventTest(long eventId, String phone, String mac_addr, byte devType, byte eventType, LocalDateTime send_reg_date, HashMap<String, Object> data) {
        this.eventId = eventId;
        this.phone = phone;
        this.mac_addr = mac_addr;
        this.devType = devType;
        this.gwRegDate = send_reg_date;
        this.eventType = eventType;
        JSONObject json = new JSONObject(data);
        this.data = json.toString();
    }

    public EventDto toEventDto(HashMap<String, Object> data) {
        return EventDto.builder()
                .eventId(eventId)
                .data(data)
                .devType(devType)
                .eventType(eventType)
                .macAddr(mac_addr)
                .phone(phone)
                .regDate(gwRegDate)
                .build();
    }
}
