package com.telefield.ntsp.domain.event.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.domain.device.sensor_cycle.activity.ActivityEntity;
import com.telefield.ntsp.domain.device.sensor_cycle.radar.RadarEntity;
import lombok.*;


import java.time.LocalDateTime;
import java.util.HashMap;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class EventDto {
    private long eventId;

    private String phone;

    private String macAddr;

    private byte devType;

    private byte eventType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;

    private HashMap<String, Object> data;

    public Event toEntity() {
        return Event.builder()
                .devType(devType)
                .mac_addr(macAddr)
                .phone(phone)
                .send_reg_date(regDate)
                .eventType(eventType)
                .data(data)
                .build();
    }

    public EventTest toTestEntity() {
        return EventTest.builder()
                .devType(devType)
                .mac_addr(macAddr)
                .phone(phone)
                .send_reg_date(regDate)
                .eventType(eventType)
                .data(data)
                .build();
    }

    public ActivityEntity toActivity(String phone, LocalDateTime regDate){
        return ActivityEntity.builder()
                .location((String) data.getOrDefault("location", ""))
                .count((int) data.getOrDefault("count", 0))
                .phone(phone)
                .gwRegDate(regDate)
                .build();
    }

    public RadarEntity toRadar(String phone, LocalDateTime regDate){
        return RadarEntity.builder()
                .location((String) data.getOrDefault("location", ""))
                .count((int) data.getOrDefault("count", 0))
                .heartRate((int) data.getOrDefault("heartRate", 0))
                .breath((int) data.getOrDefault("breath", 0))
                .x((Integer) data.getOrDefault("x", 0))
                .y((Integer) data.getOrDefault("y", 0))
                .gwRegDate(regDate)
                .phone(phone).build();
    }

    @Builder
    public EventDto(long eventId, String phone, String macAddr, byte devType, byte eventType, LocalDateTime regDate, HashMap<String, Object> data) {
        this.phone = phone;
        this.macAddr = macAddr;
        this.devType = devType;
        this.eventType = eventType;
        this.regDate = regDate;
        this.data = data;
        this.eventId = eventId;
    }
}
