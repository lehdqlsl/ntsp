package com.telefield.ntsp.domain.event.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    List<Event> findByPhoneOrderByGwRegDateDesc(String s);

    @Query(value = "select count(eventType) as count, date(gwRegDate) as date from GW_EVENT_HISTORY where phone=?1 and eventType=?2 and gwRegDate > ?3 GROUP BY date(gwRegDate)  order  by gwRegDate asc", nativeQuery = true)
    List<EventStat> findEventStatByPhoneAndDate(String phone, int eventType, LocalDateTime date);
}
