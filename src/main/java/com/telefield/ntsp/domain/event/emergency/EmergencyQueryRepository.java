package com.telefield.ntsp.domain.event.emergency;

import com.querydsl.core.BooleanBuilder;
import com.telefield.ntsp.define.EventType;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_failure.QGatewayFailure;
import com.telefield.ntsp.domain.search.HistorySearchDto;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public class EmergencyQueryRepository extends QuerydslRepositorySupport {
    public EmergencyQueryRepository() {
        super(Emergency.class);
    }

    @Transactional(readOnly = true)
    public List<Emergency> findTodayEmergencyByClose(boolean isClosed){
        QEmergency qEmergency = QEmergency.emergency;
        List<Emergency> list = from(qEmergency)
                .where(qEmergency.isClosed.eq(isClosed)
                ).orderBy(qEmergency.eventId.gwRegDate.desc())
                .fetch();
        return list;
    }

    @Transactional(readOnly = true)
    public long countTodayEmergencyByEventType(int eventType){
        QEmergency qEmergency = QEmergency.emergency;
        return from(qEmergency)
                .where(qEmergency.isClosed.eq(false)
                        .and(qEmergency.eventId.eventType.eq((byte)eventType))
                ).fetchCount();
    }

    @Transactional(readOnly = true)
    public long countTodayEmergencyByEventTypeAndDate(int eventType, LocalDateTime start, LocalDateTime end){
        QEmergency qEmergency = QEmergency.emergency;
        return from(qEmergency)
                .where(qEmergency.eventId.eventType.eq((byte)eventType)
                        .and(qEmergency.eventId.gwRegDate.between(start,end))
                ).fetchCount();
    }

    @Transactional(readOnly = true)
    public List<Emergency> findTodayNoActivityByClose(){
        QEmergency qEmergency = QEmergency.emergency;
        List<Emergency> list = from(qEmergency)
                .where(qEmergency.isClosed.eq(false)
                        .and(qEmergency.eventId.eventType.in(EventType.NO_ACTIVITY))
                ).fetch();
        return list;
    }

    @Transactional(readOnly = true)
    public List<Emergency> getEmergencyListByPhone(String phone) {
        QEmergency qEmergency = QEmergency.emergency;
        List<Emergency> list = from(qEmergency)
                .where(qEmergency.eventId.phone.eq(phone))
                .orderBy(qEmergency.eventId.gwRegDate.desc())
                .fetch();
        return list;
    }

    @Transactional(readOnly = true)
    public List<Emergency> findNoActivityByClose(String phone){
        QEmergency qEmergency = QEmergency.emergency;

        return from(qEmergency)
                .where(qEmergency.eventId.phone.eq(phone)
                        .and(qEmergency.eventId.eventType.in(EventType.NO_ACTIVITY)
                                .and(qEmergency.isClosed.eq(false))
                        )
                ).fetch();
    }

    @Transactional(readOnly = true)
    public Emergency findLastEmerCall(String phone){
        QEmergency qEmergency = QEmergency.emergency;
        return from(qEmergency)
                .where(qEmergency.eventId.phone.eq(phone)
                        .and(qEmergency.eventId.eventType.in(EventType.EMERGENCY_CALL)
                                .and(qEmergency.isClosed.eq(false))
                        )
                ).orderBy(qEmergency.emergencyId.desc()).limit(1).fetchOne();
    }

    @Transactional(readOnly = true)
    public Emergency findLastFire(String phone) {
        QEmergency qEmergency = QEmergency.emergency;
        return from(qEmergency)
                .where(qEmergency.eventId.phone.eq(phone)
                        .and(qEmergency.eventId.eventType.in(EventType.FIRE_START)
                                .and(qEmergency.isClosed.eq(false))
                        )
                ).orderBy(qEmergency.emergencyId.desc()).limit(1).fetchOne();
    }


    public List<Emergency> emergencySearch(HistorySearchDto historySearchDto) {
        LocalDate startDate = LocalDate.parse(historySearchDto.getStartDate());
        LocalDate endDate = LocalDate.parse(historySearchDto.getEndDate());

        QEmergency qEmergency = QEmergency.emergency;

        BooleanBuilder builder = new BooleanBuilder();
        if(!historySearchDto.getPhone().equals("")){
            builder.and(qEmergency.eventId.phone.eq(historySearchDto.getPhone()));
        }
        builder.and(qEmergency.eventId.gwRegDate.between(startDate.atStartOfDay(), endDate.atTime(23,59)));
        if(!historySearchDto.getCloseType().equals("0")){
            builder.and(qEmergency.isClosed.eq(historySearchDto.isClosed()));
        }
        if(historySearchDto.getEventValue()!=0){
            builder.and(qEmergency.eventId.eventType.eq(historySearchDto.getEventValue()));
        }

        return from(qEmergency)
                .where(builder).orderBy(qEmergency.eventId.gwRegDate.desc())
                .fetch();
    }

}
