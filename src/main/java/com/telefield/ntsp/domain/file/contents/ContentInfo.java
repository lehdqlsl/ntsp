package com.telefield.ntsp.domain.file.contents;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@Table(name = "SERVER_FILE_CONTENT_INFO")
@ToString
@Entity
public class ContentInfo {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long seq;

    private String type;

    private String category;

    private String fileName;

}
