package com.telefield.ntsp.domain.file.contents;

import com.querydsl.core.BooleanBuilder;
import com.telefield.ntsp.domain.recipient.info.QRecipientInfo;
import com.telefield.ntsp.domain.recipient.info.RecipientInfo;
import com.telefield.ntsp.domain.recipient.status.QRecipientStatus;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContentInfoQueryRepository extends QuerydslRepositorySupport {
    public ContentInfoQueryRepository() {
        super(ContentInfo.class);
    }

    public List<RecipientInfo> getSearchRecipient(String phone, String name, byte status, String address) {
        QRecipientInfo qRecipientInfo = QRecipientInfo.recipientInfo;
        QRecipientStatus qRecipientStatus = QRecipientStatus.recipientStatus;

        BooleanBuilder builder = new BooleanBuilder();

        if (!phone.equals("")) {
            builder.and(qRecipientInfo.phone.eq(phone));
        }

        if (!name.equals("")) {
            builder.and(qRecipientInfo.name.eq(name));
        }

        if (status != -1) {
            builder.and(qRecipientStatus.current_status.eq((byte) status));
        }

        if (!address.equals("")) {
            builder.and(qRecipientInfo.address1.like(address + "%"));
        }

        return from(qRecipientInfo).join(qRecipientStatus).on(qRecipientInfo.phone.eq(qRecipientStatus.phone))
                .where(builder).fetch();
    }
}
