package com.telefield.ntsp.domain.file.contents;

import com.telefield.ntsp.domain.recipient.info.RecipientInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentInfoRepository extends JpaRepository<ContentInfo, Long> {

}
