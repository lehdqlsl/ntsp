package com.telefield.ntsp.domain.admin.emergency_action;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.domain.event.emergency.Emergency;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@Table(name="SERVER_EMERGENCY_ACTION")
@Entity
@ToString
public class EmergencyAction {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long emer_action_id;

    @Column(length = 25)
    private String user;

    @Column(length = 25)
    private String title;

    @Column(length = 100)
    private String text;

    @ManyToOne
    @JoinColumn(name="emergencyId")
    private Emergency emergencyId;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime svRegDate;

    @Builder
    public EmergencyAction(String title, String text, Emergency emergencyId, String user) {
        this.title = title;
        this.text = text;
        this.emergencyId = emergencyId;
        this.user = user;
    }
}
