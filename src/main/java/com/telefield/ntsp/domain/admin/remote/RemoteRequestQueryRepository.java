package com.telefield.ntsp.domain.admin.remote;


import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;


public class RemoteRequestQueryRepository extends QuerydslRepositorySupport  {
    public RemoteRequestQueryRepository() {
        super(RemoteRequest.class);
    }

}
