package com.telefield.ntsp.domain.admin.sensor_failure_action;


import com.telefield.ntsp.domain.admin.gateway_failure_action.GatewayFailureAction;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;


public class SensorFailureActionQueryRepository extends QuerydslRepositorySupport  {
    public SensorFailureActionQueryRepository() {
        super(SensorFailureAction.class);
    }

}
