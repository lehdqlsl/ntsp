package com.telefield.ntsp.domain.admin.fcm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FcmKeyRepository extends JpaRepository<FcmKey, String> {

}
