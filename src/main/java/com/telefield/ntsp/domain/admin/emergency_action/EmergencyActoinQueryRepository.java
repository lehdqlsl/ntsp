package com.telefield.ntsp.domain.admin.emergency_action;

import com.telefield.ntsp.define.GatewayFailureStatus;
import com.telefield.ntsp.domain.admin.sensor_failure_action.SensorFailureAction;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.gateway_failure.QGatewayFailure;
import com.telefield.ntsp.domain.event.emergency.Emergency;
import com.telefield.ntsp.domain.event.emergency.QEmergency;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;

public class EmergencyActoinQueryRepository extends QuerydslRepositorySupport  {
    public EmergencyActoinQueryRepository() {
        super(Emergency.class);
    }
}