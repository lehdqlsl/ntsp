package com.telefield.ntsp.domain.admin.account;

import com.telefield.ntsp.define.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
@Table(name = "SERVER_ADMIN_USER")
@Entity
@ToString
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userNum;

    private String userId;

    private String userName;

    private String password;

    private String refreshToken;

    @Enumerated(EnumType.STRING)
    private Role role;

    public UserDto toUserDto() {
        return UserDto.builder()
                .userId(userId)
                .userName(userName)
                .role(role)
                .build();
    }
}
