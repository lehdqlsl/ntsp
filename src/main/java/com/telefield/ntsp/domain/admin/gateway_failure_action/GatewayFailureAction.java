package com.telefield.ntsp.domain.admin.gateway_failure_action;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name="SERVER_GATEWAY_FAILURE_ACTION")
public class GatewayFailureAction {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long action_id;

    @Column(length = 25)
    private String user;

    @Column(length = 25)
    private String title;

    @Column(length = 100)
    private String text;

    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="failureId")
    private GatewayFailure failureId;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime svRegDate;

    @Builder
    public GatewayFailureAction(String title, String text, GatewayFailure failureId, String user) {
        this.title = title;
        this.text = text;
        this.user = user;
        this.failureId = failureId;
    }
}
