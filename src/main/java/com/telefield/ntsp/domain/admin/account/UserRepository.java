package com.telefield.ntsp.domain.admin.account;

import com.telefield.ntsp.domain.admin.emergency_action.EmergencyAction;
import com.telefield.ntsp.domain.event.emergency.Emergency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUserIdAndPassword(String userId, String password);
    Optional<User> findByUserId(String userId);
}
