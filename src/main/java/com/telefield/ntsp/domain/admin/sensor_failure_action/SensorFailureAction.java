package com.telefield.ntsp.domain.admin.sensor_failure_action;


import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.device.sensor_failure.SensorFailure;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name="SERVER_SENSOR_FAILURE_ACTION")
public class SensorFailureAction {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long action_id;

    @Column(length = 25)
    private String title;

    @Column(length = 100)
    private String text;

    @ManyToOne
    @JoinColumn(name="failure_id")
    private SensorFailure failure_id;

    @CreationTimestamp
    private LocalDateTime server_reg_date;

    @Builder
    public SensorFailureAction(String title, String text, SensorFailure failure_id, LocalDateTime server_reg_date) {
        this.title = title;
        this.text = text;
        this.failure_id = failure_id;
        this.server_reg_date = server_reg_date;
    }
}
