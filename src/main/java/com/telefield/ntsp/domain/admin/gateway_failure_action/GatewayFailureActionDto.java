package com.telefield.ntsp.domain.admin.gateway_failure_action;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class GatewayFailureActionDto {

    private String contents;

    @JsonProperty("isClosed")
    private boolean isClosed;

    @JsonProperty("isAware")
    private boolean isAware;

    private GatewayFailure failureId;

    private String user;

    public GatewayFailureAction toEntity(GatewayFailure gatewayFailure) {
        return GatewayFailureAction.builder()
                .text(contents)
                .title("미처리")
                .failureId(gatewayFailure)
                .user(user)
                .build();
    }
}
