package com.telefield.ntsp.domain.admin.account;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefield.ntsp.define.Role;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.json.JSONObject;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Builder
@ToString
@Setter
@Getter
public class UserDto {
    @NotNull
    private String userId;

    private String userName;

    @NotNull
    private String refreshToken;

    @Enumerated(EnumType.STRING)
    private Role role;

    public String toJSON(){
        ObjectMapper mapper = new ObjectMapper();
        String str = "";
        try {
            str = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return str;
    }
}
