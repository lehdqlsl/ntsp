package com.telefield.ntsp.domain.admin.emergency_action;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.telefield.ntsp.domain.admin.gateway_failure_action.GatewayFailureAction;
import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import com.telefield.ntsp.domain.event.emergency.Emergency;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class EmergencyActionDto {

    private String contents;

    @JsonProperty("isClosed")
    private boolean isClosed;

    private long emergencyId;

    private String user;

    public EmergencyAction toEntity(Emergency emergencyId) {
        return EmergencyAction.builder()
                .emergencyId(emergencyId)
                .user(user)
                .text(contents)
                .build();
    }
}
