package com.telefield.ntsp.domain.admin.emergency_action;

import com.telefield.ntsp.domain.event.emergency.Emergency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmergencyActionRepository extends JpaRepository<EmergencyAction, Long> {
    List<EmergencyAction> findByEmergencyIdOrderBySvRegDateDesc(Emergency emergency);

}
