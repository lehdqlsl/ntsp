package com.telefield.ntsp.domain.admin.fcm;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.define.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@Table(name = "SERVER_ADMIN_FCMKEY")
@Entity
@ToString
public class FcmKey {
    @Id
    private String mobile;

    private String fcmKey;

    @UpdateTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;
}
