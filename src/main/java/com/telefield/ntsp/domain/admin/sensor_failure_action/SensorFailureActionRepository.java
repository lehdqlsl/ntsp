package com.telefield.ntsp.domain.admin.sensor_failure_action;

import com.telefield.ntsp.domain.admin.gateway_failure_action.GatewayFailureAction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SensorFailureActionRepository extends JpaRepository<SensorFailureAction, Long>{

}
