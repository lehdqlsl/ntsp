package com.telefield.ntsp.domain.admin.remote;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RemoteRequestRepository extends JpaRepository<RemoteRequest, Long>{

    List<RemoteRequest> findByPhone(String phone);
}
