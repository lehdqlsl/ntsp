package com.telefield.ntsp.domain.admin.sensor_failure_action;

import com.telefield.ntsp.domain.device.sensor_failure.SensorFailure;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class SensorFailureActionDto {

    private String title;

    private String text;

    private SensorFailure failure_id;
    public SensorFailureAction toEntity() {
        return SensorFailureAction.builder()
                .title(title)
                .text(text)
                .failure_id(failure_id)
                .build();
    }
}
