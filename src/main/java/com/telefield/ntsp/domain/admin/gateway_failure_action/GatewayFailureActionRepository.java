package com.telefield.ntsp.domain.admin.gateway_failure_action;

import com.telefield.ntsp.domain.device.gateway_failure.GatewayFailure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GatewayFailureActionRepository extends JpaRepository<GatewayFailureAction, Long>{
    List<GatewayFailureAction> findByFailureIdOrderBySvRegDateDesc(GatewayFailure failure_id);
}
