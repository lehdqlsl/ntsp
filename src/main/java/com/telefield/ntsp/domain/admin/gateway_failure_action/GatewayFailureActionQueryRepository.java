package com.telefield.ntsp.domain.admin.gateway_failure_action;


import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;



public class GatewayFailureActionQueryRepository extends QuerydslRepositorySupport  {
    public GatewayFailureActionQueryRepository() {
        super(GatewayFailureAction.class);
    }

}
