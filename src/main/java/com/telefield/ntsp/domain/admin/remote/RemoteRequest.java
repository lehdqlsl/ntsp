package com.telefield.ntsp.domain.admin.remote;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.telefield.ntsp.define.RemoteType;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name="SERVER_GATEWAY_REMOTE_REQUEST")
public class RemoteRequest {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long seq;

    @Column(length = 25)
    private String userName;

    @Column(length = 25)
    private String phone;

    @Column(length = 25)
    @Enumerated(EnumType.STRING)
    private RemoteType remoteType;

    @Column(length = 100)
    private String content;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime svRegDate;

    boolean isReceived;

    @Builder
    public RemoteRequest(String phone, String userName, RemoteType remoteType, String content, boolean isReceived) {
        this.phone = phone;
        this.userName = userName;
        this.remoteType = remoteType;
        this.content = content;
        this.isReceived = isReceived;
    }
}
