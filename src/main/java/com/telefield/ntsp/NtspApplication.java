package com.telefield.ntsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class NtspApplication {

    public static void main(String[] args) {
        SpringApplication.run(NtspApplication.class, args);
    }

}
